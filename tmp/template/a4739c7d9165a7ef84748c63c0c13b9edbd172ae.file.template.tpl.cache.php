<?php /* Smarty version Smarty-3.1.8, created on 2017-09-01 15:56:40
         compiled from "/var/www/sistemas/framework/mvc/core/mvc/views/layout/pecari_bs4/template.tpl" */ ?>
<?php /*%%SmartyHeaderCode:113526204559a9ad6852f018-04720718%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a4739c7d9165a7ef84748c63c0c13b9edbd172ae' => 
    array (
      0 => '/var/www/sistemas/framework/mvc/core/mvc/views/layout/pecari_bs4/template.tpl',
      1 => 1502901558,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '113526204559a9ad6852f018-04720718',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'titulo' => 0,
    '_layoutParams' => 0,
    'css' => 0,
    'plg' => 0,
    'it' => 0,
    'landing' => 0,
    'widgets' => 0,
    'wd' => 0,
    '_error' => 0,
    '_mensaje' => 0,
    '_contenido' => 0,
    'js' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_59a9ad6858fce7_03908096',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59a9ad6858fce7_03908096')) {function content_59a9ad6858fce7_03908096($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/sistemas/framework/mvc/core/libs/smarty/libs/plugins/modifier.date_format.php';
?><!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content = "width = device-width, initial-scale = 1.0, minimum-scale = 1.0, maximum-scale = 1.0, user-scalable = no" />
        <title><?php echo (($tmp = @$_smarty_tpl->tpl_vars['titulo']->value)===null||$tmp==='' ? "Sin t&iacute;tulo" : $tmp);?>
</title>
        <meta charset="utf-8">
        <link href="<?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['ruta_css'];?>
bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['ruta_css'];?>
footable-0.1.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['ruta_css'];?>
bs4_base.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<?php if (isset($_smarty_tpl->tpl_vars['_layoutParams']->value['css'])&&count($_smarty_tpl->tpl_vars['_layoutParams']->value['css'])){?>
<?php  $_smarty_tpl->tpl_vars['css'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['css']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['_layoutParams']->value['css']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['css']->key => $_smarty_tpl->tpl_vars['css']->value){
$_smarty_tpl->tpl_vars['css']->_loop = true;
?>
        <link href="<?php echo $_smarty_tpl->tpl_vars['css']->value;?>
" rel="stylesheet" type="text/css">
<?php } ?>
<?php }?>
<?php if (isset($_smarty_tpl->tpl_vars['_layoutParams']->value['css_plugin'])&&count($_smarty_tpl->tpl_vars['_layoutParams']->value['css_plugin'])){?>
<?php  $_smarty_tpl->tpl_vars['plg'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['plg']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['_layoutParams']->value['css_plugin']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['plg']->key => $_smarty_tpl->tpl_vars['plg']->value){
$_smarty_tpl->tpl_vars['plg']->_loop = true;
?>
        <link href="<?php echo $_smarty_tpl->tpl_vars['plg']->value;?>
" rel="stylesheet" type="text/css">
<?php } ?>
<?php }?>
        
    </head>
    
    <body>
    
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="<?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['root'];?>
"><?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['configs']['app_name'];?>
</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav01" aria-controls="nav01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="nav01">
		  <ul class="navbar-nav mr-auto">
<?php  $_smarty_tpl->tpl_vars['it'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['it']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['_layoutParams']->value['menu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['it']->key => $_smarty_tpl->tpl_vars['it']->value){
$_smarty_tpl->tpl_vars['it']->_loop = true;
?>
<?php if ($_smarty_tpl->tpl_vars['it']->value['enlace']=="dropdown"){?>
				<li class="nav-item dropdown">
					<a data-toggle="dropdown" class="nav-link dropdown-toggle" href="#">
						<?php echo $_smarty_tpl->tpl_vars['it']->value['titulo'];?>
 <b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
<?php echo $_smarty_tpl->getSubTemplate ("submenu_recurse.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null, array('items'=>$_smarty_tpl->tpl_vars['it']->value['submenu']), 0);?>
									
					</ul>
				</li>
<?php }else{ ?>
                    <li<?php if (isset($_smarty_tpl->tpl_vars['_layoutParams']->value['item'])&&$_smarty_tpl->tpl_vars['_layoutParams']->value['item']==$_smarty_tpl->tpl_vars['it']->value['id']){?>class="active"<?php }?>>
                    	<a class="nav-link" href="<?php echo $_smarty_tpl->tpl_vars['it']->value['enlace'];?>
">
<?php if (isset($_smarty_tpl->tpl_vars['it']->value['imagen'])){?>                        	<i class="<?php echo $_smarty_tpl->tpl_vars['it']->value['imagen'];?>
"> </i> <?php }?><?php echo $_smarty_tpl->tpl_vars['it']->value['titulo'];?>

                        </a>
                	</li>
<?php }?>
<?php } ?>
          </ul>
      	
        </ul>
<?php if (Session::get('autenticado')){?>
				<div class="pull-right">
					<a class="btn btn-outline-primary" href="<?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['root'];?>
usuarios/registro/perfil" title="Perfil de usuario" role="button">
						<span class="fa fa-user"></span>
					</a>
					<a class="btn btn-outline-primary" href="<?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['root'];?>
usuarios/login/cerrar" title="Cerrar sesión" role="button">
						<span class="fa fa-power-off"></span>
					</a>
				</div>
<?php }elseif(@NO_LOGIN==="NO_LOGIN"){?>
			  <form class="form-inline my-2 my-lg-0 navbar-right" method="post" action="<?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['root'];?>
usuarios/login">
				  <input type="hidden" value="1" name="enviar">
				<div class="form-group">
				  <input type="text" placeholder="Usuario" name="usuario" class="form-control">
				</div>
				<div class="form-group">
				  <input type="password" placeholder="Contraseña" name="pass" class="form-control">
				</div>
				<button type="submit" class="btn btn-outline-success">Ingresar</button>
<?php if (Session::get('allow_register')){?>
<a href="<?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['root'];?>
usuarios/registro" class="btn btn-info">Registro</a>
<?php }?>
			</form>
 <?php }?>			  

      </div>

    </nav>


    </div>
<?php if (isset($_smarty_tpl->tpl_vars['landing']->value)){?>              
<div class="jumbotron padt32">
	<div class="container">
		<div style="float:left;<?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['logo_style'];?>
" >
			<img src="<?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['ruta_img'];?>
<?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['logo_img'];?>
">
		</div>
		<h1><?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['configs']['app_name'];?>
</h1>
		<p class="lead"><?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['configs']['app_slogan'];?>
</p>
	</div>
</div>
<?php }else{ ?>

<?php }?>

        <div class="container card" id="mainContainer" style="background: #fff;">
            <div class="card-body">
<?php if (count($_smarty_tpl->tpl_vars['widgets']->value['sidebar'])>0){?>
                <div class="col-lg-3">
<?php if (isset($_smarty_tpl->tpl_vars['widgets']->value['sidebar'])){?>
    <?php  $_smarty_tpl->tpl_vars['wd'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['wd']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['widgets']->value['sidebar']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['wd']->key => $_smarty_tpl->tpl_vars['wd']->value){
$_smarty_tpl->tpl_vars['wd']->_loop = true;
?>
                    <?php echo $_smarty_tpl->tpl_vars['wd']->value;?>

    <?php } ?>
<?php }?>
                </div>
            
                <div class="col-lg-8">
<?php }else{ ?>
                <div class="col-lg-11">
<?php }?>
                    <noscript><p>Para el correcto funcionamiento debe tener el soporte para javascript habilitado</p></noscript>
                    
<?php if (isset($_smarty_tpl->tpl_vars['_error']->value)){?>
                        <div id="_errl" class="alert alert-error">
                            <a class="close" data-dismiss="alert">x</a>
                            <?php echo $_smarty_tpl->tpl_vars['_error']->value;?>

                        </div>
<?php }?>

<?php if (isset($_smarty_tpl->tpl_vars['_mensaje']->value)){?>
                        <div id="_msgl" class="alert alert-success">
                            <a class="close" data-dismiss="alert">x</a>
                            <?php echo $_smarty_tpl->tpl_vars['_mensaje']->value;?>

                        </div>
<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['_contenido']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null, array(), 0);?>

                    </div>
                </div>
            </div>
        </div>
	 <div id="footer" class="footer" style="z-index:999">
      <div class="container">
        <p class="text-muted pull-right">Copyright &copy; 2013-<?php echo smarty_modifier_date_format($_SERVER['REQUEST_TIME'],'%Y');?>
 <a href="http://darknoid.byethost7.com" target="_blank">Darknoid Software</a></p>
      </div>
    </div>
            
        <!-- javascript -->
        <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['ruta_fw'];?>
public/js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['ruta_fw'];?>
public/js/footable.min.js"></script>
        <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['ruta_js'];?>
popper.min.js"></script>
        <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['ruta_js'];?>
bootstrap.min.js"></script>

<?php if (isset($_smarty_tpl->tpl_vars['_layoutParams']->value['js_plugin'])&&count($_smarty_tpl->tpl_vars['_layoutParams']->value['js_plugin'])){?>
<?php  $_smarty_tpl->tpl_vars['plg'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['plg']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['_layoutParams']->value['js_plugin']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['plg']->key => $_smarty_tpl->tpl_vars['plg']->value){
$_smarty_tpl->tpl_vars['plg']->_loop = true;
?>
        <script src="<?php echo $_smarty_tpl->tpl_vars['plg']->value;?>
" type="text/javascript"></script>
<?php } ?>
<?php }?>
<?php if (isset($_smarty_tpl->tpl_vars['_layoutParams']->value['js'])&&count($_smarty_tpl->tpl_vars['_layoutParams']->value['js'])){?>
<?php  $_smarty_tpl->tpl_vars['js'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['js']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['_layoutParams']->value['js']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['js']->key => $_smarty_tpl->tpl_vars['js']->value){
$_smarty_tpl->tpl_vars['js']->_loop = true;
?>
        <script src="<?php echo $_smarty_tpl->tpl_vars['js']->value;?>
" type="text/javascript"></script>
<?php } ?>
<?php }?>

<script type="text/javascript">
    var _root_ = '<?php echo $_smarty_tpl->tpl_vars['_layoutParams']->value['root'];?>
';
</script>
    </body>
</html>
<?php }} ?>