<?php /*%%SmartyHeaderCode:113526204559a9ad6852f018-04720718%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a4739c7d9165a7ef84748c63c0c13b9edbd172ae' => 
    array (
      0 => '/var/www/sistemas/framework/mvc/core/mvc/views/layout/pecari_bs4/template.tpl',
      1 => 1502901558,
      2 => 'file',
    ),
    'd141a0d00d191f30ae6e4660cdb8f005906c33c4' => 
    array (
      0 => '/var/www/sistemas/framework/mvc/core/mvc/views/layout/pecari_bs4/submenu_recurse.tpl',
      1 => 1502900965,
      2 => 'file',
    ),
    '323797cc21ce804af265625256f3bf6ff7ff6c87' => 
    array (
      0 => '/var/www/sistemas/framework/mvc/core/mvc/views/index/index.tpl',
      1 => 1411488466,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '113526204559a9ad6852f018-04720718',
  'variables' => 
  array (
    'titulo' => 0,
    '_layoutParams' => 0,
    'css' => 0,
    'plg' => 0,
    'it' => 0,
    'landing' => 0,
    'widgets' => 0,
    'wd' => 0,
    '_error' => 0,
    '_mensaje' => 0,
    '_contenido' => 0,
    'js' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_59a9ad685a7895_52907708',
  'cache_lifetime' => 3,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59a9ad685a7895_52907708')) {function content_59a9ad685a7895_52907708($_smarty_tpl) {?><!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content = "width = device-width, initial-scale = 1.0, minimum-scale = 1.0, maximum-scale = 1.0, user-scalable = no" />
        <title>Portada</title>
        <meta charset="utf-8">
        <link href="http://localhost/sistemas/framework/mvc/core/mvc/views/layout/pecari_bs4/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="http://localhost/sistemas/framework/mvc/core/mvc/views/layout/pecari_bs4/css/footable-0.1.css" rel="stylesheet" type="text/css">
        <link href="http://localhost/sistemas/framework/mvc/core/mvc/views/layout/pecari_bs4/css/bs4_base.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        
    </head>
    
    <body>
    
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="http://localhost/sistemas/framework/mvc/">Pecarí</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav01" aria-controls="nav01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="nav01">
		  <ul class="navbar-nav mr-auto">
				<li class="nav-item dropdown">
					<a data-toggle="dropdown" class="nav-link dropdown-toggle" href="#">
						Mantenimiento <b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						 <li class="nav-item dropdown-submenu">
							<a data-toggle="dropdown" class="dropdown-item dropdown-toggle" href="#">
								Permisos de sistema
							</a>
							<ul class="dropdown-menu">
<!-- Start submenu //-->
						<li class="dropdown-item">
							<a  class="dropdown-item dropdown-standalone" href="http://localhost/sistemas/framework/mvc/usuarios">
								<i class="icon-user glyphicon glyphicon-user fa fa-user"> </i> Usuarios
							</a>
						</li>
						<li class="dropdown-item">
							<a  class="dropdown-item dropdown-standalone" href="http://localhost/sistemas/framework/mvc/acl/roles">
								<i class="icon-list-alt glyphicon glyphicon-list-alt fa fa-list-alt"> </i> Roles
							</a>
						</li>
						<li class="dropdown-item">
							<a  class="dropdown-item dropdown-standalone" href="http://localhost/sistemas/framework/mvc/acl/permisos">
								<i class="icon-list-alt glyphicon glyphicon-list-alt fa fa-list-alt"> </i> Permisos
							</a>
						</li>
									
<!-- End submenu //-->
							</ul>
						</li>
						 <li class="nav-item dropdown-submenu">
							<a data-toggle="dropdown" class="dropdown-item dropdown-toggle" href="#">
								Base de datos
							</a>
							<ul class="dropdown-menu">
<!-- Start submenu //-->
						<li class="dropdown-item">
							<a  class="dropdown-item dropdown-standalone" href="http://localhost/sistemas/framework/mvc/respaldo">
								<i class="icon-download-alt glyphicon glyphicon-download-alt fa fa-download"> </i> Respaldar
							</a>
						</li>
						<li class="dropdown-item">
							<a  class="dropdown-item dropdown-standalone" href="http://localhost/sistemas/framework/mvc/respaldo/restore">
								<i class="icon-refresh glyphicon glyphicon-refresh fa fa-refresh"> </i> Restaurar
							</a>
						</li>
						<li class="dropdown-item">
							<a  class="dropdown-item dropdown-standalone" href="http://localhost/sistemas/framework/mvc/respaldo/limpiar">
								<i class="icon-wrench glyphicon glyphicon-wrench fa fa-wrench"> </i> Limpiar
							</a>
						</li>
									
<!-- End submenu //-->
							</ul>
						</li>
						 <li class="nav-item dropdown-submenu">
							<a data-toggle="dropdown" class="dropdown-item dropdown-toggle" href="#">
								Mailer interno
							</a>
							<ul class="dropdown-menu">
<!-- Start submenu //-->
						<li class="dropdown-item">
							<a  class="dropdown-item dropdown-standalone" href="http://localhost/sistemas/framework/mvc/modmailer/editor">
								<i class="icon-pencil glyphicon glyphicon-pencil fa fa-pencil"> </i> Editor de plantillas
							</a>
						</li>
									
<!-- End submenu //-->
							</ul>
						</li>
						<li class="dropdown-item">
							<a  class="dropdown-item dropdown-standalone" href="http://localhost/sistemas/framework/mvc/pecariInfo">
								<i class=""> </i> Pecarí Info
							</a>
						</li>
									
					</ul>
				</li>
                    <li>
                    	<a class="nav-link" href="http://localhost/sistemas/framework/mvc/prueba">
                        	<i class="icon-eye-open icon-white"> </i> Prueba
                        </a>
                	</li>
          </ul>
      	
        </ul>
				<div class="pull-right">
					<a class="btn btn-outline-primary" href="http://localhost/sistemas/framework/mvc/usuarios/registro/perfil" title="Perfil de usuario" role="button">
						<span class="fa fa-user"></span>
					</a>
					<a class="btn btn-outline-primary" href="http://localhost/sistemas/framework/mvc/usuarios/login/cerrar" title="Cerrar sesión" role="button">
						<span class="fa fa-power-off"></span>
					</a>
				</div>
			  

      </div>

    </nav>


    </div>


        <div class="container card" id="mainContainer" style="background: #fff;">
            <div class="card-body">
                <div class="col-lg-11">
                    <noscript><p>Para el correcto funcionamiento debe tener el soporte para javascript habilitado</p></noscript>
                    

<h3>Framework Pecarí v0.9.6.1</h3>
<p>
Framework para desarrollo de aplicaciones web creado por <a href="http://darknoid.blogspot.com" target="_blank">Washington Fernández</a>
</p>
                    </div>
                </div>
            </div>
        </div>
	 <div id="footer" class="footer" style="z-index:999">
      <div class="container">
        <p class="text-muted pull-right">Copyright &copy; 2013-2017 <a href="http://darknoid.byethost7.com" target="_blank">Darknoid Software</a></p>
      </div>
    </div>
            
        <!-- javascript -->
        <script type="text/javascript" src="http://localhost/sistemas/framework/mvc/public/js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="http://localhost/sistemas/framework/mvc/public/js/footable.min.js"></script>
        <script type="text/javascript" src="http://localhost/sistemas/framework/mvc/core/mvc/views/layout/pecari_bs4/js/popper.min.js"></script>
        <script type="text/javascript" src="http://localhost/sistemas/framework/mvc/core/mvc/views/layout/pecari_bs4/js/bootstrap.min.js"></script>


<script type="text/javascript">
    var _root_ = 'http://localhost/sistemas/framework/mvc/';
</script>
    </body>
</html>
<?php }} ?>