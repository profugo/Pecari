<?php
include_once 'pecari_base_url.php';

// Framework version
$config[ "version" ] = "0.9.6.5.2";

// Default settings
$config[ "default_layout"     ] = "pecari";
$config[ "default_module"     ] = "";
$config[ "default_controller" ] = "index";

// URL related stuff
$config[ "fw_url"    ] = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]/{$pecari_base_url}";
$config[ "base_url"  ] = $config[ "fw_url" ];

// $config[ "fw_url"    ] = 'http://sistemas.coodi.info/pecari/framework/';

$config[ "core_url"  ] = $config[ "fw_url"  ] . "core/";
$config[ "user_url"  ] = $config[ "base_url"  ] . "application/";

// Application name, company and logo
$config[ "app_name"    ] = "Pecarí";
$config[ "app_slogan"  ] = "Framework Pecarí v" . $config[ "version" ];
$config[ "app_logo"    ] = "pecari_logo_white.gif";
$config[ "logo_css"    ] = "width:180px; height:120px";
$config[ "app_company" ] = "www.darknoid.blogger.com";

// The hash must be different in every app in order to improve security
$config[ "hash_key" ] = "8cd9430fa4d7d8";

// Session setup 
// Time to live in minutes, 0 for no limit
$config[ "session_time" ] = 0;

// Path to the framework, and to the application defined as user_path
$config[ "root"      ] = realpath( dirname( __FILE__ ) . DS . '..' . DS . '..' )  . DS;
$config[ "core_path" ] = $config[ "root"      ] . "core" . DS;
$config[ "app_path"  ] = $config[ "core_path" ] . "base" . DS;
$config[ "user_path" ] = $config[ "root"      ] . "application" . DS;

// Default prefix for tables used by the framework
$config[ "fw_prefix" ] = "";

// Disable login section in main menu
//$config[ "no_login" ] = "1";
