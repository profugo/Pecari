<?php
$config["safe_pass"] = true;
$config["error_limit"] = 5;
$config["lock_minutes"] = 20;
$config["users_per_page"] = 15;
$config["err_msg"] = "Cuenta bloqueada preventivamente por seguridad";
$config["allow_register"] = 0;

$config["mail_from"] = "";
$config["mail_name"] = "";
$config["mail_host"] = "";
$config["mail_user"] = "";
$config["mail_pass"] = "";