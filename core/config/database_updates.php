<?php
$config[ "updates" ] = array(
    array( 
        "version" => "0.8.9", 
        "sql" => array()
        ),
    array(
        "version" => "0.8.9.1",
        "sql" => array(
                "ALTER TABLE `" . FW_PREFIX . "usuarios` ADD `errors` INT NOT NULL DEFAULT '0'",
                "CREATE TABLE IF NOT EXISTS `" . FW_PREFIX . "usuarios_bloqueo` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `usuario` int(11) NOT NULL,
                  `fecha` datetime NOT NULL,
                  `ip` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;
                "
            )
        ),
    array(
        "version" => "0.9.1",
        "sql" => array(
            "CREATE TABLE IF NOT EXISTS `" . FW_PREFIX . "mail_manager` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `nombre` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
              `clave` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
              `html` text COLLATE utf8_spanish_ci NOT NULL,
              `text` text COLLATE utf8_spanish_ci NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;
            "
        )
    ),
    array(
        "version" => "0.9.4",
        "sql" => array(
            "CREATE TABLE `" . FW_PREFIX . "sessions` (
            `id` CHAR(128) NOT NULL,
            `set_time` CHAR(10) NOT NULL,
            `data` text NOT NULL,
            `session_key` CHAR(128) NOT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

            "
        )
    ),
    array(
        "version" => "0.9.6.3",
        "sql" => array(
            "ALTER TABLE `" . FW_PREFIX . "gen_conf` ADD UNIQUE( `campo`);"
        )
    ),
    array(
        "version" => "0.9.6.5",
        "sql" => array(
            "CREATE TABLE `" . FW_PREFIX . "tuplas` (
            `clave` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
            `valor` text COLLATE utf8_spanish_ci NOT NULL,
            PRIMARY KEY (`clave`)
          ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;"
        )
    )
);