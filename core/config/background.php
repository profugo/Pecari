<?php
// Format array(<family>, <backround>)
// <family>: any from "floral", "mini" or "mono"
// <background>: a number from 1 to 5 of 5 availables tiles per family
$config["default_background"] = array("mini", 4);

// CSS style to pass to the style generator, this will apply to the html background
//$config["style"] = "min-height:100%; background-size: cover;";