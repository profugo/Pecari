<?php
$base_menu = array();

$menu_usuario = array(
    'id'      => 'permisos',
    'titulo'  => 'Permisos de sistema',
    'enlace'  => 'dropdown',
    'submenu' => array(
        array(
            'id'     => 'usuarios',
            'titulo' => 'Usuarios',
            'enlace' => BASE_URL . 'usuarios',
            'imagen' => 'icon-user glyphicon glyphicon-user fa fa-user'
        ),
        array(
            'id'     => 'roles',
            'titulo' => 'Roles',
            'enlace' => BASE_URL . 'acl/roles',
            'imagen' => 'icon-list-alt glyphicon glyphicon-list-alt fa fa-list-alt'
        ),
        array(
            'id'     => 'permisos',
            'titulo' => 'Permisos',
            'enlace' => BASE_URL . 'acl/permisos',
            'imagen' => 'icon-list-alt glyphicon glyphicon-list-alt fa fa-list-alt'
        )
    )
);
 
$menu_respaldo = array(
    'id'      => 'respaldos',
    'titulo'  => 'Base de datos',
    'enlace'  => 'dropdown',
    'submenu' => array(
        array(
            'id'     => 'respaldar',
            'titulo' => 'Respaldar',
            'enlace' => BASE_URL . 'respaldo',
            'imagen' => 'icon-download-alt glyphicon glyphicon-download-alt fa fa-download'
        ),
        array(
            'id'     => 'restaurar',
            'titulo' => 'Restaurar',
            'enlace' => BASE_URL . 'respaldo/restore',
            'imagen' => 'icon-refresh glyphicon glyphicon-refresh fa fa-refresh'
        ),
        array(
            'id'     => 'limpiar',
            'titulo' => 'Limpiar',
            'enlace' => BASE_URL . 'respaldo/limpiar',
            'imagen' => 'icon-wrench glyphicon glyphicon-wrench fa fa-wrench'
        )
    )
);

$menu_mailer = array(
    'id'      => 'mailer',
    'titulo'  => 'Mailer interno',
    'enlace'  => 'dropdown',
    'submenu' => array(
        array(
            'id'     => 'editor',
            'titulo' => 'Editor de plantillas',
            'enlace' => BASE_URL . 'modmailer/editor',
            'imagen' => 'icon-pencil glyphicon glyphicon-pencil fa fa-pencil'
        )
    )
);

$menu_pecari = array(
    'id'      => 'pecariInfo',
    'titulo'  => 'Pecarí Info',
    'enlace'  => BASE_URL . 'pecariInfo',
    'imagen' => 'fa fa-info'
);

$menu_editor = array(
    'id'      => 'pecariMenuEditor',
    'titulo'  => 'Editor de menús',
    'enlace'  => BASE_URL . 'menueditor',
    'imagen' => 'fa fa-list'
);

if ( Session::get('autenticado') ) {
    if( $acl->permiso( "admin_access") == true) {
        $base_menu[] = array(
            'id'      => 'mantenimiento',
            'titulo'  => 'Mantenimiento',
            'enlace'  => 'dropdown',
            'imagen'  => 'fa fa-wrench', 
            'submenu' => array(
                $menu_usuario,
                $menu_respaldo,
                $menu_mailer,
                $menu_pecari,
                $menu_editor
            )
        );
    }
}

$config["menu"] = $base_menu;
