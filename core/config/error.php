<?php
$config["sys_errors"] = array( 
        'default' => 'Ha ocurrido un error y la página no puede mostrarse',
        'e5050' => 'Acceso restringido',
        'e5051' => 'Su usuario no tiene privilegios suficientes',
        'e5052' => 'Error de autenticación de cuenta',
        'e5053' => 'Error al enviar correo para autenticar cuenta',
        'e1001' => 'Error en formato de solicitud',
        'e1002' => 'Parámetros incorrectos',
        'e1003' => 'Propiedad <u>{PARAM}</u> no declarada',
        'e1004' => 'No se pudo enviar el correo electrónico, contacte con el administrador para que verifique los parámetros de configuración',
        'e8080' => 'Tiempo de la sesion agotado' 
);