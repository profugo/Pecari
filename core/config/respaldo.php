<?php
$config["filename"] = "backup";
$config["compress"] = "None";

// Default values
// $config['include-tables'] = array(); // Empty for all
// $config['exclude-tables'] = array(); // Empty for none
// $config['no-data'] = false; // No includes
// $config['add-drop-database'] = false;
// $config['add-drop-table'] = false; // Drop table if exists
// $config['single-transaction'] = true;
// $config['lock-tables'] = false;
// $config['add-locks'] = true;
// $config['extended-insert'] = true;
// $config['disable-foreign-keys-check'] = false;
// $config['where'] = '';
// $config['no-create-info'] = false;