<?php
$config["filename"] = 'backup.sql.gz'; // Specify the dump filename to suppress the file selection dialog
$config["linespersession"] = 3000;     // Lines to be executed per one import session
$config["delaypersession"] = 0;        // You can specify a sleep time in milliseconds after each session
									   // Works only if JavaScript is activated. Use to reduce server overrun

// Allowed comment markers: lines starting with these strings will be ignored by BigDump
$comment[]='#';                       // Standard comment lines are dropped by default
$comment[]='-- ';
$comment[]='DELIMITER';               // Ignore DELIMITER switch as it's not a valid SQL statement
// $comment[]='---';                  // Uncomment this line if using proprietary dump created by outdated mysqldump
// $comment[]='CREATE DATABASE';      // Uncomment this line if your dump contains create database queries in order to ignore them
$comment[]='/*!';                     // Or add your own string to leave out other proprietary things

$config["comment"] = $comment;

// Pre-queries: SQL queries to be executed at the beginning of each import session

// $pre_query[]='SET foreign_key_checks = 0';
// $pre_query[]='Add additional queries if you want here';
// $config["pre_query"] = $pre_query; 

// Default query delimiter: this character at the line end tells Bigdump where a SQL statement ends
// Can be changed by DELIMITER statement in the dump file (normally used when defining procedures/functions)
$config["delimiter"] = ';';

// String quotes character
$config["string_quotes"] = '\'';  // Change to '"' if your dump file uses double qoutes for strings

// How many lines may be considered to be one query (except text lines)
$config["max_query_lines"] = 300;


$config["data_chunk_length"] =16384;  // How many chars are read per time
