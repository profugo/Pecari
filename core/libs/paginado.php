<?php
class Paginado extends Controller
{
    public function index()
    {
    }

    public function paginar($pagina, $pag_total)
    {
        $pagina     = $this->filtrarInt($pagina);

        $pagina     = $pagina == 0 ? 1 : $pagina;
        $pag_inicio = ($pagina - 4) < 1 ? 1 : ($pagina - 4);
        $pag_fin    = $pag_inicio + 9;

        if ($pag_fin > $pag_total) {
            $pag_fin = $pag_total;
            $pag_inicio = $pag_fin - 9;
            if ($pag_inicio < 1) {
                $pag_inicio = 1;
            }
        }

        $pag_prev = ($pagina - 10) < 1 ? 1 : ($pagina - 10);
        $pag_next = $pagina + 10;

        if ($pag_next > $pag_total) {
            $pag_next = $pag_total;
        }

        $paginas = array();
        for ($i = $pag_inicio; $i <= $pag_fin; $i++) {
            $linea = array("pag" => $i, "class" => "", "url" => "href='#' onclick='toPage({$i});return false'");
            if ($i == $pagina) {
                $linea["class"] = "active";
                $linea["url"] = "";
            }
            $paginas[] = $linea;
        }

        $paginado = array(
            "pag_total" => $pag_total,
            "nav_prev"  => array("class" => "", "url" => "href='#' onclick='toPage({$pag_prev});return false'"),
            "nav_next"  => array("class" => "", "url" => "href='#' onclick='toPage({$pag_next});return false'"),
            "paginas"   => $paginas,
            "actual"    => $pagina
        );

        if ($pagina == 1) {
            $paginado["nav_prev"]["class"] = "disabled";
            $paginado["nav_prev"]["url"] = "";
        }

        if ($pagina == $pag_total) {
            $paginado["nav_next"]["class"] = "disabled";
            $paginado["nav_next"]["url"] = "";
        }

        return $paginado;
    }
	
}