<?php

class Mail_manager
{
	private $_modelo;
	private $_view;
	private $_smtp;
	private $_subject = "Sin asunto preestablecido";
	
	public function __construct($view, $modelo = false, $smtp = false)
	{
		if (is_object($view)) {
			$this->_view = $view;
		} else {
			throw new Exception('Mail_manager error: View not an object in construct');
		}
		
		if ($modelo != false && is_object($modelo)){
			$this->_modelo = $modelo;
		} else {
			throw new Exception('Mail_manager error: No model set in construct');
		}
		
		if (is_array($smtp)) {
			$this->_smtp = $smtp;
		} else {
			throw new Exception('Mail_manager error: No smtp mail data in construct');
		}
		
	}
	
	public function setSubject($subject)
	{
		$this->_subject = $subject;
	}
	
	private function _compile($templates, $data)
	{
		$templates["html"] = str_replace('\r\n', "\n", $templates["html"]);
		$templates["text"] = str_replace('\r\n', "\n", $templates["text"]);
		
                if (isset($data["body"])) {
                    foreach($data["body"] as $clave => $valor) {
                            $templates["html"] = str_replace("@" . strtoupper($clave) . "@", $valor, $templates["html"]);
                            $templates["text"] = str_replace("@" . strtoupper($clave) . "@", $valor, $templates["text"]);
                    }
                }
                
                if (isset($data["subject"])) {
                    foreach($data["subject"] as $clave => $valor) {
                            $templates["subject"] = str_replace("@" . strtoupper($clave) . "@", $valor, $templates["subject"]);
                    }
                } else {
                    $templates["subject"] = $this->_subject;
                }
		$templates["text"] = nl2br($templates["text"]);
                $templates["html"] = stripslashes($templates["html"]);
		
		return $templates;
	}
	
	public function send($target, $template, $data, $bcc = false)
	{
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPAuth   = true;
		 
		$mail->From     = $this->_smtp["mail_from"];
		$mail->FromName = $this->_smtp["mail_name"];
		$mail->Host     = $this->_smtp["mail_host"];
		$mail->Username = $this->_smtp["mail_user"];
		$mail->Password = $this->_smtp["mail_pass"];
		 
		$mail->CharSet="utf-8";
		$mail->IsHTML(true);

		$plantilla = $this->_modelo->getTemplateByKey($template);
		if (! $plantilla) {
			$plantilla = array();
			ob_start();
			$this->_view->assign("datos", $data["body"]);
			$this->_view->renderizar($template, false, true);
			$plantilla["html"] = ob_get_clean();
			$plantilla["text"] = "Requiere un visor de correos que soporte código HTML para ver este mensaje.";
			$plantilla["subject"] = $this->_subject;
		} 
		
		$plantillas = $this->_compile($plantilla, $data);
		
		$mail->Subject = $plantillas["subject"];
		$mail->Body    = $plantillas["html"];
		$mail->AltBody = $plantillas["text"];
		
                if (! is_array($target)) {
                    $target = array($target);
                }
		
		foreach ($target as $tar) {
                    $mail->AddAddress($tar);
                }
                
                if ($bcc != false) {
                    if (!is_array($bcc)) {
                        $bcc = array($bcc);
                    }
                    
                    foreach ($bcc as $bcce) {
                        $mail->AddBCC($bcce);
                    }
                }
		return $mail->Send();
	}
}