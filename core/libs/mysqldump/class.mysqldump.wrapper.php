<?php

class MysqldumpWrapper
{
	
	public function __construct()
	{
		$file = pathinfo(__FILE__, PATHINFO_DIRNAME) . DS . 'Mysqldump.php';
		if (is_readable($file)) {
			include_once $file;
		} else {
			throw new Exception("No se encuentra la libreria Mysqldump.php");
		}
	}
	
	public function dump($database = array(), $settings = array(), $target = "/tmp/output.sql" )
	{
		
		$defaultSettings = array(
				'include-tables' => array(), // all if empty
				'exclude-tables' => array(),
				'compress' => 'None',  // None, Gzip, Bzip2
				'no-data' => false,
				'add-drop-database' => false,
				'add-drop-table' => true,
				'single-transaction' => true,
				'lock-tables' => false,
				'add-locks' => true,
				'extended-insert' => true,
				'disable-foreign-keys-check' => false,
				'where' => '',
				'no-create-info' => false
		);
		
		$settings = array_merge($defaultSettings, $settings);
		/*
		 *  El campo "Type" que está establecido como "mysql" puede trabajar como
		 *  Dblib
		 *  MySql
		 *  
		 */ 
		$dump = new Ifsnop\Mysqldump\Mysqldump(
				$database["db"], 
				$database["user"], 
				$database["pass"], 
				$database["host"], 
				'mysql', 
				$settings
			);
		$dump->start($target);
		
	}
}
?>