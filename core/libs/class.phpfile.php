<?php

class PhpFile {
    private $_control;
    public function __construct() {
        $this->_control = $this->generate();
//        if ($this->compare($this->_control, $this->createKey($control)) == false) {
//            echo "Instalación trucha";
//            exit;
//        } else {
//            echo "Ok";
//        }
    }
    
    private function _str2array($string)
    {
        if (strlen($string) != 32) {
            return false;
        }
        
        $ret = array();
        for ($i = 0; $i < strlen($string); $i = $i + 2) {
            $byte = substr($string, $i, 2);
            $ret[] = hexdec($byte);
        }
        
        return $ret;
    }
    
    public function generate()
    {
        $instPath = ROOT . "index.php";
        $indexDate = filemtime($instPath);
        return md5($indexDate . $instPath . gethostname());
    }
    
    public function compare($file, $strbase = false)
    {
        $control = "b0468f79fffff89272ca1c3ad362c3a6";
        
        if ($strbase == false) {
            $strbase = $this->_control;
        }
        
        if (strlen($file) != 65) {
            $file = "{$control}@{$control}";
        }
        
        $val = $this->_str2array($control);
        
        $base = $this->_str2array($strbase);
        $file = explode("@", $file);
        $file[0] = $this->_str2array($file[0]);
        $file[1] = $this->_str2array($file[1]);
        $ret = "";
        for ($i = 0; $i < count($base); $i++) {
            $v = dechex($file[0][$i] ^ $file[1][$i] ^ $base[$i]);
            if (strlen($v) == 1) {
                $v = "0" . $v;
            }
            $ret .= $v;
        }
        
        return ($ret == $control);
    }
    
}