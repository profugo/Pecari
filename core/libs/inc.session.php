<?php
//inc.session.php

class SysSession implements SessionHandlerInterface
{
    private $link;
    private $_conf;
    
    function __construct() {
        $this->_conf = new Configuration();
        $this->_conf->load("database");
        // This line prevents unexpected effects when using objects as save handlers.
        register_shutdown_function('session_write_close');
    }
    
    public function open($savePath, $sessionName)
    {
        $link = new mysqli(
            $this->_conf->item("database", "db_host"), 
            $this->_conf->item("database", "db_user"),
            $this->_conf->item("database", "db_pass"),
            $this->_conf->item("database", "db_name")
         );
        
        if($link){
            $this->link = $link;
            return true;
        }else{
            return false;
        }
    }
    
    public function close()
    {
        mysqli_close($this->link);
        return true;
    }
    
    public function read($id)
    {
        $result = mysqli_query($this->link,"SELECT data FROM " . FW_PREFIX . "sessions WHERE id = '".$id."' AND set_time > '".time()."'");
        if($row = mysqli_fetch_assoc($result)){
            return $row['data'];
        }else{
            return "";
        }
    }
    
    public function write($id, $data)
    {
        $key = md5($data);
        $result = mysqli_query($this->link,"
                REPLACE INTO " . FW_PREFIX . "sessions 
                    SET id = '".$id."',
                    set_time = '".time()."',
                    session_key = '".$key."',
                    data = '".$data."'");
        if($result){
            return true;
        }else{
            return false;
        }
    }
    
    public function destroy($id)
    {
        $result = mysqli_query($this->link,"DELETE FROM " . FW_PREFIX . "sessions WHERE id ='".$id."'");
        if($result){
            return true;
        }else{
            return false;
        }
    }
    
    public function gc($maxlifetime)
    {
        $result = mysqli_query($this->link,"DELETE FROM " . FW_PREFIX . "sessions WHERE (set_time + ".$maxlifetime.") < ".$maxlifetime);
        if($result){
            return true;
        }else{
            return false;
        }
    }
}
$handler = new SysSession();
session_set_save_handler($handler, true);
