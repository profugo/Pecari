<?php
/**
 * Clase de modelo de widget menu
 *
 * PHP Version 5.4
 *
 * @category Application.widgets
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /application/core/widgets/models/menu.php
 *
 */

/**
 * Clase de modelo de widget menu
 *
 * @category Application.mvc.controllers
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /application/core/widgets/models/menu.php
 */

class MenuModelWidget extends Model
{
    /**
     * Constructor predeterminado
     * 
     * @return null
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Obtener menú
     * 
     * @return array
     */
    public function getMenu()
    {
        return array(
            array(
                'id' => 'usuarios',
                'titulo' => 'Usuarios',
                'enlace' => BASE_URL . 'usuarios',
                'imagen' => 'icon-user'
                ),
            
                array(
                'id' => 'acl',
                'titulo' => 'Listas de control de acceso',
                  'enlace' => BASE_URL . 'acl',
                'imagen' => 'icon-list-alt'
                   ),
                
            array(
                'id' => 'roles',
                'titulo' => 'Roles',
                'enlace' => BASE_URL . 'acl/roles',
                'imagen' => 'icon-list-alt'
                ),
                
            array(
                'id' => 'permisos',
                'titulo' => 'Permisos',
                'enlace' => BASE_URL . 'acl/permisos',
                   'imagen' => 'icon-list-alt'
                )
        );
    }
}

