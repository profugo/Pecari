<?php
/**
 * Clase de controlador de widget menu
 *
 * PHP Version 5.4
 *
 * @category Application.widgets
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /application/core/widgets/menu.php
 *
 */

/**
 * Clase de controlador de widget menu
 *
 * @category Application.mvc.controllers
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /application/core/widgets/menu.php
 */

class MenuWidget extends Widget
{
    private $_modelo;
    
    /**
     * Constructor predeterminado
     * 
     * @return null
     */
    public function __construct()
    {
        $this->_modelo = $this->loadModel('menu');
    }
    
    /**
     * Obtener el render de menu
     * 
     * @return html
     */
    public function getMenu()
    {
        $data['menu'] = $this->_modelo->getMenu();
        return $this->render('menu-right', $data);
    }
    
    /**
     * Obtener posicion del widget
     * 
     * @return multitype:string multitype:string
     */
    public function getConfig()
    {
        return array(
            'position' => 'sidebar',
            'show' => array( 'usuarios' ),
            'hide' => array( 'inicio' )
        );
    }
}