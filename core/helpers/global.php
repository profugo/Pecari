<?php
function toDefine($data = array())
{
    foreach ($data as $key => $value) {
        define(strtoupper($key), $value);
    }
}

function bunzip2 ($in, $out)
{
    if (!file_exists ($in) || !is_readable ($in))
        return false;
    if ((!file_exists ($out) && !is_writeable (dirname ($out)) || (file_exists($out) && !is_writable($out)) ))
        return false;

    $in_file = bzopen ($in, "r");
    $out_file = fopen ($out, "w");

    while ($buffer = bzread ($in_file, 4096)) {
        fwrite ($out_file, $buffer, 4096);
    }

    bzclose ($in_file);
    fclose ($out_file);

    return true;
}

function gunzip($srcName, $dstName) 
{
    if (!file_exists ($srcName) || !is_readable ($srcName))
        return false;
    if ((!file_exists ($dstName) && !is_writeable (dirname ($dstName)) || (file_exists($dstName) && !is_writable($dstName)) ))
        return false;

    $sfp = gzopen($srcName, "r");
    $fp = fopen($dstName, "w");

    while (!gzeof($sfp)) {
        $string = gzread($sfp, 4096);
        fwrite($fp, $string, strlen($string));
    }
    gzclose($sfp);
    fclose($fp);

    return true;
}
