<?php

class Templates
{
    public function get($file, $path = "templates/")
    {
        return file_get_contents($path . $file);
    }
    
    public function set($file, $data)
    {
        file_put_contents($file, $data);
    }
}