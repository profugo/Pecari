-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `@FW_PREFIX@gen_conf`
--

CREATE TABLE `@FW_PREFIX@gen_conf` (
  `campo` varchar(50) NOT NULL,
  `valor` varchar(256) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `@FW_PREFIX@gen_conf`
--

INSERT INTO `@FW_PREFIX@gen_conf` (`campo`, `valor`) VALUES
('cache_clear', '1504036920'),
('default_role', '2'),
('version', '0.9.6.1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `@FW_PREFIX@mail_manager`
--

CREATE TABLE `@FW_PREFIX@mail_manager` (
  `id` int(11) NOT NULL,
  `nombre` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `html` text COLLATE utf8_spanish_ci NOT NULL,
  `text` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `@FW_PREFIX@permisos`
--

CREATE TABLE `@FW_PREFIX@permisos` (
  `id_permiso` int(11) NOT NULL,
  `permiso` varchar(100) NOT NULL,
  `key` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `@FW_PREFIX@permisos`
--

INSERT INTO `@FW_PREFIX@permisos` (`id_permiso`, `permiso`, `key`) VALUES
(1, 'Tareas de administracion', 'admin_access'),
(2, 'Administrar usuarios', 'admin_users'),
(3, 'Usuario limitado', 'limited_user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `@FW_PREFIX@permisos_role`
--

CREATE TABLE `@FW_PREFIX@permisos_role` (
  `role` int(11) NOT NULL,
  `permiso` int(11) NOT NULL,
  `valor` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `@FW_PREFIX@permisos_role`
--

INSERT INTO `@FW_PREFIX@permisos_role` (`role`, `permiso`, `valor`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `@FW_PREFIX@permisos_usuario`
--

CREATE TABLE `@FW_PREFIX@permisos_usuario` (
  `usuario` int(11) NOT NULL,
  `permiso` int(11) NOT NULL,
  `valor` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `@FW_PREFIX@roles`
--

CREATE TABLE `@FW_PREFIX@roles` (
  `id_role` int(11) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `@FW_PREFIX@roles`
--

INSERT INTO `@FW_PREFIX@roles` (`id_role`, `role`) VALUES
(1, 'Administrador'),
(2, 'Gestor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `@FW_PREFIX@sessions`
--

CREATE TABLE `@FW_PREFIX@sessions` (
  `id` char(128) NOT NULL,
  `set_time` char(10) NOT NULL,
  `data` text NOT NULL,
  `session_key` char(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `@FW_PREFIX@usuarios`
--

CREATE TABLE `@FW_PREFIX@usuarios` (
  `id` int(4) UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `role` int(11) NOT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `codigo` bigint(20) NOT NULL,
  `errors` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `@FW_PREFIX@usuarios`
--

INSERT INTO `@FW_PREFIX@usuarios` (`id`, `nombre`, `usuario`, `pass`, `email`, `role`, `estado`, `fecha`, `codigo`, `errors`) VALUES
(1, 'Sys Admin', 'admin', '@ADMIN@', 'admin@system.com', 1, 1, '0000-00-00 00:00:00', 3025923304, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `@FW_PREFIX@usuarios_bloqueo`
--

CREATE TABLE `@FW_PREFIX@usuarios_bloqueo` (
  `id` int(11) NOT NULL,
  `usuario` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `ip` varchar(16) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


--
-- Indices de la tabla `@FW_PREFIX@gen_conf`
--
ALTER TABLE `@FW_PREFIX@gen_conf`
  ADD UNIQUE KEY `campo` (`campo`,`valor`);

--
-- Indices de la tabla `@FW_PREFIX@mail_manager`
--
ALTER TABLE `@FW_PREFIX@mail_manager`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `@FW_PREFIX@permisos`
--
ALTER TABLE `@FW_PREFIX@permisos`
  ADD PRIMARY KEY (`id_permiso`);

--
-- Indices de la tabla `@FW_PREFIX@permisos_role`
--
ALTER TABLE `@FW_PREFIX@permisos_role`
  ADD UNIQUE KEY `role` (`role`,`permiso`);

--
-- Indices de la tabla `@FW_PREFIX@permisos_usuario`
--
ALTER TABLE `@FW_PREFIX@permisos_usuario`
  ADD UNIQUE KEY `usuario` (`usuario`,`permiso`);

--
-- Indices de la tabla `@FW_PREFIX@roles`
--
ALTER TABLE `@FW_PREFIX@roles`
  ADD PRIMARY KEY (`id_role`);

--
-- Indices de la tabla `@FW_PREFIX@sessions`
--
ALTER TABLE `@FW_PREFIX@sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `@FW_PREFIX@usuarios`
--
ALTER TABLE `@FW_PREFIX@usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `@FW_PREFIX@usuarios_bloqueo`
--
ALTER TABLE `@FW_PREFIX@usuarios_bloqueo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de la tabla `@FW_PREFIX@mail_manager`
--
ALTER TABLE `@FW_PREFIX@mail_manager`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT de la tabla `@FW_PREFIX@permisos`
--
ALTER TABLE `@FW_PREFIX@permisos`
  MODIFY `id_permiso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `@FW_PREFIX@roles`
--
ALTER TABLE `@FW_PREFIX@roles`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `@FW_PREFIX@usuarios`
--
ALTER TABLE `@FW_PREFIX@usuarios`
  MODIFY `id` int(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `@FW_PREFIX@usuarios_bloqueo`
--
ALTER TABLE `@FW_PREFIX@usuarios_bloqueo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
