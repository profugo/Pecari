<?php
// URL related stuff

$config[ "base_url"  ] = "@BASE_URL@";
$config[ "user_url"  ] = $config[ "base_url"  ] . "application/";

// Application name, company and logo
$config[ "app_name"    ] = "Pecarí";
// $config[ "app_slogan"  ] = "Framework Pecarí";
$config[ "app_logo"    ] = "pecari_logo_white.gif";
$config[ "logo_css"    ] = "width:180px; height:120px";
$config[ "app_company" ] = "www.darknoid.blogger.com";

// The hash must be different in every app in order to improve security
$config[ "hash_key" ] ="@HASH_KEY@";

// Path to the framework, and to the application defined as user_path
$config[ "root"      ] = realpath( dirname( __FILE__ ) . DS . '..' . DS . '..' )  . DS;
$config[ "user_path" ] = $config[ "root"      ] . "application" . DS;


// Default settings
$config[ "default_layout"     ] = "pecari_bs4";

$config[ "fw_prefix" ] = "@PREFIJO@";
