<?php

include_once 'databaseClass.php';
include_once 'templatesClass.php';
include_once '../../base/Hash.php';

$templates = new Templates();

function get($var)
{
    $ret = isset($_POST[$var]) ? $_POST[$var] : NULL;
    if (isset($ret)) {
        $ret = trim($ret);
    }
    return $ret;
}

function puts($var)
{
    global $bbdd;
    if (isset($bbdd[$var])) {
        echo $bbdd[$var];
    } 
}

function replace($target, $values)
{
    foreach($values as $key=>$value) {
        $key = strtoupper("@{$key}@");
        $target = str_replace($key, $value, $target);
    }
    
    return $target;
}

$footer_anio = date("Y");
if (isset($_GET["d"]) || isset($directory)) {
    $directory = isset($_GET["d"]) ? $_GET["d"] : $directory;
    $d = str_replace("!", "/", $directory);
    $ruta_conf = $d . "application/config";
    $ruta_bbdd = $ruta_conf . "/database.php";
    $ruta_fw = $ruta_conf . "/framework.php";
    $ruta_tmp = $d . "tmp";
    if (is_writable($ruta_conf) == false) {
        $error = "ERROR: Se necesita permiso de escritura en el directorio de configuración<br><code>$ruta_conf</code>";
    } elseif (is_writable($ruta_tmp) == false) {
        $error = "ERROR: Se necesita permiso de escritura en el directorio temporal<br><code>$ruta_tmp</code>";
    }
} else {
    $error = "ERROR: Acceso no autorizado.";
}

$bbdd = array(
    "db_host" => get("host"),
    "db_user" => get("user"),
    "db_pass" => get("pass"),
    "db_name" => get("name"),
    "db_char" => get("char"),
    "prefijo" => get("prefijo"),
    "base_url"=> get("url"),
    "apass"   => get("apass"),
    "hash_key"=> substr(md5(date("Y-m-d H:i:s:u")), -16)
);

if (isset($bbdd["db_name"])) {
    if(strlen($bbdd["apass"]) < 6) {
        $error = "ERROR: Se requiere clave de administrador y debe tener al menos 6 caracteres de largo.";
    } else {
        try {
            $_db = new Database($bbdd);
            
            $query = $templates->get("pecari_base.sql");
            $file_fw = replace($templates->get("framework.php"), $bbdd);
            $file_db = replace($templates->get("database.php"), $bbdd);
            
            $hash = Hash::getHash('sha1', $_db->filtrarSql($bbdd["apass"]), $bbdd["hash_key"]);
            $query = str_replace("@ADMIN@", $hash, $query);
            $query = str_replace("@FW_PREFIX@", $bbdd["prefijo"], $query);
            $templates->set($ruta_tmp . "/tmpquery.sql", $query);
            
            $_db->parseSqlFile($ruta_tmp . "/tmpquery.sql");
            unlink($ruta_tmp . "/tmpquery.sql"); 
            $templates->set($ruta_bbdd, $file_db);
            $templates->set($ruta_fw, $file_fw);
            
            chdir("../../..");
            if (is_writable("install")) {
				rename("install", "_install");
			} 
            
            header("Location: " . $_SERVER["REQUEST_URI"]);
            exit;
        } catch (Exception $ex) {
            $error = $ex->getMessage();
        }
    }
} else {
    $bbdd = array(
        "db_host" => "localhost",
        "db_user" => "root",
        "db_char" => "utf8",
        "prefijo" => "fw_",
        "base_url"=> (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" . $_SERVER["REQUEST_URI"]
    );
}
include_once 'views/index.phtml';
exit;
