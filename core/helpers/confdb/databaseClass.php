<?php
class Database extends PDO
{
    private $_bbdd;
    
    public function __construct($datos)
    {
        $this->_bbdd = $datos;
        parent::__construct(
            'mysql:host=' . $datos["db_host"] .
            ';dbname=' . $datos["db_name"],
            $datos["db_user"],
            $datos["db_pass"],
            array(
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . $datos["db_char"]
            )
        );
    }
    
    public function filtrarSql($clave)
    {
        $retorno = false;

        if ( ! empty($clave) ) {
            $clave = strip_tags($clave);

            if ( ! get_magic_quotes_gpc() ) {
                $clave = mysqli_real_escape_string(
                    mysqli_connect(
                        $this->_bbdd["db_host"],
                        $this->_bbdd["db_user"],
                        $this->_bbdd["db_pass"]
                    ),
                    $clave
                );
            }

            $retorno = trim($clave);
        }

        return $retorno;
    }
    
    private function _leerCebadorSql( $archivo )
    {
        if (! is_readable($archivo)) {
            return false;
        }
        $lineas = array();
        $handle = fopen($archivo,'r');
        $una = "";
        while ($linea = fgets($handle)) {
            $linea = trim($linea);
            if ($linea != "" && strpos(" " . $linea, "--") != 1) {
                $una .= $linea;
                if (substr($linea, -1, 1) == ";") {
                    $lineas[] = substr($una, 0, -1);
                    $una = "";
                } else {
                    $una .= " ";
                }
            }
        }
        fclose($handle);

        return $lineas;
    }
	
    public function parseSqlFile($archivo)
    {
        $lineas = $this->_leerCebadorSql($archivo);
        foreach($lineas as $linea) {
            $this->query($linea);
        }
    }
}