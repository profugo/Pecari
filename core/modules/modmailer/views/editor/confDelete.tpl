<h2 class="maintitle">Mailer interno<sub>Eliminar plantilla de correo electrónico</sub></h2>
¿Realmente querés elminiar la plantilla <b>{$datos.nombre}</b> ({$datos.clave})?<br>
Te recuerdo que una vez le des clic al botón <code>Eliminar</code> se eliminará efectivamente la plantilla, la operación no se puede deshacer salvo que tengas un respaldo anterior desde el que puedas restaurar la información.
<hr>
<div style="text-align: right">
<a href="{$_layoutParams.root}modmailer/editor" class="btn btn-default btn-secondary">Cancelar</a> &nbsp;
<a href="{$_layoutParams.root}modmailer/editor/eliminar/{$datos.id}/1" class="btn btn-primary">Eliminar</a>
</div>