<h2 class="maintitle">Mailer interno<sub>Editor de plantillas de correo electrónico</sub></h2>

{if count($templates) > 0}
<table class="table table-bordered table-striped table-condensed">
        <tr>
            <th>Plantilla</th>
            <th>Clave</th>
            <th></th>
        </tr>
{foreach from=$templates item=template}
		<tr>
            <td>{$template.nombre}</td>
            <td>{$template.clave}</td>
            <td>
                <a href="{$_layoutParams.root}modmailer/editor/amTemplate/{$template.id}">
                   Editar
                </a>
                -
                <a href="{$_layoutParams.root}modmailer/editor/eliminar/{$template.id}">
                   Eliminar
                </a>
            </td>
        </tr>
{/foreach}
</table>
{else}
<div class="alert alert-info">No hay definidas plantillas personalizadas aún</div>
{/if}

<p><a href="{$_layoutParams.root}modmailer/editor/amTemplate" class="btn btn-primary"><i class="glyphicon glyphicon-plus icon-plus fa fa-plus icon-white"> </i> Agregar plantilla</a></p>