<h2 class="maintitle">Mailer interno<sub>{if isset($editar)}Editar{else}Nueva{/if} plantilla de correo electrónico</sub></h2>

{if isset($error)}
<div class="alert alert-danger">{$error}</div>
{/if}

<form method="post" class="form">
<input type="hidden" name="submit" value="1">
  <div class="form-group">
    <label for="templateName">Nombre de la plantilla</label>
    <input type="text" name="name" class="form-control" id="templateName" placeholder="ej: Mi primer plantilla" value="{$datos.nombre|default:""}" required="required">
  </div>
  
  <div class="form-group">
    <label for="templateClave">Clave de la plantilla</label>
    <input type="text" name="key" class="form-control" id="templateClave" placeholder="ej: primer_plantilla" value="{$datos.clave|default:""}" required="required">
  </div>
  
  <div class="form-group">
    <label for="templateHtml">Código HTML de la plantilla</label>
    <textarea rows="10" name="html" class="form-control" id="templateHtml" placeholder="ej: Hola <b>@USUARIO@</b><br /> Bienvenido a @NOMBRESITIO@">{$datos.html|default:""}</textarea>
  </div>
  
  <div class="form-group">
    <label for="templateTxt">Formato texto plano de la plantilla</label>
    <textarea rows="10" name="txt" class="form-control" id="templateTxt" placeholder="ej: Hola @USUARIO@ \n Bienvenido a @NOMBRESITIO@">{$datos.text|default:""}</textarea>
  </div>
  
  <hr>
  
  <div class="form-group" style="text-align: right">
    <a href="{$_layoutParams.root}modmailer/editor" class="btn btn-default btn-secondary">Cancelar</a> &nbsp; 
    <button class="btn btn-primary" type="submit">{if isset($editar)}Editar{else}Agregar{/if}</button>
  </div>
</form>

{if isset($foco)}
<input type="hidden" id="foco" value="{$foco}">
{/if}