<?php
/**
 * Modelo del controlador index del modulo de registro de usuarios
 *
 * PHP Version 5.4
 *
 * @category Framework.core.modules.mailer
 * @package  Sistema_Basico
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/mailer/models/editorModel.php
 *
 */

/**
 * Modelo del controlador index del modulo de registro de usuarios
 *
 * @category Framework.core.modules.usuarios
 * @package  Sistema_Basico
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/mailer/controllers/editorModel.php
 */

class EditorModel extends Model
{
    
    /**
     * Controlador predeterminado
     * 
     *  @return null
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    
    public function getTemplates()
    {
    	return $this->_db->query(
    			"SELECT * FROM " . FW_PREFIX . "mail_manager ORDER BY nombre, clave"
    		)->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getTemplate($template)
    {
    	return $this->_db->query(
    			"SELECT * FROM " . FW_PREFIX . "mail_manager WHERE id='{$template}'"
    	)->fetch(PDO::FETCH_ASSOC);
    }
    
    public function getTemplateByKey($template)
    {
    	return $this->_db->query(
    		"SELECT * FROM " . FW_PREFIX . "mail_manager WHERE clave='{$template}'"
    	)->fetch(PDO::FETCH_ASSOC);
    }
    
    public function keyExists($clave) 
    {
    	$dato = $this->_db->query(
    		"SELECT id FROM " . FW_PREFIX . "mail_manager WHERE clave='{$clave}'"
    	)->fetch(PDO::FETCH_ASSOC);
    	
    	return isset($dato["id"]) ? true : false;
    }
    
    public function keyExistsB($clave, $id)
    {
    	$dato = $this->_db->query(
    			"SELECT id FROM " . FW_PREFIX . "mail_manager WHERE clave='{$clave}' AND id!='{$id}'"
    			)->fetch(PDO::FETCH_ASSOC);
    	 
    	return isset($dato["id"]) ? true : false;
    }
    
    public function addTemplate($datos)
    {
    	$campos = array();
    	foreach($datos as $clave=>$valor) {
    		$campos[":" . $clave] = $valor;
    	}
    	
    	$this->_db->prepare(
    			"insert into " . FW_PREFIX . "mail_manager values" .
    			"(null, :nombre, :clave, :html, :text)"
    	)->execute($campos);
    }
    
    public function updTemplate($datos, $id)
    {
    	$campos = array();
    	foreach($datos as $clave=>$valor) {
    		$campos[":" . $clave] = $valor;
    	}
    	
    	$this->_db->prepare("
    		UPDATE " . FW_PREFIX . "mail_manager SET
    			nombre = :nombre,
    			clave = :clave,
    			html = :html,
    			text = :text
    		WHERE id = {$id}	
    	")->execute($campos);
    }
    
    public function delTemplate($template)
    {
    	$this->_db->query("DELETE FROM " . FW_PREFIX . "mail_manager WHERE id='{$template}'");
    }
}