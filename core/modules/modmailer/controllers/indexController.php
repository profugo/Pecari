<?php
/**
 * Controlador de modulo de usuarios
 *
 * PHP Version 5.4
 *
 * @category Framework.core.modules.mailer
 * @package  Framework_basico
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/mailer/controllers/indexController.php
 *
 */

/**
 * Controlador de modulo de usuarios
 *
 * @category Framework.core.modules.usuarios
 * @package  Framework_basico
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/mailer/controllers/indexController.php
 */

class indexController extends modmailerController
{
    /**
     * Constructor predeterminado
     * 
     * @return null
     */
    public function __construct() 
    {
        parent::__construct();
    }
    
    /**
     * Index de usuarios
     * Muestra lista de usuarios de sistema
     * 
     * @see usuariosController::index()
     * 
     * @return html
     */
    public function index()
    {
    	$this->redireccionar("modmailer/editor");
    }
}