<?php
/**
 * Controlador de modulo de usuarios
 *
 * PHP Version 5.4
 *
 * @category Framework.core.modules.mailer
 * @package  Framework_basico
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/mailer/controllers/indexController.php
 *
 */

/**
 * Controlador de modulo de usuarios
 *
 * @category Framework.core.modules.usuarios
 * @package  Framework_basico
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/mailer/controllers/indexController.php
 */

class editorController extends modmailerController
{
    /**
     * Constructor predeterminado
     * 
     * @return null
     */
    public function __construct() 
    {
        parent::__construct("editor");
        $this->_view->setCss('modmailer/index/dynCss/noresize', 'D');
    }
    
    /**
     * Index de usuarios
     * Muestra lista de usuarios de sistema
     * 
     * @see usuariosController::index()
     * 
     * @return html
     */
    public function index()
    {
    	$this->_view->assign("templates", $this->_modelo->getTemplates());
    	$this->_view->renderizar("index");
    }
    
    private function _checkDatos($edit = false)
    {
    	$errores = array();
    	$datos = array(
    			"nombre" => trim($this->getSql("name")),
    			"clave"  => trim($this->getSql("key")),
    			"html"   => trim($this->getSql("html", true)),
    			"text"   => trim($this->getSql("txt"))
    	);
    	
    	if ($datos["nombre"] == "") {
    		$errores[] = "Falta el nombre de la plantilla";
    		if (! isset($foco)) {
    			$foco = "Name";
    		}
    	}
    	
    	if ($datos["clave"] == "") {
    		$erroes[] = "Falta la clave para la plantilla";
    		if (! isset($foco)) {
    			$foco = "Clave";
    		}
    	}
    	
    	if ($edit == false) {
	    	if ($this->_modelo->keyExists($datos["clave"]) == true) {
	    		$errores[] = "Ya existe una plantilla con ese nombre";
	    		if (! isset($foco)) {
	    			$foco = "Clave";
	    		}
	    	}
    	} else {
    		if ($this->_modelo->keyExistsB($datos["clave"], $edit) == true) {
    			$errores[] = "Ya existe una plantilla con ese nombre";
    			if (! isset($foco)) {
    				$foco = "Clave";
    			}
    		}
    	}
    	
    	if ($datos["html"] == "" && $datos["text"] == "") {
    		$errores[] = "Tenés que agregar al menos uno de los modos de plantilla, html o texto plano";
    		if (! isset($foco)) {
    			$foco = "Html";
    		}
    	}
    	
    	return array("datos" => $datos, "errores" => $errores, "foco" => $foco);
    }
    
    public function amTemplate($template = false)
    {
    	if ($template != false) {
    		$this->_view->assign("editar", true);
    	}
    	
    	$envio = $this->getInt("submit");
    	if ($envio == 1) {
    		
    		$checkDatos = $this->_checkDatos($template);
    		
    		if (count($checkDatos["errores"]) > 0) {
    			$checkDatos["datos"]["html"] = stripcslashes(str_replace('\r\n', "\n", $checkDatos["datos"]["html"]));
    			$checkDatos["datos"]["text"] = stripcslashes(str_replace('\r\n', "\n", $checkDatos["datos"]["text"]));
    			
    			$this->_view->setJs("editor");
    			$this->_view->assign("error", join($checkDatos["errores"], "<br>"));
    			$this->_view->assign("datos", $checkDatos["datos"]);
    			$this->_view->assign("foco", $checkDatos["foco"]);
    		} else {
    			if ($template == false) {
	    			$this->_modelo->addTemplate($checkDatos["datos"]);
    			} else {
    				$this->_modelo->updTemplate($checkDatos["datos"], $template);
    			}
    			$this->redireccionar("modmailer/editor");
    			return;
    		}
    	} else {
    		if ($template != false) {
    			$template = $this->filtrarInt($template);
    			if ($template == 0) {
    				$this->redireccionar("error/5050");
    				return;
    			}
    			 
    			$datos = $this->_modelo->getTemplate($template);
    			 
    			if (! $datos) {
    				$this->redireccionar("error/5050");
    				return;
    			}
    			 
    			$datos["html"] = stripcslashes(str_replace('\r\n', "\n", $datos["html"]));
    			$datos["text"] = stripcslashes(str_replace('\r\n', "\n", $datos["text"]));
    			
    			$this->_view->assign("datos", $datos);
    		}
    	}
    	
    	$this->_view->renderizar("amTemplate");
    }
    
    
    public function eliminar($template, $doDelete = false)
    {
    	$template = $this->filtrarInt($template);
    	if ($template == 0) {
    		$this->redireccionar("error/5050");
    		return;
    	}
    	
    	$datos = $this->_modelo->getTemplate($template);
    	
    	if (! $datos) {
    		$this->redireccionar("error/5050");
    		return;
    	}
    	
    	if ($doDelete != false) {
    		$this->_modelo->delTemplate($template);
    		$this->redireccionar("modmailer/editor");
    		return;
    	}
    	
    	$this->_view->assign("datos", $datos);

    	$this->_view->renderizar("confDelete");
    }
}