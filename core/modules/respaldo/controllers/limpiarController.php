<?php

class LimpiarController extends respaldoController
{	
	/**
	 * Constructor predeterminado
	 *
	 * @return null
	 */
	public function __construct()
	{
		parent::__construct("limpiar");
		$this->_view->setCss('usuarios/login/dynCss/noresize', 'D');
	}

	public function index()
	{
		$this->_view->renderizar("warning");
	}
	
	public function doLimpiar()
	{
		$this->_modelo->limpiarUsuarios();
		$this->_modelo->optimizarTablas();
		
		$this->_view->renderizar("pronto");
	}
}
