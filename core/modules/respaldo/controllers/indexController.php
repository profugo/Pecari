<?php

class IndexController extends respaldoController
{

	private $_mysqldump;
	private $_params;
	private $_archivo;
	private $_archivo_nombre;
	private $_database;
	private $_algoritmos  = array(
			1 => array( "name" => "None", "ext" => ".sql"),
			2 => array( "name" => "Gzip", "ext" => ".sql.gz"),
			3 => array( "name" => "Bzip2", "ext" => ".sql.bz2")
		);

	/**
	 * Constructor predeterminado
	 *
	 * @return null
	 */
	public function __construct()
	{
		parent::__construct();
		$this->getLibrary("mysqldump/class.mysqldump.wrapper");
		$this->_mysqldump = new MysqldumpWrapper();
		
		$this->_conf->load("respaldo");
		$this->_conf->load("database");
		
		$this->_params = $this->_conf->branch("respaldo");
		if (isset($this->_params["filename"])) {
			$this->_archivo = $this->_params["filename"];
			unset($this->_params["filename"]);
		}
		
		$this->_archivo_nombre = $this->_archivo;
		
		$this->_database = array(
			"db"   => $this->_conf->item("database", "db_name"),
			"user" => $this->_conf->item("database", "db_user"),
			"pass" => $this->_conf->item("database", "db_pass"),
			"host" => $this->_conf->item("database", "db_host")
		);
		
		$ruta = defined('PILL_PATH') ? PILL_PATH : ROOT;
		
		if (is_writable($ruta . 'tmp') == false) {
			throw new Exception("No se puede crear el archivo de respaldo");
		}
		$this->_view->setCss('usuarios/login/dynCss/noresize', 'D');
	}
	
	private function dumpFile()
	{
		$ruta = defined('PILL_PATH') ? PILL_PATH : ROOT;
		$this->_archivo = $ruta . 'tmp' . DS . $this->_archivo . '.sql';
		
		$this->_mysqldump->dump($this->_database, $this->_params, $this->_archivo);
	}
	
	public function index()
	{
		
		$compresion = $this->getInt("compresion");
		
		if ($compresion > 0) {
			$this->_view->renderizar("dumping");
			$this->_params["compress"] = $this->_algoritmos[$compresion]["name"];
			$this->dumpFile();
			echo("<script>document.location='" . BASE_URL . "respaldo/index/done/" . $compresion . "';</script>");
			exit;
		}
		
		$this->_view->assign("nombre", $this->_database["db"] );
		$this->_view->renderizar("index");

	}

	public function done($compresion)
	{
		$archivo = BASE_URL . "tmp/" . $this->_archivo_nombre . $this->_algoritmos[$compresion]["ext"];
		$this->_view->assign("archivo", $archivo);
		$this->_view->renderizar("done");
	}
}