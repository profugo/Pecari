<?php

class RestoreController extends respaldoController
{
	private $_model;
	private $_types = array('application/sql', 'application/gzip', 'application/x-bzip');
	private $_uploadDir; 
	private $_params;
	private $_mode;
	private $_handler;
	private $_filesize = 0;
	private $_config;
	
	/**
	 * Constructor predeterminado
	 *
	 * @return null
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_conf->load("database");
		$this->_conf->load("restore");
		$this->_config = $this->_conf->branch("restore");
		
		$this->_model = $this->loadModel("restore");
		$this->loadHelper("global");
		$ruta = defined('PILL_PATH') ? PILL_PATH : ROOT;
		$this->_uploadDir = $ruta . 'tmp' . DS;
		$this->_view->setCss('usuarios/login/dynCss/noresize', 'D');
	}
	
	private function _getMaxUpload()
	{
		$maximo    = ini_get("upload_max_filesize");
		$max_human = $maximo;
		
		if (preg_match("/([0-9]+)K/i", $maximo, $tempregs)) {
			$maximo = $tempregs[1] * 1024;
		}
		if (preg_match("/([0-9]+)M/i", $maximo, $tempregs)) {
			$maximo = $tempregs[1] * 1024 * 1024;
		}
		if (preg_match("/([0-9]+)G/i", $maximo, $tempregs)) {
			$maximo= $tempregs[1] * 1024 * 1024 * 1024;
		}
		
		return array("max" => $maximo, "human" => $max_human);
	}
	
	private function _isUpload()
	{
		if (isset($_FILES['archivo'])) {
			if (! in_array($_FILES['archivo']['type'], $this->_types)) {
				$this->_view->assign("error", "ERROR: Tipo de archivo no soportado, solo se aceptan archivos .sql .gz o .bz2");
			} else {
				$uploadfile = $this->_uploadDir . basename($_FILES['archivo']['name']);
			
				if (!move_uploaded_file($_FILES['archivo']['tmp_name'], $uploadfile)) {
					$this->_view->assign("error", "ERROR: No se pudo copiar el archivo al directorio de proceso");
				} else {
					$this->_view->assign("archivo", basename($_FILES['archivo']['name']));
					$this->_view->setJs(array("restore"));
					$this->_view->renderizar("conf_restore");
					return true;
				}
			}
		}
		return false;
	}
	
	public function index()
	{
		$this->_view->assign("nombre", $this->_conf->item("database", "db_name"));
		
		if($this->_isUpload() == true) {
			exit;
		}
		
		$maximo = $this->_getMaxUpload();
		
		$this->_view->assign("maximo", $maximo["max"]);
		$this->_view->assign("max_human", $maximo["human"]);
		$this->_view->assign("encoding", $this->_conf->item("database", "db_char"));
		$this->_view->renderizar("index");
	}

	private function _detectMode()
	{
		if (preg_match("/\.gz$/i", $this->_params["res_filename"]))
		{ 
			$this->_mode = "gzip";
		} elseif (preg_match("/\.bz2$/i", $this->_params["res_filename"]))
		{
			$this->_mode = "bzip2";
		} else {
			$this->_mode = "sql";
		}
	}
	
	private function _expandFile()
	{
		$fn   = $this->_params["res_filename"];
		$of   = $fn;
		$ruta = $this->_uploadDir;
		
		switch ($this->_mode)
		{
			case "gzip" :
				$of = substr($fn, 0, -3);
				gunzip($ruta . $fn, $ruta . $of);
				break;
			case "bzip2" :
				$of = substr($fn, 0, -4);
				bunzip2($ruta . $fn, $ruta . $of);
				break;
		}
		
		if ($of != $fn) {
			if(is_readable($ruta . $fn)) {
				unlink($ruta . $fn);
			} else {
				$this->_dumpError("INESPERADO: No se encuentra el archivo " . $fn);
				exit;
			}
			$this->_params["res_filename"] = $of;
		}
	}
	
	private function _openFile()
	{
		$archivo = $this->_uploadDir . $this->_params["res_filename"];
		$this->_handler = fopen($archivo, "r");
		
		if($this->_handler == false) {
			throw new Exception("No pude abrir el archivo " . $archivo . " para importarlo.");
			exit;
		}
	}
	
	private function _getFilesize()
	{
		@fseek($this->_handler, 0, SEEK_END);
		$this->_filesize = ftell($this->_handler);
	}
	
	private function _dumpError($error)
	{
		echo "<span class='error'>{$error}</span>\n";
	}
	
	private function _doStats($data)
	{
		$lines_this   = $data["linenumber"] - $this->_params["res_start"];
		$lines_done   = $data["linenumber"] - 1;
		$lines_togo   = ' ? ';
		$lines_tota   = ' ? ';
			
		$queries_this = $data["queries"];
		$queries_done = $data["totalqueries"];
		$queries_togo = ' ? ';
		$queries_tota = ' ? ';
			
		$bytes_this   = $data["foffset"] - $this->_params["res_foffset"];
		$bytes_done   = $data["foffset"];
		$kbytes_this  = round($bytes_this / 1024,2);
		$kbytes_done  = round($bytes_done / 1024,2);
		$mbytes_this  = round($kbytes_this / 1024,2);
		$mbytes_done  = round($kbytes_done / 1024,2);
			
		$bytes_togo  = $this->_filesize - $data["foffset"];
		$bytes_tota  = $this->_filesize;
		$kbytes_togo = round($bytes_togo / 1024,2);
		$kbytes_tota = round($bytes_tota / 1024,2);
		$mbytes_togo = round($kbytes_togo / 1024,2);
		$mbytes_tota = round($kbytes_tota / 1024,2);
			
		$pct_this   = ceil($bytes_this / $this->_filesize * 100);
		$pct_done   = ceil($data["foffset"] / $this->_filesize * 100);
		$pct_togo   = 100 - $pct_done;
		$pct_tota   = 100;
		
		if ($bytes_togo==0)
		{
			$lines_togo   = '0';
			$lines_tota   = $data["linenumber"] - 1;
			$queries_togo = '0';
			$queries_tota = $data["totalqueries"];
		}

		$stats = array(
				"lines_this"   => $lines_this,
				"lines_done"   => $lines_done,
				"lines_togo"   => $lines_togo,
				"lines_tota"   => $lines_tota,
					
				"queries_this" => $queries_this,
				"queries_done" => $queries_done,
				"queries_togo" => $queries_togo,
				"queries_tota" => $queries_tota,
					
				"bytes_this"   => $bytes_this,
				"bytes_done"   => $bytes_done,
				"kbytes_this"  => $kbytes_this,
				"kbytes_done"  => $kbytes_done,
				"mbytes_this"  => $mbytes_this,
				"mbytes_done"  => $mbytes_done,
					
				"bytes_togo"  => $bytes_togo,
				"bytes_tota"  => $bytes_tota,
				"kbytes_togo" => $kbytes_togo,
				"kbytes_tota" => $kbytes_tota,
				"mbytes_togo" => $mbytes_togo,
				"mbytes_tota" => $mbytes_tota,
					
				"pct_this"   => $pct_this,
				"pct_done"   => $pct_done,
				"pct_togo"   => $pct_togo,
				"pct_tota"   => $pct_tota
		);
		
		if ($data["linenumber"] < $this->_params["res_start"] + $this->_config["linespersession"]) {
			$this->_view->assign("ended", true);
		} else {
			if ($this->_config["delaypersession"] != 0) {
				$this->_view->assign("delay", $this->_config["delaypersession"]);
			}
		}
		
		$this->_view->assign("stats", $stats);
		$this->_view->assign("data", $data);
		$this->_view->renderizar("stats", false, true);
	}
	
	private function _doIteration()
	{
		$error = false;
		
		// Check $this->_params["res_foffset"] upon $this->_filesize
		if ($this->_params["res_foffset"] > $this->_filesize) {
			$this->_dumpError("INESPERADO: No puedo ubicar el puntero de archivo mas allá del fin del archivo");
			$error = true;
		}
		
		// Set file pointer to $this->_params["res_foffset"]
		if(!$error && (fseek($this->_handler, $this->_params["res_foffset"]) != 0)) {
			$this->_dumpError("INESPERADO: No puedo ubicar el puntero de archivo en la posición: " . $this->_params["res_foffset"]);
			$error = true;
		}
		
		// Start processing queries from $$this->_handler
		if (!$error) {
			$query        = "";
			$queries      = 0;
			$totalqueries = $this->_params["res_queries"];
			$linenumber   = $this->_params["res_start"];
			$querylines   = 0;
			$inparents    = false;
			
			// Stay processing as long as the $this->_config["linespersession"] is not reached or 
			// the query is still incomplete
			while ($linenumber < $this->_params["res_start"] + $this->_config["linespersession"] || $query != "") {
				
				// Read the whole next line
				$dumpline = "";
				while (!feof($this->_handler) && substr($dumpline, -1) != "\n" && substr($dumpline, -1) != "\r") {
					$dumpline .= fgets($this->_handler, $this->_config["data_chunk_length"]);
				}
				if ($dumpline === "") {
					break;
				}
				
				// Remove UTF8 Byte Order Mark at the file beginning if any
				if ($this->_params["res_foffset"] == 0) {
					$dumpline = preg_replace('|^\xEF\xBB\xBF|', '', $dumpline);
				}
				
				// Handle DOS and Mac encoded linebreaks (I don't know if it really works on Win32 or Mac Servers)
				$dumpline = str_replace("\r\n", "\n", $dumpline);
				$dumpline = str_replace("\r", "\n", $dumpline);
				
				// Recognize delimiter statement
				if (!$inparents && strpos($dumpline, "DELIMITER ") === 0) {
					$this->_config["delimiter"] = str_replace("DELIMITER ", "", trim($dumpline));
				}
				
				
				// Skip comments and blank lines only if NOT in parents				
				if (!$inparents) {
					$skipline = false;
					reset($this->_config["comment"]);
					foreach ($this->_config["comment"] as $comment_value) {
						if (trim($dumpline) == "" || strpos(trim($dumpline), $comment_value) === 0) {
							$skipline = true;
							break;
						}
					}
					if ($skipline) {
						$linenumber++;
						continue;
					}
				}
				
				// Remove double back-slashes from the dumpline prior to count the quotes ('\\' can only be within strings)
				$dumpline_deslashed = str_replace("\\\\", "", $dumpline);
				
				// Count ' and \' (or " and \") in the dumpline to avoid query break within a text field ending by $delimiter
				$parents = substr_count($dumpline_deslashed, $this->_config["string_quotes"]) - substr_count($dumpline_deslashed, "{$this->_config["string_quotes"]}");
				if ($parents % 2 != 0) {
					$inparents = !$inparents;
				}
				
				// Add the line to query
				$query .= $dumpline;
				
				// Don't count the line if in parents (text fields may include unlimited linebreaks)
				if (!$inparents) {
					$querylines++;
				}

				// Stop if query contains more lines as defined by $max_query_lines
				if ($querylines > $this->_config["max_query_lines"]) {
					$this->_dumpError("Detenido en la línea $linenumber. <br>" .
							   "En este punto la consulta actual tiene mas de {$this->_config["max_query_lines"]} " .
							   "líneas volcadas. Esto puede pasar si su volcado fué creado por alguna herramienta que no " .
							   "coloque un punto y coma seguida de un salto de línea al final de cada consulta, " .
							   "o si su volcado contiene inserciones extendidas o definiciones de procedimientos muy largos.");
					$error = true;
					break;
				}

				// Execute query if end of query detected ($delimiter as last character) AND NOT in parents
				if ((preg_match('/'.preg_quote($this->_config["delimiter"],'/').'$/',trim($dumpline)) || $this->_config["delimiter"]=='') && !$inparents) {
				
					// Cut off delimiter of the end of the query				
					$query = substr(trim($query), 0, -1 * strlen($this->_config["delimiter"]));
					
					if (!$this->_model->processQuery($query)) {
						$this->_dumpError("Error at the line $linenumber: ". trim($dumpline)."<br>" .
								   "Query: ".trim(nl2br(htmlentities($query)))."<br>");
						$error = true;
						break;
					}
					$totalqueries++;
					$queries++;
					$query      = "";
					$querylines = 0;
				}
				$linenumber++;
			}
		}
		
		// Get the current file position
		if (!$error) {
			$foffset = ftell($this->_handler);
		}
		if (!$foffset) {
			$this->_dumpError("INESPERADO: No puedo leer el desplazamiendo del puntero de archivo");
			$error = true;
		}
		
		// Print statistics
		if (!$error) {
			$this->_doStats(array(
				"linenumber"   => $linenumber,
				"queries"      => $queries,
				"totalqueries" => $totalqueries,
				"foffset"      => $foffset,
				"delimiter"    => $this->_params["res_delimiter"],
				"fn"           => $this->_params["res_filename"],
				"delay"        => $this->_config["delaypersession"]
			));
				
		}
	}
	
	public function process() 
	{
		$command = $this->getSql("command");
		if ($command == "cancel") {
			$this->redireccionar("respaldo/restore");
			exit;
		}
		
		@ini_set('auto_detect_line_endings', true);
		@set_time_limit(0);
		
		$this->_params = array(
				"res_start"     => $this->getInt("start"),
				"res_filename"  => $this->getSql("fn"),
				"res_foffset"   => $this->getInt("foffset"),
				"res_queries"   => $this->getInt("totalqueries"),
				"res_delimiter" => $this->getSql("delimiter") != 0 ? $this->getSql("delimiter") : $this->_config["delimiter"]
			);
		
		$this->_view->assign("params", $this->_params);
		$this->_view->renderizar("restoring", false, true);

		$this->_detectMode();
		$this->_expandFile();
		$this->_openFile();
		$this->_getFilesize();
		$this->_doIteration();
	}
}