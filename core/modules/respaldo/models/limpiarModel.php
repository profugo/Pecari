<?php
/**
 * Modelo del controlador registro del modulo de limpieza de tablas
 *
 * PHP Version 5.4
 *
 * @category Framework.core.modules.usuarios
 * @package  Framework_base
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/usuarios/models/limpiarModel.php
 *
 */

/**
 * Modelo del controlador registro del modulo de limpieza de tablas
 *
 * @category Framework.core.modules.usuarios
 * @package  Framework_base
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/usuarios/models/limpiarModel.php
 */

class limpiarModel extends Model
{
    /**
     * Constructor predeterminado
     * 
     * @return null
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Elimina los registros en la tabla usuarios_bloqueo y en usuarios de los usuarios que tengan estado 0
     * (no activado) y hayan creado la cuenta hace mas de 2 dias
     */
    public function limpiarUsuarios()
    {
    	$datos = $this->_db->query(
    		"SELECT id FROM " . FW_PREFIX . "usuarios WHERE estado=0 AND DATEDIFF(NOW(), fecha) > 2"
    	)->fetchAll(PDO::FETCH_ASSOC);
    	
    	if (is_array($datos) && count($datos) > 0) {
    		foreach($datos as $usuario) {
    			$id = $usuario["id"];
    			$this->_db->query("DELETE FROM " . FW_PREFIX . "usuarios_bloqueo WHERE usuario='{$id}'");
    		}
    	}
    	
    	$this->_db->query("DELETE FROM " . FW_PREFIX . "usuarios WHERE estado=0 AND DATEDIFF(NOW(), fecha) > 2");
    }
    
    /**
     * Optimiza todas las tablas del sistema, las predeterminadas y las creadas para el programa en uso
     */
    public function optimizarTablas()
    {
    	$datos = $this->_db->query("SHOW TABLES")->fetchAll(PDO::FETCH_NUM);
    	
    	foreach($datos as $tabla) {
    		$this->_db->query("OPTIMIZE TABLE " . $tabla[0]);
    	}
    }
}