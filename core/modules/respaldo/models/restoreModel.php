<?php

class RestoreModel extends Model
{

	/**
	 * Controlador predeterminado
	 *
	 *  @return null
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	public function processQuery($query)
	{
		return $this->_db->query($query);
	}
}