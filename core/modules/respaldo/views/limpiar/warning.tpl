<h2 class="maintitle">Base de datos<sub>Optimizar y limpiar las tablas</sub></h2>

Si hacés clic en el botón <code>Limpiar</code> se van a eliminar los registros de usuario que no se hayan activado luego de 48hs y se va a aplicar el método OPTIMIZE TABLE a todas las tablas del sistema.
<hr>
<div style="float:right">
<a href="{$_layoutParams.root}" class="btn btn-default  btn-secondary">Cancerlar</a>
<a href="{$_layoutParams.root}respaldo/limpiar/doLimpiar" class="btn btn-success">Limpiar</a>
</div> 