<h2 class="maintitle">Base de datos<sub>Respaldo de base de datos</sub></h2>

<form method="post">
	<table class="table table-striped table-condensed table-bordered">
		<tr>
			<td>
				<b>Base de datos:</b>
			</td>
			<td style="width:80%">
				{$nombre}
			</td>
		</tr>
		<tr>
			<td>
				<b>Compresión:</b>
			</td>
			<td>
				<select name="compresion">
					<option value="1">Ninguna</option>
					<option value="2">Compresión gzip</option>
					<option value="3">Compresión bzip2</option>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan=2>
				<button class="btn btn-default">Aceptar</button>
			</td>
		</tr>
	</table>
</form>
