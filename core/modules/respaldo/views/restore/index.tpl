<h2 class="maintitle">Base de datos<sub>Restaurar base de datos</sub></h2>

<form enctype="multipart/form-data" method="post">
	<table class="table table-striped table-condensed table-bordered">
{if isset($error)}
		<tr>
			<td colspan=2 style="color:#f00;font-weight:bold;text-align:center">
				{$error}
			</td>
		</tr>
{/if}
		<tr>
			<td>
				<b>Base de datos:</b>
			</td>
			<td style="width:80%">
				{$nombre}
			</td>
		</tr>
		<tr>
			<td>
				<b>Archivo:</b>
			</td>
			<td>
				<input type="file" name="archivo">
			</td>
		</tr>
		<tr>
			<td>
				<b></b>
			</td>
			<td>
				Sólo se aceptan archivos de tipo sql, gz o bz2 de hasta {$max_human} ({$maximo} bytes).<br>
				Tenga en cuenta que esta base de datos sólo acepta codificación <i>{$encoding}</i>, el archivo de respaldo debe estar en la misma codificación.
			</td>
		</tr>
		<tr>
			<td colspan=2>
				<button class="btn btn-default btn-secondary">Aceptar</button>
			</td>
		</tr>
	</table>
</form>
