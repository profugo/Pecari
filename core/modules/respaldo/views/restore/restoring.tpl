<html>
<head>
	<meta name="viewport" content = "width = device-width, initial-scale = 1.0, minimum-scale = 1.0, maximum-scale = 1.0, user-scalable = no" />
    <meta charset="utf-8">
    <link href="{$_layoutParams.ruta_css}bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="{$_layoutParams.ruta_css}bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
    <link href="{$_layoutParams.ruta_css}docs.css" rel="stylesheet" type="text/css">
    <link href="{$_layoutParams.ruta_css}docs_admini.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="{$_layoutParams.ruta_fw}public/js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="{$_layoutParams.ruta_fw}public/js/bootstrap-3.0.0.min.js"></script>
<style>
body{
	font-family:arial;
}
.error{
	color: #f00;
	font-weight: bold;
}
</style>
</head>
<body>
	<h3>Restaurando base de datos</h3>
	<hr>
	<h5>Procesanso archivo {$params.res_filename}...</h5>
	<form enctype="multipart/form-data" id="restoring" method="post">
	
	
	<input type="hidden" name="start" id="start" value="{$params.res_start|default:0}">
	<input type="hidden" name="fn" id="fn" value="{$params.res_filename|default:0}">
	<input type="hidden" name="foffset" id="foffset" value="{$params.res_foffset|default:0}">
	<input type="hidden" name="totalqueries" id="totalqueries" value="{$params.res_queries|default:0}">
	<input type="hidden" name="delimiter" id="delimiter" value="{$params.res_delimiter|default:0}">
	
	</form>
