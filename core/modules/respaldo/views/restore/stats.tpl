    <table class="table table-striped table-bordered table-condensed">
	    <tr>
	    	<th> </th>
	    	<th>Sesión</th>
	    	<th>Hecho</th>
	    	<th>Falta</th>
	    	<th>Total</th>
	    </tr>
	    
	    <tr>
	    	<th>Líneas</th>
	    	<td>{$stats.lines_this}</td>
	    	<td>{$stats.lines_done}</td>
	    	<td>{$stats.lines_togo}</td>
	    	<td>{$stats.lines_tota}</td>
	    </tr>
	    
	    <tr>
	    	<th>Consultas</th>
	    	<td>{$stats.queries_this}</td>
	    	<td>{$stats.queries_done}</td>
	    	<td>{$stats.queries_togo}</td>
	    	<td>{$stats.queries_tota}</td>
	    </tr>
	    
	    <tr>
	    	<th>Bytes</th>
	    	<td>{$stats.bytes_this}</td>
	    	<td>{$stats.bytes_done}</td>
	    	<td>{$stats.bytes_togo}</td>
	    	<td>{$stats.bytes_tota}</td>
	    </tr>
	    
	    <tr>
	    	<th>KB</th>
	    	<td>{$stats.kbytes_this}</td>
	    	<td>{$stats.kbytes_done}</td>
	    	<td>{$stats.kbytes_togo}</td>
	    	<td>{$stats.kbytes_tota}</td>
	    </tr>
	    
	    <tr>
	    	<th>MB</th>
	    	<td>{$stats.mbytes_this}</td>
	    	<td>{$stats.mbytes_done}</td>
	    	<td>{$stats.mbytes_togo}</td>
	    	<td>{$stats.mbytes_tota}</td>
	    </tr>
	    
	    <tr>
	    	<th>%</th>
	    	<td>{$stats.pct_this}</td>
	    	<td>{$stats.pct_done}</td>
	    	<td>{$stats.pct_togo}</td>
	    	<td>{$stats.pct_tota}</td>
	    </tr>
	    
	    <tr>
	    	<th>Avance (%)</th>
	    	<td colspan="4">
	    		<div class="progress">
  					<div class="bar progress-bar" role="progressbar" aria-valuenow="{$stats.pct_done}" aria-valuemin="0" aria-valuemax="{$stats.pct_tota}" style="width: {$stats.pct_done}%;">
    				{$stats.pct_done}%
					</div>
				</div>
	    	</td>
	    </tr>
    </table>
    
{if isset($ended)}
<div class="alert alert-success">
<h4>Felicidades</h4>
Se alcanzó el final del archivo, el proceso ha finalizado correctamente.
</div>
{else}
{if isset($delay)}
<div class="alert">
Ahora voy a esperar {$delay} milisegundos para iniciar una nueva sesión de carga de datos.
</div>
{/if}

<script language="JavaScript" type="text/javascript">
    $("#start").val({$data.linenumber});
    $("#fn").val("{$data.fn}");
    $("#foffset").val({$data.foffset});
    $("#totalqueries").val({$data.totalqueries});
    $("#delimiter").val("{$data.delimiter}");
    
    window.setTimeout("$('#restoring').submit()",500 + {$data.delay});
</script>
   
<div class="alert">
	Haz clic en <b><a href="{$_layoutParams.root}" target="_top">ALTO</a></b> para abortar la importación o espera unos instantes
</div>

{/if}


<script>window.scrollTo(0,document.body.scrollHeight);</script>