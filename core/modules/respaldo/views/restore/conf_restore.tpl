<h2 class="maintitle">Base de datos<sub>Restaurar base de datos</sub></h2>

<form enctype="multipart/form-data" method="post" id="form" target="i1" action="{$_layoutParams.root}respaldo/restore/process">
	<table class="table table-striped table-condensed table-bordered">
		<tr>
			<td colspan=2>
				Tenga en cuenta que la restauración usa un método no acumulativo, las tablas y/o procedimientos que estén en la base de datos y en el respaldo serán eliminadas de la base de datos y reemplazadas por la versión existente en el respaldo que se está restaurando.
			</td>
		</tr>
		<tr>
			<td>
				<b>Base de datos:</b>
			</td>
			<td style="width:80%">
				{$nombre}
			</td>
		</tr>
		<tr>
			<td>
				<b>Archivo:</b>
			</td>
			<td>
				{$archivo}
			</td>
		</tr>
		<tr>
			<td colspan=2>
				<input type="hidden" name="fn" value="{$archivo}">
				<button class="btn btn-default btn-secondary" name="command" value="accept">Aceptar</button>
				<button class="btn btn-default btn-secondary" name="command" value="cancel">Cancelar</button>
			</td>
		</tr>
	</table>
</form>

<iframe src="" name="i1" id="i1" style="width:100%;border:1px solid #ddd;height:256px;display:none"></iframe>