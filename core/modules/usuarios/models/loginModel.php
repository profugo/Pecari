<?php
/**
 * Modelo del controlador login del modulo de registro de usuarios
 *
 * PHP Version 5.4
 *
 * @category Framework.core.modules.usuarios
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/usuarios/models/loginModel.php
 *
 */

/**
 * Modelo del controlador login del modulo de registro de usuarios
 *
 * @category Framework.core.modules.usuarios
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/usuarios/registros/loginModel.php
 */

class LoginModel extends Model
{
    /**
     * Constructor predeterminado
     * 
     * @return null
     */
    public function __construct()
    {
        parent::__construct();
    }
       
    /**
     * Obtener datos de un usuario filtrado por usuario y clave
     * 
     * @param string $usuario  nombre de usuario
     * @param string $password clave en plano
     * 
     * @return array datos de usuario
     */
    public function getUsuario($usuario, $password)
    {
        $hash = Hash::getHash('sha1', $password, HASH_KEY);
        if ($this->_safePass == false) {
            $hash = sha1($password);
        }
     
        $datos = $this->_db->query(
            "select * from " . FW_PREFIX . "usuarios " .
            "where usuario = '$usuario' " .
            "and pass = '{$hash}'"
        )->fetch();

        if (isset($datos["errors"])) {
        	$this->_db->query("UPDATE " . FW_PREFIX . "usuarios SET errors=0 WHERE id='" . $datos["id"] . "'");
        }
        
        return $datos;
    }
    
    /**
     * Marcar en base de datos la fecha en que se limpio la cache
     * 
     * @return bool true si actualizo base de datos
     */
    public function clearCache()
    {
        $retorno = false;
        $time  = strtotime($this->getGen('cache_clear'));
        $now   = time();
        $diff  = ($now - $time );
        if ( $diff > 30 || $diff < 0 ) {
        	$this->setGen("cache_clear", $now);
            $retorno = true;
        }
        return $retorno;
    }
    
    public function incError($usuario, $limit)
    {
    	$ret = false;
    	$dato = $this->_db->query(
    		"SELECT id, errors FROM " . FW_PREFIX . "usuarios WHERE usuario='{$usuario}'")->fetch(PDO::FETCH_ASSOC
    	);
    	
    	if (isset($dato["id"])) {
    		if ((int)$dato["errors"] < $limit) {
    			$this->_db->query(
    				"UPDATE " . FW_PREFIX . "usuarios SET errors='" . ((int)$dato["errors"] + 1) . "' WHERE id='" . $dato["id"] . "'"
    			);
    		} else {
    			$this->_db->query(
    				"UPDATE " . FW_PREFIX . "usuarios SET estado=2 WHERE id='" . $dato["id"] . "'"
    			);
    			$lock = $this->_getLocked($dato["id"]);
    			if ($lock == false) {
	    			$this->_db->query("
	    				INSERT INTO " . FW_PREFIX . "usuarios_bloqueo (usuario, fecha, ip) VALUES " .
	    					"(" . $dato["id"] . ", NOW(), '" . $_SERVER["REMOTE_ADDR"] . "')
	    			");
    			} else {
    				$ret = $lock["fecha"];
    			}
    		}
    	}
    	return $ret;
    }
    
    public function checkUnlock($usuario)
    {
    	$ret = false;
    	$bloqueo = $this->_getLocked($usuario);
    	if (isset($bloqueo["fecha"])) {
    		$actual =  new DateTime();
    		if ($bloqueo["fecha"] >= $actual->format("Y-m-d H:i:s")) {
    			echo "desbloqueame papa";	
    		} else {
    			$ret = $bloqueo["fecha"];
    		}
    	}
    	
    	return $ret;
    }
    
    public function doUnlock($usuario)
    {
    	$this->_db->query("UPDATE " . FW_PREFIX . "usuarios SET estado=1, errors=0 WHERE id ='{$usuario}'");
    	$this->_db->query("DELETE FROM " . FW_PREFIX . "usuarios_bloqueo WHERE usuario ='{$usuario}'");
    }
    
    private function _getLocked($usuario)
    {
    	return $this->_db->query(
    		"SELECT fecha FROM " . FW_PREFIX . "usuarios_bloqueo WHERE usuario = '{$usuario}'"
    	)->fetch(PDO::FETCH_ASSOC);
    }
}
