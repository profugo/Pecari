<?php
/**
 * Modelo del controlador registro del modulo de registro de usuarios
 *
 * PHP Version 5.4
 *
 * @category Framework.core.modules.usuarios
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/usuarios/models/registroModel.php
 *
 */

/**
 * Modelo del controlador registro del modulo de registro de usuarios
 *
 * @category Framework.core.modules.usuarios
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/usuarios/models/registroModel.php
 */

class RegistroModel extends Model
{
    /**
     * Constructor predeterminado
     * 
     * @return null
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Verificar nombre de usuario
     * 
     * @param string $usuario nombre de usuario
     * 
     * @return array(id, codigo) de tabla usuarios
     */
    public function verificarUsuario($usuario)
    {
        return $this->_db->query(
            "select id, codigo from " . FW_PREFIX . "usuarios where usuario = '$usuario'"
        )->fetch(PDO::FETCH_ASSOC);
    }
    
    /**
     * Verificar mail de usuario
     * 
     * @param string $email email del usuario
     * 
     * @return bool true si el correo está asociado al usuario
     */
    public function verificarEmail($email)
    {
        return $this->_db->query(
            "select id from " . FW_PREFIX . "usuarios where email = '$email'"
        )->fetch(PDO::FETCH_ASSOC);
    }
    
    /**
     * Obtener el rol predeterminado para usuarios nuevos
     * 
     * @return string nombre de rol
     */
    public function getDefaultRole()
    {
        return $this->getGen('default_role');
    }
    
    public function getRoles()
    {
    	$data = $this->_db->query("SELECT * FROM " . FW_PREFIX . "roles")->fetchAll(PDO::FETCH_ASSOC);
    	return $data;
    }
    
    public function updUsuario($id, $datos)
    {
    	foreach($datos as $clave=>$valor) {
    		$this->_db->query("UPDATE " . FW_PREFIX . "usuarios SET {$clave}='{$valor}' WHERE id={$id}");
    	}
    }
    /**
     * Dar de alta un nuevo usuario
     * 
     * @param string $nombre   nombre personal del usuario
     * @param string $usuario  nombre de usuario
     * @param string $password contraseña
     * @param string $email    email del usuario
     * 
     * @return null
     */
    public function registrarUsuario($nombre, $usuario, $password, $email, $role )
    {
        $random = rand(1782598471, 9999999999);
        $role   = $role > 0 ? $role : $this->getDefaultRole();
        $hash = Hash::getHash('sha1', $password, HASH_KEY);
        if ($this->_safePass == false) {
            $hash = sha1($password);
        }
        $this->_db->prepare(
            "insert into " . FW_PREFIX . "usuarios values" .
            "(null, :nombre, :usuario, :password, " .
            ":email, :role, 0, now(), :codigo, 0)"
        )->execute(
            array(
                ':nombre' => $nombre,
                ':usuario' => $usuario,
                ':password' => $hash,
                ':email' => $email,
                ':role' => $role,
                ':codigo' => $random
            )
        );
    }
    
    /**
     * Obtener datos de usuario
     * 
     * @param int    $id     id de usuario
     * @param string $codigo codigo de verificacion de cuenta
     * 
     * @return null
     */
    public function getUsuario($id, $codigo)
    {
        $usuario = $this->_db->query(
            "select * from " . FW_PREFIX . "usuarios where id = $id and codigo = $codigo"
        );
        return $usuario->fetch();
    }
    
    /**
     * Obtener datos de usuario
     *
     * @param int    $id     id de usuario
     * @param string $codigo codigo de verificacion de cuenta
     *
     * @return null
     */
    public function getUsuarioById($id)
    {
    	$usuario = $this->_db->query(
    			"select * from " . FW_PREFIX . "usuarios where id = $id"
    	);
    	return $usuario->fetch();
    }
    
    /**
     * Actualizar contraseña de usuario
     * 
     * @param int    $id     id de usuario
     * @param string $codigo cadena de verificacion de cuenta
     * @param string $pass   contraseña en plano (cifrada en esta funcion)
     * 
     * @return null
     */
    public function updPassword( $id, $codigo, $pass )
    {
        $pass = Hash::getHash('sha1', $pass, HASH_KEY);
        if ($this->_safePass == false) {
            $pass = sha1($pass);
        }
        
        $this->_db->query(
            "UPDATE " . FW_PREFIX . "usuarios SET pass='{$pass}' WHERE id={$id} AND codigo={$codigo}"
        );
    }

    /**
     * Activar cuenta de usuario
     * 
     * @param int    $id     id de usuario
     * @param string $codigo cadena de verificacion de cuenta
     * 
     * @return null
     */
    public function activarUsuario($id, $codigo)
    {
        $this->_db->query(
            "update " . FW_PREFIX . "usuarios set estado = 1 " .
            "where id = $id and codigo = '$codigo'"
        );
    }
}