<?php
/**
 * Modelo del controlador index del modulo de registro de usuarios
 *
 * PHP Version 5.4
 *
 * @category Framework.core.modules.usuarios
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/usuarios/models/indexModel.php
 *
 */

/**
 * Modelo del controlador index del modulo de registro de usuarios
 *
 * @category Framework.core.modules.usuarios
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/usuarios/controllers/indexModel.php
 */

class IndexModel extends Model
{
    
    /**
     * Controlador predeterminado
     * 
     *  @return null
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Obtener lista de usuarios
     * 
     * @return array 
     */
    public function getUsuarios($noAdmin = false, $role = 0, $pagina = 0, $lpp = 10)
    {
    	if ($pagina < 1) {
    		$pagina = 1;
    	}
    	$inicio   = (($pagina - 1) * $lpp);
    	
    	$qExtend  = $noAdmin == false ? "" : " AND u.role <> 1";
        $qExtend  = $role == 0 ? $qExtend : " AND u.role={$role}";
         
        $usuarios = $this->_db->query(
            "
        		SELECT 
        			u.*,
        			r.role
        		FROM " . FW_PREFIX . "usuarios u, " . FW_PREFIX . "roles r
        		WHERE
        			u.role = r.id_role
        		{$qExtend}
        		ORDER BY u.usuario
        		LIMIT {$inicio},{$lpp}
        	"
        );
        	
        return $usuarios->fetchAll(PDO::FETCH_ASSOC);
    }
    
    /**
     * Obtener id, nombre de usuario y rol de un usuario
     * 
     * @param int $usuarioID id de usuario
     * 
     * @return array(id,usuario,role)
     */
    public function getUsuario($usuarioID)
    {
        $usuarios = $this->_db->query(
            "select u.id, u.usuario, u.nombre, r.role from " . FW_PREFIX . "usuarios u, " . FW_PREFIX . "roles r ".
            "where u.role = r.id_role and u.id = $usuarioID"
        );
        return $usuarios->fetch();
    }
    
    public function getTotalUsuarios($role = 0)
    {
    	$role = $role > 0 ? "AND role={$role}" : "";
    	
    	$datos = $this->_db->query(
    		"SELECT  COUNT(id) AS total FROM " . FW_PREFIX . "usuarios WHERE estado=1 {$role}"
    	)->fetch(PDO::FETCH_ASSOC);
    	
    	return isset($datos["total"]) ? (int)$datos["total"] : 0;
    }
    
    /**
     * Obtener los permisos propios del usuario
     * 
     * @param int $usuarioID id de usuario
     * 
     * @return lista de permisos del usuario
     */
    public function getPermisosUsuario($usuarioID)
    {
        $acl = new ACL($usuarioID);
        return $acl->getPermisos();
    }
    
    /**
     * Obtener los permisos del rol asignado al usuario
     * 
     * @param int $usuarioID id de usuario
     * 
     * @return lista de permisos del rol
     */
    public function getPermisosRole($usuarioID)
    {
        $acl = new ACL($usuarioID);
        return $acl->getPermisosRole();
    }
    
    /**
     * Eliminar permiso de usuario
     * 
     * @param int $usuarioID id de usuario
     * @param int $permisoID id de permiso
     * 
     * @return null
     */
    public function eliminarPermiso($usuarioID, $permisoID)
    {
        $this->_db->query(
            "delete from " . FW_PREFIX . "permisos_usuario where ".
            "usuario = $usuarioID and permiso = $permisoID"
        );
    }
    
    /**
     * Establecer valor de permiso de usuario
     * 
     * @param int $usuarioID id de usuario
     * @param int $permisoID id de permiso
     * @param int $valor     valor del permiso
     * 
     * @return null
     */
    public function editarPermiso($usuarioID, $permisoID, $valor)
    {
        $this->_db->query(
            "replace into " . FW_PREFIX . "permisos_usuario set ".
            "usuario = $usuarioID , permiso = $permisoID, valor ='$valor'"
        );
    }
    
    /**
     * Obtener lista de roles
     * 
     * @return array(*) de tabla roles
     */
    public function getRoles($noAdmin = false)
    {
    	$qExtend  = $noAdmin == false ? "" : "WHERE id_role <> 1";
        $dato = $this->_db->query(
            "SELECT * FROM " . FW_PREFIX . "roles {$qExtend} ORDER BY id_role"
        );
        return $dato->fetchAll(PDO::FETCH_ASSOC);
    }
    
    /**
     * Establecer el rol de un usuario
     * 
     * @param int $usuarioID id de usuario
     * @param int $roleID    id de rol
     * 
     * @return null
     */
    public function setRole( $usuarioID, $roleID )
    {
        $this->_db->query(
            "UPDATE " . FW_PREFIX . "usuarios SET role={$roleID} WHERE id={$usuarioID}"
        );
    }
    
    /**
     * Eliminar un usuario
     * 
     * @param int $usuarioID id del usuario
     * 
     * @return null
     */
    public function delUsuario( $usuarioID )
    {
    	$this->_db->query(
    		"DELETE FROM " . FW_PREFIX . "permisos_usuario WHERE usuario='{$usuarioID}'"
    	);
    	$this->_db->query(
    		"DELETE FROM " . FW_PREFIX . "usuarios WHERE id='{$usuarioID}'"
    	);
    }
    
    /**
     * Actualizar contraseña de usuario
     *
     * @param int    $id     id de usuario
     * @param string $pass   contraseña en plano (cifrada en esta funcion)
     *
     * @return null
     */
    public function updPassword( $id, $pass )
    {
    	$pass = Hash::getHash('sha1', $pass, HASH_KEY);
        if ($this->_safePass == false) {
            $pass = sha1($pass);
        }
        
    	$this->_db->query(
    		"UPDATE " . FW_PREFIX . "usuarios SET pass='{$pass}' WHERE id={$id}"
    	);
    }
    
    public function habilitar($id)
    {
    	$this->_db->query("UPDATE " . FW_PREFIX . "usuarios SET estado=1, errors=0 WHERE id='" . $id . "'");
    }
}
