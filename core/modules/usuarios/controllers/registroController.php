<?php
/**
 * Controlador de modulo de registro de usuarios
 *
 * PHP Version 5.4
 *
 * @category Framework.core.modules.usuarios
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/usuarios/controllers/registroController.php
 *
 */

/**
 * Controlador de modulo de registro de usuarios
 *
 * @category Framework.core.modules.usuarios
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/usuarios/controllers/registroController.php
 */

class RegistroController extends Controller
{
    private $_registro;
    
    /**
     * Constructor predeterminado
     * 
     * @return null
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_conf->load("usuarios");
        
        $metodos_publicos = array("olvido", "reset", "activar", "resetmsg");
        if (in_array($this->_request->getMetodo(), $metodos_publicos) == false) {
            if ((int)$this->_conf->item("usuarios", "allow_register") == 0 && Session::get("autenticado") != true) {
                $this->redireccionar('error/access/5050');
                return;
            }
        }
        
        $this->_registro = $this->loadModel('registro');
        $this->_registro->_safePass = $this->_conf->item("usuarios", "safe_pass");
        $this->_view->assign('titulo', 'Registro');
    }

    /**
     * Método privado que valida los datos de input de perfil de usuario
     * 
     * @param bool $update true si se validan datos de edición de perfil
     * 
     * @return boolean true si todo está bien, false en caso contrario
     */
    
    private function _validateInput($update = false)
    {
    	if ($update != false) {
            $this->_view->assign("update", true);
    	}
    	
    	if (!$this->getSql('nombre')) {
            $this->_view->assign('error', 'Debe introducir su nombre');
            $this->_view->renderizar('index', 'registro');
            return false;
    	}
    	 
    	if (!$this->getAlphaNum('usuario')) {
            $this->_view->assign('error', 'Debe introducir su nombre usuario');
            $this->_view->renderizar('index', 'registro');
            return false;
    	}

    	if (!$this->validarEmail($this->getPostParam('email'))) {
            $this->_view->assign(
                'error',
                'La direccion de email es inv&aacute;lida'
            );
            $this->_view->renderizar('index', 'registro');
            return false;
    	}
    	
    	// Si es una edición de perfil corre estas verificaciones de usuario y email
    	if ($update == true) {
            $verUsu =$this->_registro->verificarUsuario($this->getAlphaNum('usuario'));
            if ($verUsu && $verUsu["id"] != Session::get("id_usuario")) {
                $this->_view->assign(
                    'error',
                    'El usuario ' . $this->getAlphaNum('usuario') . ' ya existe'
                );
                $this->_view->renderizar('index', 'registro');
                return false;
            }

            $verEma = $this->_registro->verificarEmail($this->getPostParam('email'));
            if ($verEma && $verEma["id"] != Session::get("id_usuario")) {
                $this->_view->assign(
                    'error',
                    'Esta direccion de correo ya esta registrada'
                );
                $this->_view->renderizar('index', 'registro');
                return false;
            }
    	} else {
            // Si es un nuevo perfil, corre estas otras verificaciones de usuario y email
            if ($this->_registro->verificarUsuario($this->getAlphaNum('usuario'))) {
                $this->_view->assign(
                    'error',
                    'El usuario ' . $this->getAlphaNum('usuario') . ' ya existe'
                );
                $this->_view->renderizar('index', 'registro');
                return false;
            }

            if ($this->_registro->verificarEmail($this->getPostParam('email'))) {
                $this->_view->assign(
                    'error',
                    'Esta direccion de correo ya esta registrada'
                );
                $this->_view->renderizar('index', 'registro');
                return false;
            }
    	}
    	return true;
    }
    
    /**
     * Valida que se haya introducido clave y confirmación y que coincidan
     * 
     */
    private function _validatePass($template = 'index')
    {
    	if (!$this->getSql('pass')) {
            $this->_view->assign('error', 'Debe introducir su password');
            $this->_view->renderizar($template);
            return false;
    	}
    	 
    	if ($this->getPostParam('pass') != $this->getPostParam('confirmar')) {
            $this->_view->assign('error', 'Los passwords no coinciden');
            $this->_view->renderizar($template);
            return false;
    	}
    	
    	return true;
    }
    
    /**
     * Procesar los datos mandados por post del update de perfil de usuario
     * 
     */
    public function procesarUpdate()
    {
    	$this->_view->assign("txtBoton", "Agregar");
    	$this->_view->assign("action", "procesarAlta");
    	
    	$this->_view->setCss('usuarios/login/dynCss', 'D');
    	$this->_view->setJs('registro');
    	 
    	if ($this->getInt('enviar') == 1) {
            $this->_view->assign('datos', $_POST);

            if ($this->_validateInput(true) == false) {
                return;
            }

            $role = $this->getInt('role');
            $usuario = $this->_registro->getUsuarioById(Session::get("id_usuario"));

            $datos = array();
            if ($this->getSql("nombre") != $usuario["nombre"]) {
                $datos["nombre"] = $this->getSql("nombre");
            }
            if ($this->getAlphaNum("usuario") != $usuario["usuario"]) {
                $datos["usuario"] = $this->getAlphaNum("usuario");
            }
            if ($role != (int)$usuario["role"] && $role != 0) {
                $datos["role"] = $role;
            }
            if ($this->getPostParam('email') != $usuario["email"]) {
                $datos["email"] = $this->getPostParam('email');
            }
            $this->_registro->updUsuario(
                Session::get("id_usuario"),
                $datos
            );

            $usuario = $this->_registro->verificarUsuario(
                $this->getAlphaNum('usuario')
            );

            if (!$usuario) {
                $this->_view->assign('error', 'Error al registrar el usuario');
                $this->_view->renderizar('index', 'registro');
                exit;
            }

            $this->_view->assign('datos', false);
            $this->redireccionar();
            return;
    	}
    	 
    	// Si no terminó antes, entonces volver a la vista de formulario de alta
    	$this->amPerfil(1);
    }
    
    /**
     * Procesar los datos mandados por post de los datos de alta de usuario
     * 
     */
    public function procesarAlta()
    {
    	$this->_view->assign("txtBoton", "Agregar");
    	$this->_view->assign("action", "procesarAlta");
    	 
    	$this->_view->setCss('usuarios/login/dynCss', 'D');
    	$this->_view->setJs('registro');
    	
    	if ($this->getInt('enviar') == 1) {
            $this->_view->assign('datos', $_POST);

            if ($this->_validateInput() == false) {
                return;
            }

            if ($this->_validatePass() == false) {
                return;
            }

            $role = $this->getInt('role');

            $this->_registro->registrarUsuario(
                $this->getSql('nombre'),
                $this->getAlphaNum('usuario'),
                $this->getSql('pass'),
                $this->getPostParam('email'),
                $role
            );

            $usuario = $this->_registro->verificarUsuario(
                $this->getAlphaNum('usuario')
            );

            if (!$usuario) {
                $this->_view->assign('error', 'Error al registrar el usuario');
                $this->_view->renderizar('index', 'registro');
                exit;
            }

            $this->_view->assign('datos', false);
            $this->sendAck($usuario[ 'id' ], $usuario[ 'codigo' ]);
            return;
    	}
    	
    	// Si no terminó antes, entonces volver a la vista de formulario de alta
    	$this->amPerfil();
    }
    
    /**
     * Método genérico que permite procesar el alta y la edición de perfiles de usuarios
     * 
     * @param bool $update false: alta de usuario, true: edición de datos de perfil
     */
    public function amPerfil($update = false)
    {
    	if ($update != false) {
            $update = $this->_registro->getUsuarioById(Session::get('id_usuario'));
            if (! $update) {
                $this->redireccionar("error/5050");
                return;
            }
            $datos = array(
                "nombre" => $update["nombre"],
                "usuario" => $update["usuario"],
                "email" => $update["email"]
            );
            $this->_view->assign("datos", $datos);
            $this->_view->assign("update", true);
            $this->_view->assign("txtBoton", "Actualizar");
            $this->_view->assign("action", "procesarUpdate");
    	} else {
            $this->_view->assign("txtBoton", "Agregar");
            $this->_view->assign("action", "procesarAlta");
    	}
    	
    	$this->_view->setCss('usuarios/login/dynCss', 'D');
    	$this->_view->setJs('registro');
    	
    	// Aquí pasa la magia del proceso... o eso espero    	
    	if ($this->_acl->permiso('admin_access')) {
            $this->_view->assign("roles", $this->_registro->getRoles());
            $this->_view->assign("default_role", $this->_registro->getDefaultRole());
    	}
    	
    	$this->_view->setCss("registro");
    	$this->_view->renderizar('index', 'registro');
    }
    
    /**
     * Indice de registro de usuario
     * 
     * @see Controller::index()
     * 
     * @return html
     */
    public function index()
    {   
        $this->amPerfil();
    }

    /**
     * Mostrar mensaje de notificación de activación enviada
     * 
     */
    public function ackSent()
    {
    	$this->_view->setCss('usuarios/login/dynCss', 'D');
    	$this->_view->assign("app_name", APP_NAME);
    	$this->_view->renderizar("sendAckMsg");
    }
    
    /**
     * Enviar mail de activación
     * 
     * @param int $id id del usuario en tabla usuarios
     * @param int $codigo codigo unico asociado al usuario
     */
    public function sendAck($id, $codigo)
    {
    	if (!$this->filtrarInt($id) || !$this->filtrarInt($codigo)) {
            $this->redireccionar("error/5052");
            return;   
        }
		
        $row = $this->_registro->getUsuario(
            $this->filtrarInt($id),
            $this->filtrarInt($codigo)
        );
        
        if (!$row) {
            $this->redireccionar("error/5052");
            return;
        }
        
        $activar_url = BASE_URL . 'usuarios/registro/activar/' . $id . '/' . $codigo;
        
        $this->getLibrary("class.mail_manager");
        $mail_manager = new Mail_manager(
            $this->_view,
            $this->loadModel("editor", "modmailer"),
            $this->_conf->branch("usuarios")
        );
         
        $mail_manager->setSubject('Bienvenid@ a ' . APP_NAME);
         
        if (!$mail_manager->send(
            $row[ 'email' ],
            "sendAck",
            array(
                "body"  => array(
                    'nombre'      => $row[ 'nombre' ],
                    'app_company' => APP_NAME,
                    'base_url'    => $activar_url,
                    'usuario'     => $row[ 'usuario' ]
                )
            )
        )) {
            $this->redireccionar("error/5053");
            return;
    	}
    	
    	$this->redireccionar("usuarios/registro/ackSent");
    }
    
    /**
     * Activar cuenta de usuaro
     * 
     * @param int    $id     id de cuenta
     * @param string $codigo codigo de validacion de cuenta
     * 
     * @return html
     */
    public function activar($id, $codigo)
    {
    	$this->_view->setCss('usuarios/login/dynCss', 'D');
    	
    	if (!$this->filtrarInt($id) || !$this->filtrarInt($codigo)) {
            $this->redireccionar("error/5052");
            return;   
        }

        $row = $this->_registro->getUsuario(
            $this->filtrarInt($id),
            $this->filtrarInt($codigo)
        );

    	if (!$row) {
            $this->redireccionar("error/5052");
            return;   
        }

        if ($row['estado'] == 1) {
            $this->_view->assign('error', 'Esta cuenta ya ha sido activada');
            $this->_view->renderizar('activar', 'registro');
            exit;
        }

        $this->_registro->activarUsuario(
            $this->filtrarInt($id),
            $this->filtrarInt($codigo)
        );

        $row = $this->_registro->getUsuario(
            $this->filtrarInt($id),
            $this->filtrarInt($codigo)
        );

        if ($row['estado'] == 0) {
            $this->_view->assign(
                'error', 
                'Error al activar la cuenta, por favor intente mas tarde'
            );
            $this->_view->renderizar('activar', 'registro');
            exit;
        }

        $this->_view->assign('mensaje', 'Su cuenta ha sido activada');
        $this->_view->renderizar('activar', 'registro');
    }
    
    /**
     * Alias al método amPerfil(1), creado para simplificar los enlaces en las plantillas
     */
    public function perfil()
    {
    	$this->amPerfil(1);
    }
    
    /**
     * Metodo público que muestra el diálogo de confirmación de reseteo de password
     * 
     * @param string $usuario Nombre de usuario al que se va a resetear la clave
     * 
     */
    public function olvido($usuario)
    {
    	$this->_view->setCss('usuarios/login/dynCss', 'D');
    	
    	$usuario = $this->filtrarSQL($usuario);
    	
    	$usuario = $this->_registro->verificarUsuario($usuario);
    	if(! $usuario) {
            $this->redireccionar("error/5052");
            return;
    	}
    	
    	$this->_view->assign("uid", $usuario["id"]);
    	$this->_view->assign("codigo", $usuario["codigo"]);
    	$this->_view->renderizar('reset');
    }
    
    /**
     * Muestra el diálogo en que comunica al usuario que se reseteó su clave y se le envió un mail
     * con la nueva clave
     */
    public function resetMsg()
    {
    	$this->_view->setCss('usuarios/login/dynCss', 'D');
    	$this->_view->assign("app_name", APP_NAME);
    	$this->_view->renderizar("sendResetMsg");
    }
    
    /**
     * Reestablecer la contraseña de un usuario a demanda
     * 
     * @param int    $id     id de usuario
     * @param string $codigo codigo de validación de usuario
     * 
     * @return html y mail con nueva clave
     */
    public function reset( $id, $codigo )
    {
        $id     = $this->filtrarInt($id);
        $codigo = $this->filtrarInt($codigo);
        
        if ( $id <= 0 || $codigo <= 0 ) {
            $this->redireccionar('error/5050');
            exit;
        }
        
        $usuario = $this->_registro->getUsuario($id, $codigo);
        
        if ( ! isset( $usuario[ 'estado' ] ) || $usuario['estado'] != 1) {
            $this->redireccionar('error/5050');
            return;
        }
        
        $pass = $this->_generatePass($usuario);
        
        $this->_registro->updPassword($id, $codigo, $pass);
        
        $this->getLibrary("class.mail_manager");
      	$mail_manager = new Mail_manager(
                $this->_view,
                $this->loadModel("editor", "modmailer"),
            	$this->_conf->branch("usuarios")
            );
        $mail_manager->setSubject('Clave de usuario en ' . APP_NAME . ' reestablecida');

        if ($mail_manager->send(
            $usuario[ 'email' ],
            "sendReset",
            array(
                "subject" => array(
                    'app_name' => APP_NAME
                ),
                "body"  => array(
                    'nombre'      => $usuario[ 'nombre' ],
                    'app_company' => APP_NAME,
                    'base_url'    => BASE_URL,
                    'usuario'     => $usuario[ 'usuario' ],
                    'pass'        => $pass
                )
            )
        ) == false) {
            $this->redireccionar('error/1004');
        } else {
            $this->redireccionar('usuarios/registro/resetMsg');
        }
    } 

    /**
     * Genera una clave aleatoria de 8 caracteres alfanuméricos
     * 
     * @param array $user ficha de datos de usuario
     * 
     * @return string
     */
    private function _generatePass( $user )
    {
        $cadena = $user[ 'id'    ] .
            $user[ 'nombre'  ] .
            $user[ 'usuario' ] .
            $user[ 'pass'    ] .
            $user[ 'email'   ] .
            $user[ 'role'    ] .
            $user[ 'fecha'   ] .
            $user[ 'codigo'  ];
        
        $cadena = md5($cadena);
        $inicio = rand(0, 24);
        $cadena = substr($cadena, $inicio, 8);
        return $cadena;
    }
    
    public function clave()
    {
    	$this->_view->setCss('usuarios/login/dynCss', 'D');
    	$this->_view->setCss("registro");
    	$this->_view->assign("app_name", APP_NAME);
    	
    	$enviar = $this->getInt("enviar");
    	
    	if ($enviar == 1) {
            if ($this->_validatePass("clave") == false) {
                return;
            }

            $usuario = $this->_registro->getUsuarioById(Session::get("id_usuario"));
            $this->_registro->updPassword(
                $usuario["id"], 
                $usuario["codigo"],
                $this->getSql("pass")
            );
            $this->redireccionar();
            return;
    	}
    	
    	$this->_view->renderizar("clave");
    }
}