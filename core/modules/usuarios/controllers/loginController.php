<?php
/**
 * Controlador de modulo de login
 *
 * PHP Version 5.4
 *
 * @category Framework.core.modules.usuarios
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/usuarios/controllers/loginController.php
 *
 */

/**
 * Controlador de modulo de login
 *
 * @category Framework.core.modules.usuarios
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/usuarios/controllers/loginController.php
 */

class LoginController extends Controller
{
    private $_login;
    
    /**
     * Constructor predeterminado
     * 
     * @return null
     */
    public function __construct()
    {
        parent::__construct();
        $this->_login = $this->loadModel('login');
        $this->_conf->load("usuarios");
        $this->_login->_safePass = $this->_conf->item("usuarios", "safe_pass");
    }
    
    /**
     * Método privado que suma a la fecha en que se bloqueó la cuenta los minutos establecidos en 
     * la configuración y retorna la fecha en que se desbloqueará
     * 
     * @param string $bloqueo: String con formato fecha
     * 
     * @return DateTime: con la fecha de desbloqueo de cuenta
     */
    private function _getUnlockDate($bloqueo)
    {
    	$fecha_bloqueo = new DateTime($bloqueo);
    	$intervalo = new DateInterval("PT" . $this->_conf->item("usuarios", "lock_minutes") . "M");
    	$fecha_bloqueo->add($intervalo);
		return $fecha_bloqueo;
    }
    
    /**
     * Método que retorna los días, horas, minutos y segundos faltantes para
     * desbloquear la cuenta
     * 
     * @param DateTime $fecha_bloqueo: Fecha en que se desbloqueará la cuenta
     * @param DateTime $fecha_ahora: DateTime con la fecha actual
     * 
     * @return string: Cadena con formato legible por humanos
     */
    private function _timeFormat($fecha_bloqueo, $fecha_ahora)
    {
    	$diferencia = $fecha_bloqueo->diff($fecha_ahora);
    	
    	$msg = $diferencia->d > 0 ? $diferencia->d . " días " : '';
    	$msg.= $diferencia->h > 0 ? $diferencia->h . " horas " : '';
    	$msg.= $diferencia->i > 0 ? $diferencia->i . " minutos " : '';
    	$msg.= $diferencia->s > 0 ? $diferencia->s . " segundos" : '';
    	
    	return trim($msg);
    }
    
    /**
     * Indice de login
     * Carga vista de login y hace el control de usuario/pass
     * 
     * @see Controller::index()
     * 
     * @return html
     */
    public function index()
    {
        if (Session::get('autenticado')) {
            $this->redireccionar();
        }
        
        $target = "index";
        if (DEFAULT_LAYOUT === "pecari_bs4") {
            $this->_view->setCss("pecari_bs4");
        }
        
        $this->_view->assign('titulo', 'Iniciar Sesion');
        $this->_view->setCss('usuarios/login/dynCss', 'D');
        
        if ($this->getInt('enviar') == 1) {
            $this->_view->assign('datos', $_POST);

            if (!$this->getAlphaNum('usuario')) {
                $this->_view->assign(
                    'error', 
                    'Debe introducir su nombre de usuario'
                );
                $this->_view->renderizar($target, 'login');
                exit;
            }
            
            if (!$this->getSql('pass')) {
                $this->_view->assign('error', 'Debe introducir su password');
                $this->_view->assign('forgot', true);
                $this->_view->renderizar($target, 'login');
                exit;
            }
            
            $row = $this->_login->getUsuario(
                $this->getAlphaNum('usuario'),
                $this->getSql('pass')
            );
            
            if (!$row) {
            	$error = "Usuario y/o password incorrectos";
            	$this->_view->assign('forgot', true);
            	$bloqueo = $this->_login->incError(
            		$this->getAlphaNum('usuario'),
            		$this->_conf->item("usuarios", "error_limit")
            	);
            	
            	if ($bloqueo != false) {
            		$fecha_bloqueo = $this->_getUnlockDate($bloqueo); 
            		$fecha_ahora = new DateTime();
            		$msg = $this->_timeFormat($fecha_bloqueo, $fecha_ahora);
            		
            		$error = $this->_conf->item("usuarios", "err_msg");
              		
              		if ($fecha_bloqueo->getTimestamp() > $fecha_ahora->getTimestamp()) {
              			$error .= ",<br> se reactivará automáticamente en <br><br><b>" . $msg . "</b>";
              		}
            	}
            	
                $this->_view->assign('error', $error);
                $this->_view->renderizar($target, 'login');
                exit;
            }
            
            if ($row['estado'] != 1) {
            	$error = 'Este usuario no esta habilitado';
            	$bloqueo = $this->_login->checkUnlock($row["id"]);
            	if ($bloqueo != false && $row['estado'] == 2){
            		$error = $this->_conf->item("usuarios", "err_msg");
            		$fecha_bloqueo = $this->_getUnlockDate($bloqueo);
            		$fecha_ahora = new DateTime();
            		$msg = $this->_timeFormat($fecha_bloqueo, $fecha_ahora);
            		
            		if ($fecha_bloqueo->getTimestamp() > $fecha_ahora->getTimestamp()) {
            			$error .= ",<br> se reactivará automáticamente en <br><br><b>" . $msg . "</b>";
            		} else {
            			$this->_login->doUnlock($row['id']);
            			unset($error);
            		}
            	}
            	
            	if (isset($error)) {
	                $this->_view->assign('error', $error);
	                $this->_view->renderizar($target, 'login');
	                exit;
            	}
            }
                        
            Session::set('autenticado', true);
            Session::set('level',       $row[ 'role'    ]);
            Session::set('usuario',     $row[ 'usuario' ]);
            Session::set('id_usuario',  $row[ 'id'      ]);
            Session::set('tiempo',      time());
            
            // Esto permite limipar en forma automatica la cache del smarty, 
            // cada 30 segundos la cera
            if ( $this->_login->clearCache() == true ) {
                $this->_view->clearAllCache();
                $this->_view->clearCompiledTemplate();
            }

            $this->redireccionar();
        }
        
        $this->_view->setJs("login");
        $this->_view->renderizar($target, 'login');
        
    }

    /**
     * Cierre de sesión
     * 
     * @return html redirect
     */
    public function cerrar()
    {
        Session::destroy();
        $this->redireccionar();
    }
 
}
