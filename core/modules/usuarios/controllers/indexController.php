<?php
/**
 * Controlador de modulo de usuarios
 *
 * PHP Version 5.4
 *
 * @category Framework.core.modules.usuarios
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/usuarios/controllers/indexController.php
 *
 */

/**
 * Controlador de modulo de usuarios
 *
 * @category Framework.core.modules.usuarios
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/usuarios/controllers/indexController.php
 */

class IndexController extends usuariosController
{
    private $_usuarios;
    private $_paginado;
    
    /**
     * Constructor predeterminado
     * 
     * @return null
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_usuarios = $this->loadModel('index');
        $this->_conf->load("usuarios");
        $this->_usuarios->_safePass = $this->_conf->item("usuarios", "safe_pass");
        $this->_view->setCss('usuarios/login/dynCss/noresize', 'D');
        $this->getLibrary("paginado");
        $this->_paginado = new Paginado();
    }
    
    /**
     * Index de usuarios
     * Muestra lista de usuarios de sistema
     * 
     * @see usuariosController::index()
     * 
     * @return html
     */
    public function index($role = 0)
    {
    	$noAdmin = false;
    	if ($this->_acl->permiso('admin_users') == false) {
        	$this->_acl->acceso('admin_access');
    	} else {
    		$noAdmin = true;
    	}        
    	
    	$role = $this->filtrarInt($role);
    	$my_role = (int)Session::get("level");
    	
    	// Para que funcione la idea de hacer que un usuario no pueda ver niveles superiores al suyo
    	// primero tengo que dar una forma de asignar peso a los roles, tengo que agregar un campo
    	// en la tabla de roles que permita asignar nivel de acceso a un role
    	
    	$this->_conf->load("usuarios");
    	
    	$usersperpage = (int)$this->_conf->item("usuarios", "users_per_page");
    	
    	$pagina   = $this->getInt("pagina");
    	$total    = ceil($this->_usuarios->getTotalUsuarios($role) / $usersperpage);
    	$usuarios = $this->_usuarios->getUsuarios($noAdmin, $role, $pagina, $usersperpage);
    	
    	$this->_view->setJs("paginador", "C");
    	$this->_view->setJs("index");
    	
    	$this->_view->assign("paginado", $this->_paginado->paginar($pagina, $total));
    	$this->_view->assign('titulo', 'Usuarios');
        $this->_view->assign('usuarios', $usuarios);
    	$this->_view->assign('roles', $this->_usuarios->getRoles($noAdmin));
    	$this->_view->assign('cur_role', $role);
        $this->_view->renderizar('index', 'usuarios');
    }
    
    /**
     * Establecer permisos de usuarios
     * 
     * @param int $usuarioID id de usuario
     * 
     * @return html
     */
    public function permisos($usuarioID)
    {
    	if ($this->_acl->permiso('admin_users') == false) {
        	$this->_acl->acceso('admin_access');
    	}
        
        $id = $this->filtrarInt($usuarioID);
        
        if (!$id) {
            $this->redireccionar('usuarios');
        }
        
        if ($this->getInt('guardar') == 1) {
            $values = array_keys($_POST);
            $replace = array();
            $eliminar = array();
            
            for ($i = 0; $i < count($values); $i++) {
                if (substr($values[$i], 0, 5) == 'perm_') {
                    $permiso = (strlen($values[$i]) - 5);
                    
                    if ($_POST[$values[$i]] == 'x') {
                        $eliminar[] = array(
                            'usuario' => $id,
                            'permiso' => substr($values[$i], -$permiso)
                        );
                    } else {
                        if ($_POST[$values[$i]] == 1) {
                            $v = 1;
                        } else {
                            $v = 0;
                        }
                        
                        $replace[] = array(
                            'usuario' => $id,
                            'permiso' => substr($values[$i], -$permiso),
                            'valor' => $v
                        );
                    }
                }
            }
            
            for ($i = 0; $i < count($eliminar); $i++) {
                $this->_usuarios->eliminarPermiso(
                    $eliminar[$i]['usuario'],
                    $eliminar[$i]['permiso']
                );
            }
            
            for ($i = 0; $i < count($replace); $i++) {
                $this->_usuarios->editarPermiso(
                    $replace[$i]['usuario'],
                    $replace[$i]['permiso'],
                    $replace[$i]['valor']
                );
            }
        }
        
        $permisosUsuario = $this->_usuarios->getPermisosUsuario($id);
        $permisosRole = $this->_usuarios->getPermisosRole($id);
        
        if (!$permisosUsuario || !$permisosRole) {
            $this->redireccionar('usuarios');
        }
        
        $this->_view->assign('titulo', 'Permisos de usuario');
        $this->_view->assign('permisos', array_keys($permisosUsuario));
        $this->_view->assign('usuario', $permisosUsuario);
        $this->_view->assign('role', $permisosRole);
        $this->_view->assign('info', $this->_usuarios->getUsuario($id));
        
        $this->_view->renderizar('permisos', 'usuarios');
    }
    
    /**
     * Establecer rol de usuario
     * 
     * @param int $usuarioID id de usario
     * 
     * @return html
     */
    function setrole( $usuarioID )
    {
    	$noAdmin = false;
    	if ($this->_acl->permiso('admin_users') == false) {
        	$this->_acl->acceso('admin_access');
    	} else {
    		$noAdmin = true;
    	}
        
        $id = $this->filtrarInt($usuarioID);
        
        if (!$id) {
            $this->redireccionar('usuarios');
        }
        
        if ($this->getInt('guardar') == 1) {
            $role = $this->getInt('role');
            $this->_usuarios->setRole($id, $role);
            $this->_view->assign('_mensaje', 'Se ha cambiado el role del usuario');
            $this->_view->renderizar('index', 'usuarios');
            exit;
        }
        
        $usuario = $this->_usuarios->getUsuario($id);
        
        $this->_view->assign('roles', $this->_usuarios->getRoles($noAdmin));
        $this->_view->assign('role', $usuario[ 'role' ]);
        
        $this->_view->assign('titulo', 'Role de usuario');
        $this->_view->renderizar('setrole', 'usuarios');
    }
    
    
    public function habilitar($usuarioID)
    {
    	$this->_usuarios->habilitar($usuarioID);
    	$this->redireccionar("usuarios");
    }
    
    /**
     * Eliminar usuario del sistema
     *
     * @param int $usuarioID id de usario
     *
     * @return html
     */
    function eliminar( $usuarioID, $confirm = false )
    {
    	if ($confirm == false) {
	    	$this->_view->assign('titulo', 'Baja de usuario');
	    	$this->_view->assign('usuario', $this->_usuarios->getUsuario($usuarioID));
	    	$this->_view->renderizar('bajaconf', 'usuarios');
	    	return;
    	}
    	
    	if ($confirm == 1) {
    		$this->_usuarios->delUsuario($usuarioID);
    		$this->redireccionar("usuarios");
    	}
    }
    
    /**
     * Cambiar la clave a un usuario
     * 
     * @param int $usuarioID id de usuario
     * 
     * @return null
     */
    function cambiarClave( $usuarioID, $confirm = false )
    {
    	if ($confirm == false) {
    		$this->_view->assign('titulo', 'Cambiar clave de usuario');
    		$this->_view->assign('info', $this->_usuarios->getUsuario($usuarioID));
    		$this->_view->renderizar('clave', 'usuarios');
    		exit;
    	}
    	
    	if ( $confirm == 1) {
    		$clave1 = trim($this->getSql("clave1"));
    		$clave2 = trim($this->getSql("clave2"));
    		
    		if ($clave1 != $clave2) {
    			$error = "La clave no coincide con la verificación";
    		}
    		
    		if (strlen($clave1) < 6) {
    			$error = "La clave debe tener al menos 6 caracteres";
    		}
    		
    		if (isset($error)) {
    			$this->_view->assign('titulo', 'Cambiar clave de usuario');
    			$this->_view->assign('info', $this->_usuarios->getUsuario($usuarioID));
    			$this->_view->assign('error', $error);
    			$this->_view->renderizar('clave', 'usuarios');
    			exit;
    		}
    		
    		$this->_usuarios->updPassword($usuarioID, $clave1);
    	}
    	
    	$this->redireccionar("usuarios");
    }
    
}
