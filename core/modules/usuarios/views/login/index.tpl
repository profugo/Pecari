<div class="avatar-container">
<div class="avatar" style="background-image: url('{$_layoutParams.ruta_logo}')"></div>
</div>
<h2>{$_layoutParams.configs.app_name}<br><sub>Iniciar Sesi&oacute;n</sub></h2>
{if isset($error)}
<div class="alert alert-danger">{$error}</div>
{/if}
<form name="form1" method="post" action="" class="form">
<div class="form-group">
    <input type="hidden" value="1" name="enviar" />
    
    <table style="width: 350px;">
        <tr>
            <td style="text-align: right;">Usuario: </td>
            <td><input type="text" class="form-control" id="usuario" name="usuario" value="{$datos.usuario|default:""}" /></td>
        </tr>

        <tr>
            <td style="text-align: right;">Password: </td>
            <td><input type="password" name="pass" class="form-control" /></td>
        </tr>
    </table>
<hr> 
  
    <p style="text-align:right">
<div style="float:left; margin-top:8px">
{if Session::get('allow_register')}
<a href="{$_layoutParams.root}usuarios/registro">Crear cuenta</a>
{if isset($forgot)}
&nbsp; | &nbsp;
{/if}
{/if}
{if isset($forgot)}
<a href="{$_layoutParams.root}usuarios/registro/olvido/{$datos.usuario|default:''}">¿Olvidaste la clave?</a>
{/if}
</div>


    <button type="submit" class="btn btn-primary withMargin" style="float:right"><li class="icon-ok icon-white glyphicon glyphicon-ok fa fa-check"> </li> Entrar</button></p>
    </div>
</form>