<h2 class="maintitle">Usuarios<sub>Listado de usuarios registrados en el sistema</sub></h2>
<div class="row container">
	<div class="col-xs-6">
		<a href="{$_layoutParams.root}usuarios/registro" class="btn btn-primary"><i class="glyphicon glyphicon-plus icon-plus fa fa-plus icon-white"> </i> Agregar usuario</a>
	</div>
	
	<div class="col-xs-6">
		<div class="form-group">
			<select class="form-control" id="selRole">
				<option value="0">Todos los roles</option>
{foreach item=role from=$roles}
				<option value="{$role.id_role}"{if $role.id_role == $cur_role} selected="selected"{/if}>{$role.role}</option>
{/foreach}
			</select>
		</div>
	</div>
</div>
{if isset($usuarios) && count($usuarios)}
<div class="col-lg-12">
    <table class="table table-bordered table-striped table-condensed">
        <tr>
            <th>Usuario</th>
            <th>Role</th>
            <th></th>
        </tr>
        
        {foreach from=$usuarios item=us}
        <tr>
            <td>{$us.usuario}</td>
            <td>{$us.role}</td>
            <td>
                <a href="{$_layoutParams.root}usuarios/index/permisos/{$us.id}">
                   Permisos
                </a>
                -
                <a href="{$_layoutParams.root}usuarios/index/cambiarClave/{$us.id}">
                   Clave
                </a>
                -
                <a href="{$_layoutParams.root}usuarios/index/eliminar/{$us.id}">
                   Eliminar
                </a>
{if ($us.estado == 2) || ($us.estado == 0)}
                -
                <a href="{$_layoutParams.root}usuarios/index/habilitar/{$us.id}">
                   Habilitar
                </a>

{/if}
            </td>
        </tr>
            
        {/foreach}
    </table>
   </div>
{/if}

<div>
{if $paginado.pag_total > 1}
	<div class="col-lg-9">
		<ul class="pagination" style="margin: 0">
		  <li class="{$paginado.nav_prev.class}"><a {$paginado.nav_prev.url}>&laquo;</a></li>
{foreach item=pagina from=$paginado.paginas}
  		  <li class="{$pagina.class}"><a {$pagina.url}>{$pagina.pag}</a></li>
{/foreach}
  		  <li class="{$paginado.nav_next.class}"><a {{$paginado.nav_next.url}}>&raquo;</a></li>
		</ul>
	</div>
	
<form id="pageSelect">
  <div class="col-lg-3">
    <div class="input-group">
      <span class="input-group-addon">Ir a página</span>
    	<input type="number" id="irAPagina" min="1" max="{$paginado.pag_total}" class="form-control" placeholder="Hay {$paginado.pag_total} págs">
      <span class="input-group-btn">
        <button class="btn btn-default btn-secondary" type="button" id="cmdPaginado">Ir</button>
      </span>
    </div>
  </div>
</form>

  <form method="post" id="buscador">
  	<input type="hidden" name="pagina" id="pagina" val=""> 
  </form>
{/if}
</div>
<br>