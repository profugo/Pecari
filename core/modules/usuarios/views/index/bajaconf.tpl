<h2 class="maintitle">Usuarios<sub>Baja de Usuario</sub></h2>

<p>
    ¿Está seguro que desea eliminar al usuario <strong>"{$usuario.nombre}" ({$usuario.usuario})</strong>? 
</p>

<p>
<a href="{$_layoutParams.root}usuarios/index/eliminar/{$usuario.id}/1" class="btn btn-success">Aceptar</a> <a href="{$_layoutParams.root}usuarios" class="btn btn-success">Cancelar</a>
</p>