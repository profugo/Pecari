<h2 class="maintitle">Usuarios<sub>Cambiar clave de usuario</sub></h2>

<p>
    <strong>Usuario:</strong> {$info.usuario} | <strong>Role:</strong> <a href="{$_layoutParams.root}usuarios/index/setrole/{$info.id}" style="text-decoration:none; border-bottom:1px dotted" title="Cambiar role">{$info.role}</a> 
</p>

<form name="form1" method="post" action="{$_layoutParams.root}usuarios/index/cambiarClave/{$info.id}/1">
    
    <table class="table table-bordered table-striped table-condensed" style="width: 500px;">
{if isset($error)}
		<tr>
			<td colspan=2>
				<p style="text-align:center"><span class="alert alert-error">{$error}</span></p>
			</td>
		</tr>
{/if}
    
        <tr>
            <td style="text-align: right;">Contraseña: </td>
            <td><input type="password" name="clave1" placeholder="Nueva clave" value=""></td>
        </tr>

        <tr>
            <td style="text-align: right;">Confirmar: </td>
            <td><input type="password" name="clave2" placeholder="Repita la clave" value=""></td>
        </tr>
    </table>
        
    <p><button type="submit" class="btn btn-primary"><li class="glyphicon glyphicon-ok icon-ok fa fa-check icon-white"> </li> Confirmar</button></p>
</form>