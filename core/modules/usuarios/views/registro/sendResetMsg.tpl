<h2 class="maintitle">Usuarios<sub>Cambio forzado de contraseña</sub></h2>

<p>
Se envió un correo electrónico a la dirección que usaste para registrarte, usá la nueva clave que te mandamos para ingresar al sitio.
</p>
Gracias,<br>
El equipo de {$app_name}