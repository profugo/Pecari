<div class="avatar-container">
<div class="avatar" style="background-image: url('{$_layoutParams.ruta_logo}')"></div>
</div>
<h2>{$_layoutParams.configs.app_name}<br><sub>Registro</sub></h2>
{if isset($error)}
<div class="alert alert-danger">{$error}</div>
{/if}

<form name="form1" method="post" action="{$_layoutParams.root}usuarios/registro/{$action}">
    <input type="hidden" value="1" name="enviar" />

    <div class="control-group row">
        <label class="control-label col-sm-4" for="inputNombre">Nombre:</label>
        <div class="controls col-sm-8">
            <input type="text" class="form-control" id="inputNombre" placeholder="Nombre" aria-describedby="basic-addon3" name="nombre" value="{$datos.nombre|default:""}">
        </div>
    </div>

    <div class="control-group row">
        <label class="control-label col-sm-4" for="inputUsuario">Usuario:</label>
        <div class="controls col-sm-8">
            <input type="text" class="form-control" id="inputUsuario" placeholder="Usuario" aria-describedby="basic-addon3" name="usuario" value="{$datos.usuario|default:""}">
        </div>
    </div>

  <div class="control-group row">
    <label class="control-label col-sm-4" for="inputEmail">E-mail:</label>
    <div class="controls col-sm-8">
      <input type="text" class="form-control" id="inputEmail" placeholder="E-mail" aria-describedby="basic-addon3" name="email" value="{$datos.email|default:""}">
    </div>
  </div>
      
{if ! isset($update)}  
  <div class="control-group row">
    <label class="control-label col-sm-4" for="inputPassword">Password:</label>
    <div class="controls col-sm-8">
      <input type="password" class="form-control" id="inputPassword" placeholder="Usuario" aria-describedby="basic-addon3" name="pass">
    </div>
  </div>

  <div class="control-group row">
    <label class="control-label col-sm-4" for="inputConfirmar">Confirmar:</label>
    <div class="controls col-sm-8">
      <input type="password" class="form-control" id="inputConfirmar" placeholder="Confirmar" aria-describedby="basic-addon3" name="confirmar">
    </div>
  </div>
{/if}
{if isset($roles)}
        <div class="control-group row">
                <label class="control-label col-sm-4" for="inputRole">Role:</label>
                        <div class="controls col-sm-8">
            <select name="role" class="form-control">
{foreach item=role from=$roles}
                                <option value='{$role.id_role}'{if $role.id_role == $default_role} selected='selected'{/if}>{$role.role}</option>
{/foreach}
            </select>
                </div>
        </div>

{/if}        
<hr>
<div class="row">
    <div class="col-sm-12">
{if isset($update)}
        <a href="{$_layoutParams.root}usuarios/registro/clave" class="pull-left">Cambiar contraseña</a>
{/if}
        <button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-ok icon-ok fa fa-check icon-white"></i> {$txtBoton}</button>
    </div>
  </div>

    </form>