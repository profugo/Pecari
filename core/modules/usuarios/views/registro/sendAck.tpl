Hola <strong>{$datos.nombre}</strong>,
<p>Te registraste en <u>{$datos.app_company}</u>, el proceso de alta de tu cuenta ya está casi completo, para validar tu dirección de correo en el registro tenés que hacer clic  en el siguiente enlace:<br><br>
<a href="{$datos.base_url}">{$datos.base_url}</a>
<br><p>Acordate que el usuario con el que te registraste es: <b>{$datos.usuario}</b></p>
<p>Saludos,<br>El equipo de {$datos.app_company}</p>