Hola <strong>{$datos.nombre}</strong>,
<p>Reestableciste tu contrase&ntilde;a en <u>{$datos.app_company}</u>,
 podés ingresar al sitio haciendo clic en este enlace usando tu nueva clave<br>
<a href="{$datos.base_url}">{$datos.base_url}</a>
<br>
<p>Tu usuario es: {$datos.usuario}</p>

<p>Tu contrase&ntilde;a es: {$datos.pass}</p><p>&nbsp;</p>

<p>Saludos,<br>El equipo de {$datos.app_company}</p>