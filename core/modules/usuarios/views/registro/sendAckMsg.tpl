<h2 class="maintitle">Usuarios<sub>Autenticación de registro</sub></h2>
<p>
Se envió un correo electrónico a la dirección que usaste para registrarte, hacé clic en el enlace que se te envió para poder activar tu cuenta.
</p>
Gracias,<br>
El equipo de {$app_name}