<div class="avatar-container">
<div class="avatar" style="background-image: url('{$_layoutParams.ruta_logo}')"></div>
</div>
<h2>{$_layoutParams.configs.app_name}<br><sub>Cambio de clave</sub></h2>
{if isset($error)}
<div class="alert alert-danger">{$error}</div>
{/if}


    <form name="form1" method="post">
        <input type="hidden" value="1" name="enviar" />

  
  <div class="control-group row">
    <label class="control-label col-sm-4" for="inputPassword">Password:</label>
    <div class="controls col-sm-8">
      <input type="password" class="form-control" name="pass" id="inputPassword" placeholder="Usuario" aria-describedby="basic-addon3" name="pass">
    </div>
  </div>

  <div class="control-group row">
    <label class="control-label col-sm-4" for="inputConfirmar">Confirmar:</label>
    <div class="controls col-sm-8">
      <input type="password" class="form-control" name="confirmar" id="inputConfirmar" placeholder="Confirmar" aria-describedby="basic-addon3" name="confirmar">
    </div>
  </div>

<hr>
<div class="control-group row">
    <div class="col-sm-12">
<div style="width:100%">
	<a href="{$_layoutParams.root}usuarios/registro/perfil" class="btn btn-default btn-secondary">Cancelar</a>
	<button type="submit" class="btn btn-primary" style="float:right"><i class="glyphicon glyphicon-ok icon-ok fa fa-check icon-white"></i> Cambiar</button>
</div>
    
    </div>
  </div>
  

    </form>