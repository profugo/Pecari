<h2 class="maintitle">Usuarios<sub>Activación de cuenta</sub></h2>

{if isset($error)}
<div class="alert alert-danger">{$error}</div>
{/if}
{if isset($mensaje)}
<div class="alert alert-success">{$mensaje}</div>
{/if}
<p></p>

<a href="{$_layoutParams.root}">Ir al Inicio</a>

{if !Session::get('autenticado')}
    
    | <a href="{$_layoutParams.root}login">Iniciar Sesi&oacute;n</a>

{/if}