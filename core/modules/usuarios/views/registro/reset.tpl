<h2 class="maintitle">Usuarios<sub>Cambio de contraseña</sub></h2>
<hr>
<p>
En este caso lo que podemos hacer es crear una nueva clave y enviártela a la dirección de correo electrónico con la que te registraste en el sitio.<br>
Si querés que te creemos una clave nueva y te la mandemos por correo electrónico, hacé clic en el botón <code>Resetear clave</code> o en el botón <code>Cancelar</code> para volver a la pantalla de inicio.
</p>
<hr>
<p style="text-align:right">
<a href="{$_layoutParams.root}" class="btn btn-default btn-secondary">Cancelar</a>
<a href="{$_layoutParams.root}usuarios/registro/reset/{$uid}/{$codigo}" class="btn btn-default">Resetear clave</a>
</p>