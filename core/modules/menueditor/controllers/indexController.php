<?php
/**
 * Controlador de modulo de editor de menús
 *
 * PHP Version 7.2
 *
 * @category Framework.core.modules.menueditor
 * @package  Framework_basico
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/menueditor/controllers/indexController.php
 *
 */

class indexController extends menueditorController
{
    /**
     * Constructor predeterminado
     * 
     * @return null
     */
    public function __construct() 
    {
        parent::__construct("index");
    }
    
    /**
     * Index de menueditor
     * Muestra lista de usuarios de menús definidos en el sistema
     * 
     * @see usuariosController::ind activeex()
     * 
     * @return html
     */
    public function index()
    {
        $this->_view->assign("menus", $this->_modelo->getMenus());
    	$this->_view->renderizar("lista");
    }
}