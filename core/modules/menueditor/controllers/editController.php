<?php
/**
 * Controlador de modulo de editor de menús
 *
 * PHP Version 7.2
 *
 * @category Framework.core.modules.menueditor
 * @package  Framework_basico
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/menueditor/controllers/indexController.php
 *
 */

class editController extends menueditorController
{
    /**
     * Constructor predeterminado
     * 
     * @return null
     */
    public function __construct() 
    {
        parent::__construct("edit");
    }
    
    /**
     * Index de menueditor
     * Muestra lista de usuarios de menús definidos en el sistema
     * 
     * @see usuariosController::ind activeex()
     * 
     * @return html
     */
    public function index($menu = false)
    {
        if ($menu == false) {
            $this->redireccionar("MenuEditor");
            return;
        }
        
        $menu = $this->filtrarSQL($menu);
        $this->_view->assign("menu", $this->_modelo->getMenu($menu));
        $this->_view->setCss("https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css", "R");
        $this->_view->setJs("edit");
        $this->_view->renderizar("edit");
    }
    
    
}