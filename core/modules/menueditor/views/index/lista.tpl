<h2 class="maintitle">Menús del sistema<sub>ABM de menús de la aplicación</sub></h2>

<div class="list-group">
{foreach item=menu from=$menus}    
  <a href="{$_layoutParams.root}MenuEditor/edit/{$menu.clave}" class="list-group-item list-group-item-action">{$menu.nombre}</a>
{/foreach}
</div>