<?php
/**
 * Modelo del controlador index del modulo de edicion de menús
 *
 * PHP Version 5.4
 *
 * @category Framework.core.modules.menueditor
 * @package  Sistema_Basico
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/menueditor/models/indexModel.php
 *
 */

/**
 * Modelo del controlador index del modulo de edicion de menús
 *
 * @category Framework.core.modules.menueditor
 * @package  Sistema_Basico
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/menueditor/controllers/indexModel.php
 */

class EditModel extends Model
{
    
    /**
     * Controlador predeterminado
     * 
     *  @return null
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    
    public function getMenu($clave)
    {
        $menu = $this->_db->query(
            "SELECT valor FROM " . FW_PREFIX . "tuplas WHERE clave='{$clave}'"
        )->fetch(PDO::FETCH_ASSOC);
            
        return isset($menu["valor"]) ? json_decode($menu["valor"]) : false;
    }
}