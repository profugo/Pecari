<?php
/**
 * Modelo del controlador index del modulo de edicion de menús
 *
 * PHP Version 5.4
 *
 * @category Framework.core.modules.menueditor
 * @package  Sistema_Basico
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/menueditor/models/indexModel.php
 *
 */

/**
 * Modelo del controlador index del modulo de edicion de menús
 *
 * @category Framework.core.modules.menueditor
 * @package  Sistema_Basico
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/modules/menueditor/controllers/indexModel.php
 */

class IndexModel extends Model
{
    
    /**
     * Controlador predeterminado
     * 
     *  @return null
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getMenus()
    {
        $datos = $this->_db->query("
            SELECT clave, valor FROM " . FW_PREFIX . "tuplas ORDER BY clave
        ")->fetchAll(PDO::FETCH_ASSOC);
        
        $ret = array();
        if (is_array($datos)) {
            foreach($datos as $dato) {
                $valor = json_decode($dato["valor"], true);
                $ret[] = array(
                    "clave" => $dato["clave"], 
                    "nombre" => $valor["nombre"]
                );
            }
        } else {
            $ret = false;
        }
        return $ret;
    }
}