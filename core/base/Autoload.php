<?php
/**
 * Módulo de control de carga automática de clases
 *
 * PHP Version 5.4
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Autoload.php
 *
 */

/**
 * Cargar automáticamente clases de aplicación
 *  
 * @param string $class nombre de clase
 * 
 * @return null
 */
function autoloadCore( $class )
{ 
    $classFile = APP_PATH . ucfirst(strtolower($class)) . '.php';
    if ( file_exists($classFile) ) {
        include_once $classFile;
    }
}

/**
 * Cargar clases de biblioteca automáticamente
 * 
 * @param string $class nombre de clase
 * 
 * @return null
 */
function autoloadLibs($class)
{
    $path = 'libs' . DS . 'class.' . strtolower($class) . '.php';
    $classFileFW =  CORE_PATH . $path;
    $classFileUser =  USER_PATH . $path;
    
    if ( file_exists($classFileUser) ) {
        include_once $classFileUser;
    } elseif ( file_exists($classFileFW) ) {
        include_once $classFileFW;
    }
}

/**
 * Cargar clases de extensión automáticamente
 * 
 * @param string $class nombre de clase
 * 
 * @return null
 */
function autoloadExtensions( $class )
{
    $classFile = USER_PATH . 'extensions' . DS . 
        ucfirst(strtolower($class)) . '.php';
    if ( file_exists($classFile) ) {
        include_once $classFile;
    }
}

/**
 * Agregar funciones al spool de carga automática de clases
 */
spl_autoload_register("autoloadCore");
spl_autoload_register("autoloadLibs");
spl_autoload_register("autoloadExtensions");
