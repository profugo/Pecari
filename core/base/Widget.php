<?php
/**
 * Clase de gestión de widgets
 *
 * PHP Version 5.4
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Widget.php
 *
 */

/**
 * Clase de gestión de widgets
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Widget.php
 */

abstract class Widget
{
    /**
     * Cargar modelo específico del widget
     * 
     * @param string $model nombre del modelo
     * 
     * @throws Exception en error
     * 
     * @return class instancia de modelo
     */
    protected function loadModel( $model )
    {
        $path = CORE_PATH . 'widgets' . DS . 'models' . DS . $model . '.php';
        if ( is_readable($path) ) {
            include_once $path;
            
            $modelClass = $model . 'ModelWidget';
            
            if ( class_exists($modelClass) ) {
                return new $modelClass;
            }
        }
        
        throw new Exception('error modelo de widget');
    }
    
    /**
     * Renderizar vista del widget
     * 
     * @param string  $view vista del widget
     * @param unknown $data cosas, no se bien que
     * @param string  $ext  extension de la vista
     * 
     * @throws Exception en error
     * 
     * @return html
     */
    protected function render( $view, $data = array(), $ext = 'phtml' )
    {
        $path = CORE_PATH . 'widgets' . DS . 'views' . DS . $view . '.' . $ext;
        if (is_readable($path)) {
            ob_start();
            extract($data);
            include $path;
            $content = ob_get_contents();
            ob_end_clean();
            
            return $content;
        }

        throw new Exception('Error vista widget');
    }
}