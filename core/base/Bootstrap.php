<?php
/**
 * Clase de cebador de clases
 *
 * PHP Version 5.4
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Bootstrap.php
 *
 */

/**
 * Clase de cebador de clases
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Bootstrap.php
 */

class Bootstrap
{
    /**
     * Cebador de clases
     * Carga las clases, verifica la existencia de los metodos, prepara los
     * parámetros y ejecuta el método solicitado o llama al metodo index
     * 
     * @param Request $peticion Instancia de clase request creada en el index
     * 
     * @throws Exception en caso de no encontrar el método 
     * 
     * @return null
     */
    public static function run(Request $peticion)
    {
        $modulo     = $peticion->getModulo();
        $controller = $peticion->getControlador() . 'Controller';
        $metodo     = $peticion->getMetodo();
        $args       = $peticion->getArgs();
        
        if ( $modulo ) {
            $rutaModulo     = CORE_PATH . 'mvc' . DS . 
                'controllers' . DS . $modulo . 'Controller.php';
            $rutaModuloUser = USER_PATH . 'mvc' . DS . 
                'controllers' . DS . $modulo . 'Controller.php';
            
            
            if ( is_readable($rutaModulo) || is_readable($rutaModuloUser) ) {
                $rutaControlador = CORE_PATH . 'modules'. DS . $modulo . DS . 
                    'controllers' . DS . $controller . '.php';
                if ( is_readable($rutaModuloUser) ) {
		
                    $rutaModulo = $rutaModuloUser;
                    $rutaControlador = USER_PATH . 'modules'. DS . 
                        $modulo . DS . 'controllers' . DS . $controller . '.php';
                }
                include_once $rutaModulo;
            } else {
                throw new Exception('No base module controller found');
            }
        } else {
            $rutaControlador = CORE_PATH . 'mvc' . DS . 
                'controllers' . DS . $controller . '.php';
        }
        
        if ( is_readable($rutaControlador) ) {
            include_once $rutaControlador;
            $controller = new $controller;
            
            if ( is_callable(array( $controller, $metodo )) ) {
                $metodo = $peticion->getMetodo();
            } else {
            	array_unshift($args, $metodo);
                $metodo = 'index';
            }
            
            if ( isset( $args ) ) {
                call_user_func_array(array( $controller, $metodo ), $args);
            } else {
                call_user_func(array( $controller, $metodo ));
            }
            
        } else {
            throw new Exception('Bootstrap: Controller not found (' . $peticion->_url . ' [' . $rutaControlador . '])');
        }
    }
}
