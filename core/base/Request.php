<?php
/**
 * Clase de gestión de peticiones http
 *
 * PHP Version 5.4
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Request.php
 *
 */

/**
 * Clase de gestión de peticiones http
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Request.php
 */

class Request
{
    private $_modulo;
    private $_controlador;
    private $_metodo;
    private $_argumentos;
    private $_modules;
    private $_hashme;
    public  $_url;
    /**
     * Constructor de clase
     * Parsea la url y obtiene modulo, metodo y argumentos
     * 
     * @param array $modulos modulos activos del sistema
     * 
     * @return null
     */
    public function __construct( $modulos = false ) 
    {
        $sesskey = md5(time());
        $postkey = md5(@implode("-", $_POST));
        $urlkey  = md5($this->curPageURL());
        
        $this->_hashme = md5($sesskey . $postkey . $urlkey);

        if ( isset( $_GET[ 'url' ] ) ) {
            $url = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_URL);
            
            // Esto es un hack para subdirectorios en nginx
            $root = parse_url(BASE_URL, PHP_URL_PATH);
            if( strpos(" " . $url, $root) != false ) {
            	$url = substr($url, strlen($root) - 1);
            }
            // -- Fin del hack
            
            $this->_url = $url;
            $url = explode('/', $url);
            //$url = array_filter($url);
            
            /* modulos de la app */
            if ( $modulos ) {
                $this->_modules = $modulos;
            } else {	
                $this->_modules = array( );
            }
            
            $this->_modulo  = strtolower(array_shift($url));
            
            if ( ! $this->_modulo ) {
                $this->_modulo = false;
            } else {
                if ( count($this->_modules) ) {
                    if ( ! in_array($this->_modulo, $this->_modules) ) {
                        $this->_controlador = $this->_modulo;
                        $this->_modulo      = false;
                    } else {
                        $this->_controlador = strtolower(array_shift($url));
                        
                        if ( ! $this->_controlador ) {
                            $this->_controlador = 'index';
                        }
                    }
                } else {
                     $this->_controlador = $this->_modulo;
                     $this->_modulo      = false;
                }
            }
            
            $this->_metodo     = strtolower(array_shift($url));
            $this->_argumentos = $url;    
        } else {
            if ( ! $this->_controlador &&  ! $this->_metodo ) {
                if ( DEFAULT_MODULE != '' ) {
                    $this->_modulo = DEFAULT_MODULE;
                }
            }
        }       
                
        if ( ! $this->_controlador ) {
            $this->_controlador = DEFAULT_CONTROLLER;
        }
        
        if ( ! $this->_metodo ) {
            $this->_metodo = 'index';
        }
        
        if ( ! isset( $this->_argumentos ) ) {
            $this->_argumentos = array();
        }


        //~ echo "mod: $this->_modulo<hr>";
        //~ echo "ctrl: $this->_controlador<hr>";
        //~ echo "met: $this->_metodo<hr>";
    }
    
    /**
     * Obtener el módulo de la url
     * 
     * @return string
     */
    public function getModulo()
    {
        return $this->_modulo;
    }
    
    /**
     * Obtener el controlador de la url
     * 
     * @return string
     */
    public function getControlador()
    {
        return $this->_controlador;
    }
    
    /**
     * Obtener el método de la url
     * 
     * @return string
     */
    public function getMetodo()
    {
        return $this->_metodo;
    }
    
    /**
     * Obtener argumentos del request
     * 
     * @return array argumentos
     */
    public function getArgs()
    {
        return $this->_argumentos;
    }

    /**
     * Obtener el hash de la sesión
     * 
     * @return string
     */
    public function getHashme()
    {
        return $this->_hashme;
    }
    
    /**
     * Obtener la url completa actual
     * 
     * @return string url
     */
    public function curPageURL() 
    {
        $pageURL = 'http';
        if ( isset( $_SERVER[ "HTTPS" ] ) ) {
            if ( $_SERVER[ "HTTPS" ] == "on" ) {
                $pageURL .= "s";
            }
        }
        $pageURL .= "://";
        if ( $_SERVER[ "SERVER_PORT" ] != "80") {
            $pageURL .= $_SERVER[ "SERVER_NAME" ] . ":" . 
                $_SERVER[ "SERVER_PORT" ] . $_SERVER[ "REQUEST_URI" ];
        } else {
            $pageURL .= $_SERVER[ "SERVER_NAME" ] . $_SERVER[ "REQUEST_URI" ];
        }
        return $pageURL;
    }
}
