<?php
/**
 * Clase de base para modelos
 *
 * PHP Version 5.4
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Model.php
 *
 */

/**
 * Clase de base para modelos
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Model.php
 */

class Model
{
    private   $_registry;
    protected $_db;
    private   $_properties = array();
    
    /**
     * Constructor de clase
     * 
     * @return null
     */
    public function __construct() 
    {
        $this->_registry = Registry::getInstancia();
        $this->_db       = $this->_registry->_db;
    }
    
    private function _getJson($container) {
        $container = $this->getGen($container);
        return strlen($container) == 0 ? false : json_decode($container, true);
    }
    
    public function __get($name)
    {
        if (isset($this->_properties[$name])) {
            return $this->_properties[$name];
        } else {
            Controller::staticRedireccionar("error/1003/" . $name);
        }
    }

    public function __set($name, $value) 
    {
        $this->_properties[$name] = $value;
    }
    
    /**
     * Obtener valor de tabla de configuración general
     * 
     * @param string $field campo
     * 
     * @return string valor
     */
    public function getGen( $field )
    {
        $dato = $this->_db->query(
            "SELECT valor FROM " . FW_PREFIX . "gen_conf WHERE campo='{$field}'"
        );
        $dato = $dato->fetch(PDO::FETCH_ASSOC);
        return $dato[ 'valor' ];        
    }
    
    /**
     * Actualizar el valor de un campo en tabla de configuración general
     * 
     * @param string $field campo
     * @param string $value valor
     * 
     * @return null
     */
    public function setGen( $field, $value)
    {
        $tabla = FW_PREFIX . "gen_conf";
        return $this->_db->query("
            INSERT INTO {$tabla} ( campo, valor )
            VALUES ( '{$field}', '{$value}' )
            ON DUPLICATE KEY UPDATE
                valor=VALUES(valor)
            "
        );
    }
    
    /**
     * Guardar par campo-valor en tabla de configuración general
     * 
     * @param string $field campo
     * @param string $value valor
     * 
     * @return null
     */
    public function insGen( $field, $value )
    {
        return $this->setGen($field, $value);
    }
    
    /**
     * Método que permite guardar y recuperar campos en un array en formato
     * json en la tabla gen_conf.
     * Si se omite el valor de $value se recupera el valor del campo $field, si
     * se establece se crea o actualiza el valor del campo $field.
     * 
     * @param string $container : Nombre del contenedor
     * @param type $field       : Campo a recuperar
     * @param type $value       : Valor del campo
     */
    public function dbJson($container, $field, $value = false)
    {
        $json = $this->_getJson($container);
        if ($json == false) {
            $json = array();
        }
        
        if ($value == false) {     
            return isset($json[$field]) == true ?$json[$field] : false;
        }

        $json[$field] = $value;
        $this->setGen($container, json_encode($json));
    }
}
