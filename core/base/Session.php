<?php
/**
 * Clase de gestión de sesión
 *
 * PHP Version 5.4
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Session.php
 *
 */

/**
 * Clase de gestión de sesión
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Session.php
 */

//require_once CORE_PATH . DS . 'libs' . DS . 'inc.session.php';

class Session
{
    
    /**
     * Iniciar sesión PHP
     * 
     * @return null
     */
    public static function init()
    {
    	if (SESSION_TIME != 0) {
    		ini_set("session.cookie_lifetime","432000");
    		ini_set('session.gc_maxlifetime', SESSION_TIME * 60);
    	}

        self::start_session(self::_getSessId());
//    	session_name(self::_getSessId());
//        session_start();
        $_SESSION[self::_getSessId()]["pecari"] = VERSION;
        
        $allow_register = Configuration::loadStatic("usuarios");
        if ($allow_register["allow_register"] == 1) {
        	self::set("allow_register", 1);
        } else {
        	self::destroy("allow_register");
        }
        
        
        self::tiempo();
    }
    
    
    /**
     * Loads the session id cookie value from the configuration files
     * 
     * @return string session id
     */
    private static function _getSessId()
    {
    	$session_cfg = Configuration::loadStatic("session");
    	return $session_cfg["session_id"];
    }
    
    /**
     * 
     * @return $_SESSION
     */
    private static function _getSession()
    {
        return $_SESSION[self::_getSessId()];
    }
    /**
     * Destruír sesión si no pasa parametro
     * si pasa parametro elimina el campo de sesión
     * 
     * @param string $clave campo
     * 
     *  @return null
     */
    public static function destroy( $clave = false )
    {
    	$session = self::_getSession();
    	
        if (! $clave) {
            session_name(self::_getSessId());
            @session_destroy();
            return;
        }
        
        if ( is_array($clave) ) {
            for ($i = 0, $l = count($clave); $i < $l; $i++) {
                if ( isset( $session[ $clave[ $i ] ] ) ) {
                    unset( $session[ $clave[ $i ] ] );
                }
            }
        } else {
            if ( isset( $session[ $clave ] ) ) {
                unset( $session[ $clave ] );
            }
        }
        
    }
    
    /**
     * Establecer campo de sesión
     * 
     * @param string $clave campo
     * @param string $valor valor
     * 
     * @return false
     */
    public static function set( $clave, $valor )
    {
    	$session_id = self::_getSessId();
    	
        if ( ! empty( $clave ) ) {
            $_SESSION[ $session_id ][ $clave ] = $valor;
        }
    }
    
    /**
     * Obtener campo de sesión
     * 
     * @param string $clave campo
     * 
     * @return string valor
     */
    public static function get( $clave )
    {
    	$session = self::_getSession();
    	
        if ( isset( $session[ $clave ] ) ) {
            return $session[ $clave ];
        }
    }
    
    /**
     * Verifica que se tenga permiso de acceso
     * 
     * @param int $level nivel de acceso
     * 
     * @return null
     */
    public static function acceso( $level )
    {
        if ( ! Session::get('autenticado') ) {
            header('location:' . BASE_URL . 'error/access/5050');
            exit;
        }
        
        Session::tiempo();
        
        if ( Session::getLevel($level) > Session::getLevel(Session::get('level')) ) {
            header('location:' . BASE_URL . 'error/access/5050');
            exit;
        }
    }
    
    /**
     * Verficar que el usuario en sesión tenga permiso de vista
     * 
     * @param string $level nivel de acceso
     * 
     * @return boolean
     */
    public static function accesoView( $level )
    {
        $retorno = true;
        
        if ( ! Session::get('autenticado') ) {
            $retorno = false;
        }
        if ( Session::getLevel($level) > Session::getLevel(Session::get('level')) ) {
            $retorno = false;
        }
        
        return $retorno;
    }
    
    /**
     * Obtener el nivel de acceso
     * 
     * @param string $level clave de nivel
     * 
     * @throws Exception si no existe clave
     * 
     * @return int nivel
     */
    public static function getLevel( $level )
    {
        $retorno = '';
        $role[ 'admin'    ] = 3;
        $role[ 'especial' ] = 2;
        $role[ 'usuario'  ] = 1;
        
        if ( array_key_exists($level, $role) ) {
            $retorno = $role[ $level ];
        } else {    
            throw new Exception('Error de acceso');
        }
    }
    
    /**
     * Verifica si la sesión actual existe en la lista de niveles permitidos
     * 
     * @param array $level   lista de niveles permitidos
     * @param bool  $noAdmin true si se debe bloquear a admin 
     * 
     * @return bool
     */
    public static function accesoEstricto( array $level, $noAdmin = false )
    {
        $retorno = fase;
        
        if ( ! Session::get('autenticado') ) {
            header('location:' . BASE_URL . 'error/access/5050');
            exit;
        }
        
        Session::tiempo();
        
        if ( $noAdmin == false ) {
            if ( Session::get('level') == 'admin' ) {
                $retorno = true;
            }
        }
        
        if ( count($level) ) {
            if ( in_array(Session::get('level'), $level) ) {
                $retorno = true;
            }
        }
        
        if ( ! $retorno ) {
            header('location:' . BASE_URL . 'error/access/5050');
        }
    }
    
    /**
     * Verifica que tenga acceso específico
     * 
     * @param array $level   niveles de acceso
     * @param bool  $noAdmin true si se desea bloquear a admin
     * 
     * @return bool true si tiene acceso
     */
    public static function accesoViewEstricto( array $level, $noAdmin = false )
    {
        if ( ! Session::get('autenticado') ) {
            return false;
        }
        
        if ( $noAdmin == false ) {
            if ( Session::get('level') == 'admin' ) {
                return true;
            }
        }
        
        if ( count($level) ) {
            if ( in_array(Session::get('level'), $level) ) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Verificar que no haya caducado el tiempo de sesión
     * 
     * @throws Exception si no se definió tiempo de sesion
     * 
     * @return update de tiempo o redireccion
     */
    public static function tiempo()
    {
        if ( ! defined('SESSION_TIME') ) {
            throw new Exception('No se ha definido el tiempo de sesion'); 
        }
        
        
        if ( Session::get('tiempo') && SESSION_TIME != 0 ) {
            if ( time() - Session::get('tiempo') > ( SESSION_TIME * 60 ) ) {
                Session::destroy();
                header('location:' . BASE_URL . 'error/access/8080');
            } else {
                Session::set('tiempo', time());
            }
        }
    }
    
    public static function start_session($session_name) {
       // Make sure the session cookie is not accessible via javascript.
       $httponly = true;

       $secure = false;
       
       // Force the session to only use cookies, not URL variables.
       ini_set('session.use_only_cookies', 1);

       // Get session cookie parameters 
       $cookieParams = session_get_cookie_params(); 
       // Set the parameters
       session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
       // Change the session name 
       session_name($session_name);
       // This line regenerates the session and delete the old one. 
       // It also generates a new encryption key in the database. 
       @session_regenerate_id(true); 
       // Now we cat start the session
       @session_start();
       
    }
}
