<?php
/**
 * Clase de manejo de vistas
 *
 * PHP Version 5.4
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/View.php
 *
 */

require_once CORE_PATH .  'libs' . DS . 'smarty' . DS . 
    'libs' . DS . 'Smarty.class.php';

/**
 * Clase de manejo de vistas
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/View.php
 */
class View extends Smarty
{
    private $_request;
    private $_js;
    private $_css;
    private $_acl;
    private $_rutas;
    private $_jsPlugin;
    private $_cssPlugin;
    private $_template;
    private $_item;
    private $_altMenu;
    private $_config;
    private $template_url;
    
    /**
     * Constructor de vista
     * 
     * @param Request $peticion instancia de la petición
     * @param ACL     $_acl     instancia de lista de permisos
     * 
     * @return null
     */
    public function __construct( Request $peticion, ACL $_acl ) 
    {
        parent::__construct();
        $this->_request  = $peticion;
        $this->_js       = array();
        $this->_css         = array();
        $this->_acl      = $_acl;
        $this->_rutas    = array();
        $this->_jsPlugin = array();
        $this->_cssPlugin = array();
        $this->_template = DEFAULT_LAYOUT;
        $this->_item     = '';
        $this->_altMenu  = array();
        $reg = Registry::getInstancia();
        $this->_config   = $reg->_config;
        
        if ( is_readable(USER_PATH . 'extensions' . DS . 'Extendmenu.php') ) {
            $this->_altMenu = ExtendMenu::getMenu($this->_acl);
        }
        
        $modulo      = $this->_request->getModulo();
        $controlador = $this->_request->getControlador();
        
        $user_url = defined("APP_URL") ? APP_URL . "application/" : USER_URL;
        
        if ( $modulo ) {
            if ( is_readable(CORE_PATH . 'modules' . DS . $modulo) ) {
                $this->_rutas[ 'view' ] = CORE_PATH . 'modules' . DS . 
                    $modulo . DS . 'views' . DS . $controlador . DS;
                $this->_rutas[ 'js'   ] = CORE_URL . 'modules/' . 
                    $modulo . '/views/' . $controlador . '/js/';
                $this->_rutas[ 'css'  ] = CORE_URL . 'modules/' . 
                    $modulo . '/views/' . $controlador . '/css/';
            } else {
                $this->_rutas[ 'view' ] = USER_PATH . 'modules' . 
                    DS . $modulo . DS . 'views' . DS . $controlador . DS;
                $this->_rutas[ 'js'   ] = $user_url . 'modules/' .
                    $modulo . '/views/' . $controlador . '/js/';
                $this->_rutas[ 'css'  ] = $user_url . 'modules/' . 
                    $modulo . '/views/' . $controlador . '/css/';
            }
        } else {
            $this->_rutas[ 'view' ] = CORE_PATH . 'mvc' . DS . 
                'views' . DS . $controlador . DS;
            $this->_rutas[ 'js'   ] = CORE_URL . 'mvc/views/' . 
                $controlador . '/js/';
            $this->_rutas[ 'css'  ] = CORE_URL . 'mvc/views/' . 
                $controlador . '/css/';
        }

        $tmp1 = ROOT . 'application' . DS . 'mvc' . DS . 'views' . 
            DS . 'layout'. DS . $this->_template . DS;
        $tmp2 = CORE_PATH . 'mvc' . DS . 'views' . DS . 
            'layout'. DS . $this->_template . DS;
        
        
        if ( is_readable($tmp1 . 'template.tpl') ) {
            $this->template_dir = $tmp1;
            $this->config_dir   = $tmp1 . 'configs' . DS;
            $this->template_url = BASE_URL . "application/mvc/views/layout/";
        } else {
            $this->template_dir = $tmp2;
            $this->config_dir   = $tmp2 . 'configs' . DS;
            $this->template_url = CORE_URL . "mvc/views/layout/";
        }
        
        if (defined('PILL_PATH')) {
        	$this->cache_dir    = PILL_PATH . 'tmp' . DS .'cache' . DS;
        	$this->compile_dir  = PILL_PATH . 'tmp' . DS .'template' . DS;
        } else {
        	$this->cache_dir    = ROOT . 'tmp' . DS .'cache' . DS;
        	$this->compile_dir  = ROOT . 'tmp' . DS .'template' . DS;
		}
		
		//~ echo $this->template_dir;exit;
    }
    
    /**
     * Cargar array de vistas multiniveles
     * 
     * @param array $elementos lista de nombres de vista
     * 
     * @throws Exception en caso de fallo
     * 
     * @return multitype:string
     */
    public function setMultiview( $elementos = array() )
    {
        $retorno = array();
        
        foreach ( $elementos as $elemento ) {
            if ( is_readable($this->_rutas[ 'view' ] . $elemento . '.tpl') ) {
                $retorno[] = $this->_rutas[ 'view' ] . $elemento . '.tpl';
            } else {
                throw new Exception(
                    'View error: ' . $this->_rutas[ 'view' ] . $elemento . '.tpl'
                );
            }    
        }
        
        return $retorno;
    }
    
     /**
     * Elimina entradas duplicadas, si el menú alterno tiene entradas que 
     * ya existen en el menú de base, elimina la referencia en el menú de 
     * base y deja la del menú alterno
     * 
     * @param array $menu array del menú de base
     *  
     * @return array $menu con el menú de base sin los elementos repetidos
     */
    private function _removeDuplicateMenues($menu)
    {
		foreach ( $this->_altMenu as $entry)
		{
			$remove = -1;
			for ($i = 0; $i < count($menu); $i++)
			{
				if (isset($menu[$i]["id"])) {
					if ($menu[$i]["id"] == $entry["id"]) {
						$remove = $i;
						break;
					}
				}
			}
			
			if ($remove >= 0) {
				unset($menu[$remove]);
			}
		}
		return array_values($menu);
	}
    
    /**
     * Cargar la vista con los menues solicitados
     * 
     * @param string $vista    nombre de la vista a cargar
     * @param string $item     id del menú a resaltar (opcional)
     * @param bool   $noLayout Ocultar el layout y cargar solo la vista
     *  
     * @throws Exception en caso de fallo
     * 
     * @return html
     */
    public function renderizar( $vista, $item = false, $noLayout = false, $dynCss = false )
    {
        if ( $item ) {
            $this->_item = $item;
        }
        
        $ruta_view = $this->_rutas[ 'view' ];
        
        if ($dynCss != false) {
            switch ($dynCss) {
                case "A" :
                    $this->_rutas[ 'view' ] = ROOT . 'public/css/dynamic/';
                    break;
                case "F" :
                    $this->_rutas[ 'view' ] = CORE_PATH . 'mvc/views/dynCss/';
                    break;
                default :
                    throw new Exception('View error: Wrong dynCss value, use A or F');
            }
            header("Content-type: text/css");
        }
        
        $this->_config->load("basemenu");
        $this->_config->load("smarty");
        
        $menu = $this->_config->item("basemenu", "menu");
        if ( is_array($this->_altMenu) && count($this->_altMenu) > 0 ) {
            $menu = array_merge($this->_removeDuplicateMenues($menu), $this->_altMenu);
        }

        $template_base = $this->template_url  . $this->_template;
        
        $url_template_logo = $template_base . '/img/' . APP_LOGO;
        $url_app_logo = BASE_URL . 'public/img/' . APP_LOGO;
        
        $ruta_template_logo = $this->template_dir . $this->_template . "/img/" . APP_LOGO;
       	$ruta_app_logo = ROOT . 'public/img/' . APP_LOGO; 
       
       	// Busca el logo asignado en la configuración
       	$ruta_logo = "";
	    if (is_readable($ruta_app_logo) == true ) { 
	    	$ruta_logo = $url_app_logo;
	    } elseif (is_readable($ruta_template_logo)) {
	    	$ruta_logo = $url_template_logo;
	    } else {
	    	$ruta_logo = CORE_URL . '../public/img/pecari_logo_white.gif';
	    }

	    $url_params = explode("/", $this->_request->_url);
        $_params = array(
            'url_params' => $url_params,
            'ruta_css'   => $template_base . '/css/',
            'ruta_img'   => $template_base . '/img/',
            'ruta_js'    => $template_base . '/js/',
            'ruta_fw'    => FW_URL,
        	'ruta_logo'  => $ruta_logo,
            'logo_img'   => APP_LOGO,
            'logo_style' => LOGO_CSS,
            'menu'       => $menu,
            'item'       => $this->_item,
            'js'         => $this->_js,
            'js_plugin'  => $this->_jsPlugin,
            'css'        => $this->_css,
            'css_plugin' => $this->_cssPlugin,
            'root'       => BASE_URL,
            'configs'    => array(
                'app_name'    => APP_NAME,
                'app_slogan'  => APP_SLOGAN,
                'app_company' => APP_COMPANY
            )
        );
                
        if ( is_readable($this->_rutas[ 'view' ] . $vista . '.tpl') ) {
            if ( $noLayout ) {
                $this->assign('_layoutParams', $_params);
                $this->template_dir = $this->_rutas[ 'view' ];
                $this->display($this->_rutas[ 'view' ] . $vista . '.tpl');
                $this->_rutas[ 'view' ] = $ruta_view;
                return;
            }
            
            $this->assign('_contenido', $this->_rutas[ 'view' ] . $vista . '.tpl');
        } else {
            throw new Exception('View error (' . $this->_rutas["view"] . $vista . '.tpl)');
        }
        
        $this->assign('widgets', $this->_getWidgets());
        $this->assign('_acl', $this->_acl);
        $this->assign('_layoutParams', $_params);
        
        $this->caching = $this->_config->item("smarty", "smarty_caching");
        $this->cache_lifetime = $this->_config->item("smarty", "smarty_lifetime");
        
        $this->display('template.tpl', $this->_request->getHashme());
        $this->_rutas[ 'view' ] = $ruta_view;
    }
    
    
    /**
     * Cargar plugins js de la vista
     * 
     * @param array $js lista de nombre de plugins js
     * @param string $where opcional, P: cambia funcion a setJsPlugin, C: cambia función a setJsCorePlugin
     * @throws Exception si no encuentra archivo
     * 
     * @return null
     */
    public function setJs( $js, $where = false )
    {
    	if ($where == "P") {
            $this->setJsPlugin($js);
            return;
    	}
    	if ($where == "C") {
            $this->setJsCorePlugin($js);
            return;
    	}
    	if ($where == "R") {
            $this->setJsRemote($js);
            return;
        }
        
    	if (is_string($js)) {
    		$js = array($js);
    	}
    	
        if (is_array($js) && count($js) ) {
            foreach ( $js as $item ) {
                $this->_js[] = $this->_rutas['js'] . $item . '.js';
            }
        } else {
            throw new Exception('Js error');
        }
    }
    
    /**
     * Cargar plugins js de la aplicación
     * 
     * @param array $js lista de nombre de plugins js
     * 
     * @throws Exception si no encuentra archivo
     * 
     * @return null
     */
    public function setJsPlugin($js)
    {
    	if (is_string($js)) {
    		$js = array($js);
    	}
    	
    	$base_url = defined("APP_URL") ? APP_URL : BASE_URL;
    	 
        if ( is_array($js) && count($js) ) {
            foreach ( $js as $item ) {
                $this->_jsPlugin[] = $base_url . 'public/js/' .  $item . '.js';
            }
        } else {
            throw new Exception('Js error: Plugin error');
        }
    }
    
    public function setJsRemote($js)
    {
        if (is_string($js)) {
            $js = array($js);
        }
        
        if (is_array($js) && count($js)) {
            foreach ($js as $item) {
                $item = filter_var($item, FILTER_SANITIZE_URL);
                $this->_jsPlugin[] = $item;
            }
        } else {
            throw new Exception('JS error: setJsRemote() error');
        }
    }
    
    /**
     * Cargar plugins js del framework
     * 
     * @param array $js lista de nombre de plugins js
     * 
     * @throws Exception si no encuentra archivo
     * 
     * @return null
     */
    public function setJsCorePlugin($js)
    {
    	if (is_string($js)) {
    		$js = array($js);
    	}
    	
        if ( is_array($js) && count($js) ) {
            foreach ( $js as $item ) {
                $this->_jsPlugin[] = FW_URL . 'public/js/' .  $item . '.js';
            }
        } else {
            throw new Exception('Js error: Core error');
        }
    }
    
    /**
     * Establecer hojas de estilo
     * 
     * @param array $css hojas de estilo
     * @param string $where opcional, P: cambia funcion a setCssPlugin, C: cambia función a setCssCorePlugin
     * 
     * @throws Exception
     * 
     * @return null
     */
    public function setCss( $css, $where = false )
    {
    	if ($where == "P") {
    		$this->setCssPlugin($css);
    		return;
    	}
    	
    	if ($where == "C") {
    		$this->setCssCorePlugin($css);
    		return;
    	}
    	
    	if ($where == "D") {
    		$this->setCssDinamic($css);
    		return;
    	}
    	
        if ($where == "R") {
            $this->setCssRemote($css);
            return;
        }
        
    	if (is_string($css)) {
    		$css = array($css);
    	}
    	
        if (is_array($css) && count($css) ) {
            foreach ( $css as $item ) {
                $this->_css[] = $this->_rutas['css'] . $item . '.css';
            }
        } else {
            throw new Exception('CSS error');
        }
    }
    
    public function setCssRemote($css)
    {
        if (is_string($css)) {
            $css = array($css);
        }
        
        if (is_array($css) && count($css)) {
            foreach ($css as $item) {
                $item = filter_var($item, FILTER_SANITIZE_URL);
                $this->_css[] = $item;
            }
        } else {
            throw new Exception('CSS error: setCssRemote() error');
        }
    }
    
    public function setCssDinamic($css)
    {
    	$base_url = defined("APP_URL") ? APP_URL : BASE_URL;
    	
    	if (is_string($css)) {
    		$css = array($css);
    	}
    	
    	if ( is_array($css) && count($css) ) {
            foreach ( $css as $item ) {
                $this->_cssPlugin[] = $base_url . $item;
            }
        } else {
            throw new Exception('CSS error: Dynamic error');
        }
    }
    
    /**
     * Establecer hojas de estilo
     * 
     * @param array $css lista de hojas de estilo
     * 
     * @throws Exception
     * 
     * @return null
     */
    public function setCssPlugin($css)
    {
    	if (is_string($css)) {
    		$css = array($css);
    	}
    	
    	$base_url = defined("APP_URL") ? APP_URL : BASE_URL;
    	
        if ( is_array($css) && count($css) ) {
            foreach ( $css as $item ) {
                $this->_cssPlugin[] = $base_url . 'public/css/' .  $item . '.css';
            }
        } else {
            throw new Exception('CSS error: Plugin error');
        }
    }
    
    /**
     * Establecer hojas de estilo
     *
     * @param array $css lista de hojas de estilo
     *
     * @throws Exception
     *
     * @return null
     */
    public function setCssCorePlugin($css)
    {
    	if (is_string($css)) {
    		$css = array($css);
    	}
    	 
    	if ( is_array($css) && count($css) ) {
    		foreach ( $css as $item ) {
    			$this->_cssPlugin[] = CORE_URL . '../public/css/' .  $item . '.css';
    		}
    	} else {
    		throw new Exception('CSS error: Core error');
    	}
    }
    
    /**
     * Establecer plantilla
     * 
     * @param string $template nombre de plantilla
     * 
     * @return null
     */
    public function setTemplate( $template )
    {
        $this->_template = ( string ) $template;
    }
    
    /**
     * Procesar un widget
     * 
     * @param string $widget  nombre de widget
     * @param string $method  metodo
     * @param array  $options opciones
     * 
     * @throws Exception en caso de error de widget
     * 
     * @return algo
     */
    public function widget( $widget, $method, $options = array() )
    {
        if ( ! is_array($options) ) {
            $options = array( $options );
        }
        
        if ( is_readable(CORE_PATH . 'widgets' . DS . $widget . '.php') ) {
            include_once CORE_PATH . 'widgets' . DS . $widget . '.php';
            
            $widgetClass = $widget . 'Widget';
            
            if ( ! class_exists($widgetClass) ) {
                throw new Exception('Widget class error');
            }
            
            if ( is_callable($widgetClass, $method) ) {
                if ( count($options) ) {
                    return call_user_func_array(
                        array( new $widgetClass, $method ), 
                        $options
                    );
                } else {
                    return call_user_func(array( new $widgetClass, $method ));
                }
            }
            
            throw new Exception('Error metodo widget');
        }
        
        throw new Exception('Error de widget');
    }
    
    /**
     * Obtener posiciones de objetos
     * 
     * @throws Exception si hay error de configuracion
     * 
     * @return array de posiciones de layout
     */
    public function getLayoutPositions()
    {
//         $confName = CORE_PATH . 'mvc' . DS . 'views' . DS . 
//             'layout' . DS . $this->_template . DS . 'configs.php';
       
        $confName = $this->template_dir . 'configs.php';
		
        if ( is_readable($confName) ) {
            include_once $confName;
        } else {
            throw new Exception('Layout configuration error');
        }
        return get_layout_positions();
    }
    
    /**
     * Obtener widgets
     * 
     * @return algo
     */
    private function _getWidgets()
    {
        $widgets = array(
//             'menu-sidebar' => array(
//                 'config'   => $this->widget('menu', 'getConfig'),
//                 'content'  => array( 'menu', 'getMenu' )
//             )
        );
        
        $positions = $this->getLayoutPositions();
        $keys      = array_keys($widgets);
        $hidemenu  = true;
        foreach ( $keys as $k ) {
            /* verificar si la posicion del widget esta presente */
            if ( isset( $positions[ $widgets[ $k ][ 'config' ][ 'position' ] ] ) ) {
                /* verificar si esta deshabilitado para la vista */ 
                $show = $widgets[ $k ][ 'config' ][ 'show' ];
                $hide = $widgets[ $k ][ 'config' ][ 'hide' ];
                
                if ( ! isset( $hide ) || ! in_array($this->_item, $hide) ) {
                    /* verificar si esta habilitado para la vista */
                    if ( $show === 'all' || in_array($this->_item, $show) ) {
                        /* llenar la posicion del layout */
                        $positions[ $widgets[ $k ][ 'config' ][ 'position' ] ][] 
                            = $this->getWidgetContent($widgets[ $k ][ 'content' ]);
                        $hidemenu = false;
                    }
                }
            }
        }
        
        if ( $hidemenu == true ) {
            $position = false;
        }
        return $positions;
    }
    
    /**
     * Obtener contenido del widget
     * 
     * @param array $content contenidos
     * 
     * @throws Exception error de contenido de widget
     * 
     * @return void|mixed
     */
    public function getWidgetContent( array $content )
    {
        if ( ! isset( $content[ 0 ] ) || ! isset( $content[ 1 ] ) ) {
            throw new Exception('Error contenido widget');
            return;
        }
        
        if ( ! isset( $content[ 2 ] ) ) {
            $content[ 2 ] = array();
        }
        
        return $this->widget($content[ 0 ], $content[ 1 ], $content[ 2 ]);
    }
    
}
