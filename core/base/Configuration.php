<?php
/**
 * Clase de gestión de configuración
 *
 * PHP Version 5.4
 *
 * @category Framework.core.base
 * @package  Framework_pecari
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Configuration.php
 *
 */

/**
 * Clase de gestión de configuración
 *
 * @category Framework.core.base
 * @package  Framework_pecari
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Configuration.php
 */

class Configuration
{
    private $_items;
    private $_acl;
    
    public function __construct($acl = false)
    {
    	$this->_acl = $acl;
    }
    
    /**
     * Constructor de lista de controles de acceso
     * 
     * @param int $id opcional id de usuario, si se omite 
     * se usa el usuario en sesion de sistema
     */
    public static function init()
    {
    	$path = realpath( dirname( __FILE__ ) . DS . '..' )  . DS;

		if (file_exists($path . "config" . DS ."framework.php")) {
			include_once $path . DS . "config" . DS ."framework.php";
		} else {
			throw new Exception("Default config file not found (/core/config/framework.php");
		}
		
		$temp = $config;
		$index_path = dirname($_SERVER["SCRIPT_FILENAME"]);
		
		if (file_exists($index_path . DS . "config" . DS . "framework.php")) {
			include_once $index_path  . DS . "config" . DS ."framework.php";
		} elseif (file_exists($index_path . DS . "application" . DS . "config" . DS ."framework.php")) {
			include_once $index_path  . DS . "application" . DS . "config" . DS ."framework.php";
		}
		
		$config = array_merge($temp, $config);
		
		if (file_exists($path . DS . "helpers" . DS ."global.php")) {
			include_once $path . DS . "helpers" . DS ."global.php";
		} else {
			throw new Exception("Global helper file not found (/core/helpers/global.php");
		}
		toDefine($config);
    }
    
    /**
     * Cargar archivo de configuración
     *
     * @param string $configFile nombre del archivo de configuración, se usará como clave para
     * recuperar los valores obtenidos
     * 
     * @return null agrega el retorno de la configuración a la pila local de clase _items
     */
    public function load( $configFiles ) {
    	$acl = $this->_acl;
    	
        if (!is_array($configFiles)) {
            $configFiles = array($configFiles);
        }
        
        foreach($configFiles as $configFile) {
            if(file_exists( CORE_PATH . "config" . DS . $configFile . ".php")) {
                    include CORE_PATH . "config" . DS . $configFile . ".php";
            }

            if (isset($config)) {
                    $temp = $config;
            }

            if( file_exists( USER_PATH . "config" . DS . $configFile . ".php")) {
                    include USER_PATH . "config" . DS . $configFile . ".php";
            }

            $temp = isset($temp) ? array_merge($temp, $config) : $config;

            unset($config);

            if (defined('PILL_PATH')) {
                    if( file_exists( PILL_PATH . "config" . DS . $configFile . ".php")) {
                            include PILL_PATH . "config" . DS . $configFile . ".php";
                    }	
            }

            $config = isset($config) ? array_merge($temp, $config) : $temp;

            if (isset($config)) {
                    $this->_items[ $configFile ] = $config;
            } else {
                    throw new Exception("No configuration file found");
            }
        }
    }

    /**
     * Carga los datos de un archivo de configuración sin requerir instanciar la clase
     * 
     * @param string $configFile nombre del archivo de configuración
     * 
     * @throws Exception en caso de no encontrar el archivo o que éste no esté formateado
     * correctamente
     * 
     * @return array con los parámetros de configuración contenidos en el archivo solicitado
     */
    public static function loadStatic( $configFile ) {
    	if(file_exists( CORE_PATH . "config" . DS . $configFile . ".php")) {
    		include CORE_PATH . "config" . DS . $configFile . ".php";
    	}
    	 
    	if (isset($config)) {
    		$temp = $config;
    	}
    
    	if( file_exists( USER_PATH . "config" . DS . $configFile . ".php")) {
    		include USER_PATH . "config" . DS . $configFile . ".php";
    	}
    	 
    	$temp = isset($temp) ? array_merge($temp, $config) : $config;
    	 
    	unset($config);
    	 
    	if (defined('PILL_PATH')) {
    		if( file_exists( PILL_PATH . "config" . DS . $configFile . ".php")) {
    			include PILL_PATH . "config" . DS . $configFile . ".php";
    		}
    	}
    	
    	$config = isset($config) ? array_merge($temp, $config) : $temp;
    	
    	if (isset($config)) {
    		return $config;
    	} else {
    		throw new Exception("No configuration file found");
    	}
    }
    
    /**
     * Retorna un elemento del stack de datos de configuración local _items
     * 
     * @param string $root el namespace de configuración
     * @param string $key la clave a buscar
     * 
     * @return string con valor de la clave o null si no se encuentra
     */
    public function item( $root, $key ) {
    	if (isset($this->_items[$root][$key])) {
    		return $this->_items[$root][$key]; 
    	} else {
    		return null;
    	}
    }
    
    /**
     * Retorna un namespace de configuración completo
     * 
     * @param string $root nombre del namespace de configuración
     * 
     * @return array conteniendo el namespace completo
     */
    public function branch( $root ) {
    	if (isset($this->_items[$root])) {
    		return $this->_items[$root];
    	} else {
    		return null;
    	}
    }
    
}