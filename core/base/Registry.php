<?php
/**
 * Clase de registro
 *
 * PHP Version 5.4
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Registry.php
 *
 */

/**
 * Clase de registro
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Registry.php
 */

class Registry
{
    private static $_instancia;
    private $_data;
    
    /**
     * Constructor nulo
     * /no se puede instanciar la clase
     * 
     * @return false
     */
    private function __construct() 
    {
    }
    
    /**
     * Singleton de instancia
     * 
     * @return referencia a si mismo si existe o nueva instancia
     */
    public static function getInstancia()
    {
        if ( ! self::$_instancia instanceof self ) {
            self::$_instancia = new Registry();
        }
        
        return self::$_instancia;
    }
    
    /**
     * Guardar par campo/valor en memoria
     * 
     * @param string $name  campo
     * @param string $value valor
     * 
     * @return null
     */
    public function __set( $name, $value ) 
    {
        $this->_data[ $name ] = $value;
    }
    
    /**
     * Obtener valor de campo
     * 
     * @param string $name nombre de campo
     * 
     * @return string valor de campo
     */
    public function __get( $name ) 
    {
        $retorno = false;
        if ( isset( $this->_data[ $name ] ) ) {
            $retorno = $this->_data[ $name ];
        }
        
        return $retorno;
    }
}
