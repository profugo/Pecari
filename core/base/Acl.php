<?php
/**
 * Clase de Access Control List
 *
 * PHP Version 5.4
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Acl.php
 *
 */

/**
 * Clase de Access Control List
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Acl.php
 */

class ACL
{
    private $_registry;
    private $_db;
    private $_id;
    private $_role;
    private $_permisos;
    
    /**
     * Constructor de lista de controles de acceso
     * 
     * @param int $id opcional id de usuario, si se omite 
     * se usa el usuario en sesion de sistema
     */
    public function __construct($id = false)
    {
        $this->_id = 0;
        
        if ( $id ) {
            $this->_id = (int) $id;
        } else {
            if ( Session::get('id_usuario') ) {
                $this->_id = Session::get('id_usuario');
            }
        }
        
        $this->_registry = Registry::getInstancia();
        $this->_db       = $this->_registry->_db;
        $this->_role     = $this->getRole();
        $this->_permisos = $this->getPermisosRole();
        $this->compilarAcl();
    }
    
    /**
     * Combinar los permisos del rol con los del usuario
     * 
     * @return array permisos
     */
    public function compilarAcl()
    {
        $this->_permisos = array_merge(
            $this->_permisos,
            $this->getPermisosUsuario()
        );
    }

    /**
     * Obtener el rol asignado al usuario en sesion
     * 
     * @return int role id de rol de usuario
     */
    public function getRole()
    {
        $role = $this->_db->query(
            "select role from " . FW_PREFIX . "usuarios " .
            "where id = {$this->_id}"
        );
                
        $role = $role->fetch();
        return is_array($role) ? $role['role'] : $role;
    }
    
    /**
     * Obtener los permisos de un rol específico identificado por id
     * 
     * @return array permisos de rol
     */
    public function getPermisosRoleId()
    {
        $ids = $this->_db->query(
            "select permiso from " . FW_PREFIX . "permisos_role " .
            "where role = '{$this->_role}'"
        );  
        $ids = $ids->fetchAll(PDO::FETCH_ASSOC);
        
        $id = array();
        
        for ($i = 0; $i < count($ids); $i++) {
            $id[] = $ids[$i]['permiso'];
        }
        
        return $id;
    }
    
    /**
     * Obtener la lista de permisos asociadas a un rol
     * 
     * @return array permisos
     */
    public function getPermisosRole()
    {
        $permisos = $this->_db->query(
            "select * from " . FW_PREFIX . "permisos_role " .
            "where role = '{$this->_role}'"
        );
                
        $permisos = $permisos->fetchAll(PDO::FETCH_ASSOC);
        
        $data = array();
        
        for ($i = 0; $i < count($permisos); $i++) {
            $key = $this->getPermisoKey($permisos[$i]['permiso']);
            if ($key == '') {
                continue;
            }
            
            if ($permisos[$i]['valor'] == 1) {
                $v = true;
            } else {
                $v = false;
            }
            
            $data[$key] = array(
                'key' => $key,
                'permiso' => $this->getPermisoNombre($permisos[$i]['permiso']),
                'valor' => $v,
                'heredado' => true,
                'id' => $permisos[$i]['permiso']
            );
        }
        
        return $data;
    }
    
    /**
     * Obtener la clave de permiso partiendo del id
     * 
     * @param int $permisoID id de permiso
     * 
     * @return string clave de permiso
     */
    public function getPermisoKey($permisoID)
    {
        $permisoID = (int) $permisoID;
        
        $key = $this->_db->query(
            "select `key` from " . FW_PREFIX . "permisos " .
            "where id_permiso = {$permisoID}"
        );
                
        $key = $key->fetch();
        return $key['key'];
    }
    
    /**
     * Obtener el nombre de un permiso partiendo del id
     * 
     * @param int $permisoID id de permiso
     * 
     * @return string Nombre de permiso
     */
    public function getPermisoNombre($permisoID)
    {
        $permisoID = (int) $permisoID;
        
        $key = $this->_db->query(
            "select `permiso` from " . FW_PREFIX . "permisos " .
            "where id_permiso = {$permisoID}"
        );
                
        $key = $key->fetch();
        return $key['permiso'];
    }
    
    /**
     * Obtener todos los permisos (propios y heredados del rol)
     * de un usuario pasado por post
     * 
     * @return array permisos
     */
    public function getPermisosUsuario()
    {
        $ids = $this->getPermisosRoleId();
        
        if (count($ids)) {
            $permisos = $this->_db->query(
                "select * from " . FW_PREFIX . "permisos_usuario " .
                "where usuario = {$this->_id} " .
                "and permiso in (" . implode(",", $ids) . ")"
            );

            $permisos = $permisos->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $permisos = array();
        }
        
        $data = array();
        
        for ($i = 0; $i < count($permisos); $i++) {
            $key = $this->getPermisoKey($permisos[$i]['permiso']);
            if ($key == '') {
                continue;
            }
            
            if ($permisos[$i]['valor'] == 1) {
                $v = true;
            } else {
                $v = false;
            }
            
            $data[$key] = array(
                'key' => $key,
                'permiso' => $this->getPermisoNombre($permisos[$i]['permiso']),
                'valor' => $v,
                'heredado' => false,
                'id' => $permisos[$i]['permiso']
            );
        }
        
        return $data;
    }
    
    /**
     * Obtener el array de permisos del usuario en sesion
     * 
     * @return array de permisos
     */
    public function getPermisos()
    {
        if (isset($this->_permisos) && count($this->_permisos)) {
            return $this->_permisos;
        }
    }
    
    /**
     * Valida que se tenga el permiso indicado
     * 
     * @param string $key clave de permiso
     * 
     * @return bool true si tiene permiso
     */
    public function permiso($key)
    {
        if (array_key_exists($key, $this->_permisos)) {
            $val = $this->_permisos[$key]['valor']; 
            if ( $val == true || $val == 1 ) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Verifica que se tengan permisos y que no haya caducada la sesión
     * 
     * @param string $key permiso requerido
     * 
     * @return null si ok o redirect si no tiene permiso
     */
    public function acceso($key)
    {   
        if ($this->permiso($key)) {
            Session::tiempo();
            return;
        }
        
        header("location:" . BASE_URL . "error/access/5050");
        exit;
    }
}
