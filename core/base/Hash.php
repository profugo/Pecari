<?php
/**
 * Clase de cifrado
 *
 * PHP Version 5.4
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Hash.php
 *
 */

/**
 * Clase de cifrado
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Hash.php
 */

class Hash
{
    /**
     * Obtener el hash de una cadena dada con un algoritmo y semilla indicados
     * 
     * @param string $algoritmo Algoritmo a usar para hacer el hash
     * @param string $data      cadena a hashear
     * @param string $key       Semilla para el hash
     * 
     * @return string Hash de la cadena
     */
    public static function getHash($algoritmo, $data, $key)
    {
        $hash = hash_init($algoritmo, HASH_HMAC, $key);
        hash_update($hash, $data);
        
        return hash_final($hash);
    }
}