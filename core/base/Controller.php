<?php
/**
 * Abstracción de clase base de Controladores
 *
 * PHP Version 5.4
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Controller.php
 *
 */

/**
 * Abstracción de clase base de Controladores
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Controller.php
 */

abstract class Controller
{
    private   $_registry; 
    private   $_properties = array();
    protected $_view;
    protected $_acl;
    protected $_request;
    protected $_conf;
    protected $_modules;
    
    /**
     * Constructor de controladores
     * Crea una instancia de registro, y una de la clase vista
     * 
     * @return null
     */
    public function __construct() 
    {
        $this->_registry = Registry::getInstancia();
        $this->_acl      = $this->_registry->_acl;
        $this->_request  = $this->_registry->_request;
        $this->_conf     = $this->_registry->_config;
        $this->_modules  = $this->_registry->_modules;
        $this->_view     = new View($this->_request, $this->_acl);
       // $this->_properties = array();
    }
    
    /**
     * Prototipo de metodo index (requerido en todos los hijos de esta clase)
     * 
     * @return null
     */
    abstract public function index();
    
    public function __get($name)
    {
        if (isset($this->_properties[$name])) {
            return $this->_properties[$name];
        } else {
            $this->redireccionar("error/1003/" . $name);
        }
    }

    public function __set($name, $value) 
    {
        $this->_properties[$name] = $value;
    }

    protected function loadHelper($helper) 
    {
    	$rutaHelper = CORE_PATH . "helpers" . DS . $helper . ".php";
    	if (is_readable($rutaHelper)) {
    		include_once $rutaHelper;
    	} else {
    		throw new Exception( "Helper error");
    	}
    }
    
    /**
     * Cargar clase de modelo
     *
     * @param string $modelo nombre de clase
     * @param string $modulo opcional, nombre de módulo
     *
     * @throws Exception En caso de no encontrar modelo
     *
     * @return null
     */
    protected function loadModel( $modelo, $modulo = false )
    {
        $retorno    = false;
        $modelo     = $modelo . 'Model';
        $rutaModelo = CORE_PATH . 'mvc' . DS . 'models' . DS . $modelo . '.php';
        
        if ( ! $modulo ) {
            $modulo = $this->_request->getModulo();
        }
        
        if ( $modulo ) {
            if ( $modulo != 'default' ) {
                $rutaModelo = CORE_PATH . 'modules' . DS . $modulo . 
                    DS . 'models' . DS . $modelo . '.php';
                $rutaModel2 = USER_PATH . 'modules' . DS . $modulo . 
                    DS . 'models' . DS . $modelo . '.php';
                if ( is_readable($rutaModel2) ) {
                    $rutaModelo = $rutaModel2;
                }
            } 
        }
        
        if ( is_readable($rutaModelo) ) {
            include_once $rutaModelo;
            $modelo = new $modelo;
            $retorno = $modelo;
        } else {
            throw new Exception('Model error');
        }
        
        return $retorno;
    }
    
    /**
     * Incluír librería del framework
     * 
     * @param string $libreria nombre de librería
     * 
     * @throws Exception en caseo de no encontrar archivo
     * 
     * @return null
     */
    protected function getLibrary( $libreria )
    {
    	$rutaLocal    = USER_PATH . 'libs' . DS . $libreria . '.php';
    	$rutaLibreria = CORE_PATH . 'libs' . DS . $libreria . '.php';
    	
    	if ( is_readable($rutaLocal) ) {
    		include_once $rutaLocal;
    	} elseif ( is_readable($rutaLibreria) ) {
            include_once $rutaLibreria;
        } else {
            throw new Exception('Library error');
        }
    }
    
    /**
     * Obtener cadena filtrada por metodo post
     * 
     * @param string $clave clave de post
     * 
     * @return string cadena filtrada o cadena nula
     */
    protected function getTexto( $clave )
    {
        $retorno = '';
        if ( isset( $_POST[ $clave ] ) ) {
            if ( ! empty( $_POST[ $clave ] ) ) {
                $_POST[ $clave ] = htmlspecialchars($_POST[ $clave ], ENT_QUOTES);
                $retorno = $_POST[ $clave ];
            }
        }
            
        return $retorno;
    }
    
    /**
     * Obtener int por metodo post
     * 
     * @param string $clave clave post
     * 
     * @return int valor o 0 si la cadena post no contiene un entero
     */
    protected function getInt( $clave )
    {        
        return isset($_POST[$clave]) ? $this->filtrarInt($_POST[$clave]) : false;
    }
    
    /**
     * Obtener real por metodo post
     *
     * @param string $clave clave post
     *
     * @return real valor o 0 si la cadena post no contiene un entero
     */
    protected function getReal( $clave )
    {
        return isset($_POST[$clave]) ? $this->filtrarReal($_POST[$clave]) : false;
    }
    
    /**
     * Redirigir el flujo de navegación a subruta específica o
     * a dirección base 
     * 
     * @param string $ruta opcional, ruta de destino
     * 
     * @return redirect
     */
    protected function redireccionar( $ruta = false )
    {
        if ( $ruta ) {
            header('location:' . BASE_URL . $ruta);
        } else {
            header('location:' . BASE_URL);
        }
        exit;
    }

    static function staticRedireccionar( $ruta = false )
    {
        if ( $ruta ) {
            header('location:' . BASE_URL . $ruta);
        } else {
            header('location:' . BASE_URL);
        }
        exit;
    }
    
    /**
     * Filtrar variable, asegurar int
     * 
     * @param string $int cadena a filtrar
     * 
     * @return int numero o 0 si la cadena no es un numero
     */
    protected function filtrarInt( $int )
    {
        $int = (int) $int;

        $ret = ( is_int($int) ? $int : 0);
        
        return $ret;
    }
    
    /**
     * Filtrar variable, asegurar real
     *
     * @param string $int cadena a filtrar
     *
     * @return int numero o 0 si la cadena no es un numero
     */
    protected function filtrarReal( $real )
    {
    	$real = (float) $real;
    
    	$ret  = (is_real($real) ? $real : 0);
    
    	return $ret;
    }
    
    /**
     * Filtrar variable, asegurar SQL
     *
     * @param string $texto cadena a filtrar
     *
     * @return string filtrada o false si hay error
     */
    protected function filtrarSQL($texto)
    {
    	$retorno = false;
    	$texto = strip_tags($texto);
        $db = Configuration::loadStatic("database");

    	if ( ! get_magic_quotes_gpc() ) {
            $texto = mysqli_real_escape_string(
                mysqli_connect(
                    $db["db_host"],
                    $db["db_user"],
                    $db["db_pass"]
                ),
                $texto
            );
    	}

    	$retorno = trim($texto);
    	
    	return $retorno;
    }
    /**
     * Obtener parámetro post sin filtrar
     * 
     * @param string $clave clave post
     * 
     * @return string cadena
     */
    protected function getPostParam( $clave )
    {
        if ( isset( $_POST[ $clave ] ) ) {
            return $_POST[ $clave ];
        }
    }
    
    /**
     * Obtener cadena de post y filtrarla para ejecución sql
     * 
     * @param string $clave clave post
     * @param bool $nostrip true si no debe aplicar un strip_tags a la cadena
     * 
     * @return Ambigous <boolean, string>
     */
    protected function getSql( $clave, $nostrip = false )
    {
    	
        $retorno = false;
        if ( isset( $_POST[ $clave ] ) ) {
            if ( ! empty( $_POST[ $clave ] ) ) {
                $_POST[ $clave ] = $nostrip == false ? strip_tags($_POST[ $clave ]) : $_POST[ $clave ];
                $this->_conf->load("database");
                                
                // if ( ! get_magic_quotes_gpc() ) {
                    $_POST[ $clave ] = mysqli_real_escape_string(
                        mysqli_connect(
                        		$this->_conf->item("database", "db_host"),
                        		$this->_conf->item("database", "db_user"),
                        		$this->_conf->item("database", "db_pass")
                        ),
                    	$_POST[ $clave ]
                    );
                // }

                $retorno = trim($_POST[ $clave ]);
            }
        }
        return $retorno;
    }
    
    /**
     * Filtrar parametro POST alfanumérico
     * 
     * @param string $clave clave post
     * 
     * @return false si no es alfanum o cadena filtrada
     */
    protected function getAlphaNum( $clave )
    {
        $retorno = false;
        if ( isset( $_POST[ $clave ] ) ) {
            if ( ! empty( $_POST[ $clave ] ) ) {
                $_POST[ $clave ] = ( string ) preg_replace(
                    '/[^A-Z0-9_]/i', 
                    '', 
                    $_POST[ $clave ] 
                );
                $retorno = trim($_POST[ $clave ]);
            }
        }
        return $retorno;
    }
    
    /**
     * Validar una direccion de correo electrónico
     * 
     * @param string $email correo a verificar
     * 
     * @return boolean true si es formato valido
     */
    public function validarEmail( $email )
    {
        $retorno = true;
        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL) ) {
            $retorno = false;
        }
        
        return $retorno;
    }
    
    /**
     * Formatear cadena post por seguridad
     * 
     * @param string $clave clave post
     * 
     * @return Ambigous <boolean, string> false si error o cadena formateada
     */
    protected function formatPermiso( $clave )
    {
        $retorno = false;
        if ( isset( $_POST[ $clave ] ) ) {
            if ( ! empty( $_POST[ $clave ] ) ) {
                $_POST[ $clave ] = ( string ) preg_replace(
                    '/[^A-Z_]/i', 
                    '', 
                    $_POST[ $clave ]
                );
                $retorno = trim($_POST[ $clave ]);
            }
        }
        return $retorno;
    }
    
    /**
     * Permite la carga de una plantilla css generada al vuelo
     *
     * @return css: Retorna una hoja de estilos con content-type text/css
     */
    public function dynCss($template = "style", $location = "F")
    {
    	$this->_conf->load("background");
    	$background = $this->_conf->item("background", "default_background");
    	if (is_array($background)) {
    		$tile = FW_URL . "public/img/tiles/" . $background[0] . "/" . $background[0] . $background[1] . ".jpg";
    	} else {
    		$tile = $background != '' ? BASE_URL . "public/img/" . $background : false;
    	}
    
    	$this->_view->assign("style", $this->_conf->item("background", "style"));
    	if ($tile) { 
    		$this->_view->assign("tile", $tile);
    	}
    	$this->_view->renderizar($template, false, true, $location);
    }
}
