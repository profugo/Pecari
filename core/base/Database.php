<?php
/**
 * Clase de conexión a datos que extiene la clase PDO
 *
 * PHP Version 5.4
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Database.php
 *
 */

/**
 * Clase de conexión a datos que extiene la clase PDO
 *
 * @category Framework.core.base
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/base/Database.php
 */

class Database extends PDO
{
	
	private $_conf;
	
    /**
     * Constructor de clase
     * 
     * @param string $host   host de base de datos
     * @param string $dbname Nombre de base de datos
     * @param string $user   Nombre de usuario para conexión
     * @param string $pass   Clave de conexión
     * @param string $char   Charset para la conexión (recom. utf-8)
     * 
     * @return null
     */
    public function __construct() 
    {
        $this->_conf = new Configuration();

        $this->_conf->load("database");
                $this->_conf->load("database_updates");
        try {	
            parent::__construct(
                'mysql:host=' . $this->_conf->item("database", "db_host") .
                ';dbname=' . $this->_conf->item("database", "db_name"),
                $this->_conf->item("database", "db_user"), 
                $this->_conf->item("database", "db_pass"), 
                array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . $this->_conf->item("database", "db_char"),
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING,
                    PDO::ATTR_EMULATE_PREPARES => FALSE
                )
            );
    
            $this->_chkUpdateDb();
        }
        catch (PDOException $e) {
            $d = str_replace("/", "!", ROOT);
            echo "ERROR: No se pudo establecer conexión a la base de datos, verifique los parámetros de conexión en: <hr><code>" . 
                ROOT . "application/config/database.php</code><hr>O utilice el asistente de configuración"
                    . " <a href='" . FW_URL . "core/helpers/confdb/index.php?d={$d}'>Configurar</a>";

            exit;
        } 
    }
        
    private function _chkUpdateDb()
    {
    	// Primero verifica que exista la tabla genérica de configuración, puede tratarse de un caso
    	// en el que no se use esta tabla
    	$data = $this->query("SHOW TABLES")->fetchAll(PDO::FETCH_NUM);
    	$tablas = array();
    	foreach($data as $val) {
            $tablas[] = $val[0];
    	}
    	if (! in_array(FW_PREFIX . "gen_conf", $tablas)) {
            return;
    	}
    	
    	$data = $this->query("SELECT valor FROM " . FW_PREFIX . "gen_conf WHERE campo='version'")->fetch(PDO::FETCH_ASSOC);
    	// la versión 0.8.9 es la última sin mecanismo de arreglos autónomos en la base de datos
    	if (! isset($data["valor"])) {
            $this->query("INSERT INTO " . FW_PREFIX . "gen_conf (campo, valor) VALUES ('version', '0.8.9')");
            $ver = "0.8.9";
    	} else {
            $ver = $data["valor"];
    	}
    	
    	if (VERSION != $ver) {
            $updates = $this->_conf->item("database_updates", "updates");
            foreach( $updates as $update) {
                if ($ver < $update["version"]) {
                    foreach($update["sql"] as $sql) {
                        $this->query($sql);
                    }
                }

            }
            $this->query("UPDATE " . FW_PREFIX . "gen_conf SET valor='" . VERSION . "' WHERE campo='version'");
    	}
    }
}
