<?php 
/**
 * Controlador de base para el modulo de editor de menús
 *
 * PHP Version 7.2
 *
 * @category Framework.core.mvc
 * @package  Framework_basico
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/mvc/controllers/menueditorController.php
 *
 */

include_once 'masterTemplateController.php';

class menueditorController extends masterTemplateController
{
    /**
     * Constructor predeterminado
     * 
     * @return null
     */
    public function __construct($modelo = false) 
    {
    	parent::__construct($modelo);
        $this->_acl->acceso('admin_access');
        $this->_view->assign('titulo', 'Editor de menús');
        $this->_view->setCss('menueditor/index/dynCss/noresize', 'D');
    }
}
