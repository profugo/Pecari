<?php
/**
 * Controlador de base para el modulo de usuarios
 *
 * PHP Version 5.4
 *
 * @category Framework.core.mvc
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/mvc/controllers/usuariosController.php
 *
 */

/**
 * Controlador de base para el modulo de usuarios
 *
 * @category Framework.core.mvc
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/mvc/controllers/usuariosController.php
 */

class UsuariosController extends Controller
{
    /**
     * Constructor predeterminado
     * 
     * @return null
     */
    public function __construct() 
    {
        parent::__construct();
    }
    
    /**
     * (non-PHPdoc)
     * 
     * @see Controller::index()
     * 
     * @return null
     */
    public function index()
    {
    }
}
