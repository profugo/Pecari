<?php
/**
 * Controlador de Access Control Lists
 *
 * PHP Version 5.4
 *
 * @category Framework.core.mvc
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/mvc/controllers/aclController.php
 *
 */

/**
 * Controlador de Access Control Lists
 *
 * @category Framework.core.mvc
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/mvc/controllers/aclController.php
 */

class AclController extends Controller
{
    private $_aclm;
    
    /**
     * Controlador predeterminado
     * 
     * @return null
     */
    public function __construct() 
    {
        parent::__construct();
        $this->_aclm = $this->loadModel('acl');
        $this->_view->setCss('usuarios/login/dynCss/noresize', 'D');
    }
    
    /**
     * Indice de controlador acl
     * 
     * @see Controller::index()
     * 
     * @return html
     */
    public function index()
    {
        $this->_acl->acceso('admin_access');
        
        $this->_view->assign('titulo', 'Listas de acceso');
        $this->_view->renderizar('index', 'acl');
    }
    
    /**
     * Obtener vista de roles
     * 
     * @return html
     */
    public function roles()
    {
        $this->_acl->acceso('admin_access');
        
        $this->_view->assign('titulo', 'Administracion de roles');
        $this->_view->assign('roles', $this->_aclm->getRoles());
        $this->_view->renderizar('roles', 'acl');
    }
    
    /**
     * Editar permisos de un rol
     * 
     * @param int $roleID id de rol
     * 
     * @return html
     */
    public function permisos_role($roleID)
    {
        $this->_acl->acceso('admin_access');
        
        $id = $this->filtrarInt($roleID);
        
        if (!$id) {
            $this->redireccionar('acl/roles');
        }
        
        $row = $this->_aclm->getRole($id);
        
        if (!$row) {
            $this->redireccionar('acl/roles');
        }
        
        $this->_view->assign('titulo', 'Administracion de permisos role');
        
        if ($this->getInt('guardar') == 1) {
            $values = array_keys($_POST);
            $replace = array();
            $eliminar = array();
            
            for ($i = 0; $i < count($values); $i++) {
                if (substr($values[$i], 0, 5) == 'perm_') {
                    $permiso = (strlen($values[$i]) - 5);
                    
                    if ($_POST[$values[$i]] == 'x') {
                        $eliminar[] = array(
                            'role' => $id,
                            'permiso' => substr($values[$i], -$permiso)
                        );
                    } else {
                        if ($_POST[$values[$i]] == 1) {
                            $v = 1;
                        } else {
                            $v = 0;
                        }
                        
                        $replace[] = array(
                            'role' => $id,
                            'permiso' => substr($values[$i], -$permiso),
                            'valor' => $v
                        );
                    }
                }
            }
            
            for ($i = 0; $i < count($eliminar); $i++) {
                $this->_aclm->eliminarPermisoRole(
                    $eliminar[$i]['role'],
                    $eliminar[$i]['permiso']
                );
            }
            
            for ($i = 0; $i < count($replace); $i++) {
                $this->_aclm->editarPermisoRole(
                    $replace[$i]['role'],
                    $replace[$i]['permiso'],
                    $replace[$i]['valor']
                );
            }
            
            $this->redireccionar('acl/roles');
            return;
        }
        
        $this->_view->assign('role', $row);
        $this->_view->assign('permisos', $this->_aclm->getPermisosRole($id));
        $this->_view->renderizar('permisos_role');
    }
    
    
    /**
     * Eliminar un role existente
     * 
     * @params $roleId id de role
     * 
     * @return null
     */
    function eliminar_role( $roleId, $confirm = false )
    {
    	if ($confirm == false) {
    		
    		$this->_view->assign("titulo", "Eliminar role");
    		$this->_view->assign("role", $this->_aclm->getRole($roleId));
    		$this->_view->renderizar('del_role_conf');
    		return;
    	}
    	
    	if ($confirm == 1) {
    		$this->_aclm->eliminarRole($roleId);
    	}
    	
    	$this->redireccionar('acl/roles');
    }
  
    /**
     * Agregar rol
     * 
     * @return html
     */
    public function nuevo_role()
    {
        $this->_acl->acceso('admin_access');
        
        $this->_view->assign('titulo', 'Nuevo Role');
        
        if ($this->getInt('guardar') == 1) {
            $this->_view->assign('datos', $_POST);
            
            if (!$this->getSql('role')) {
                $this->_view->assign('_error', 'Debe introducir el nombre del role');
                $this->_view->renderizar('nuevo_role', 'acl');
                exit;
            }
            
            $this->_aclm->insertarRole($this->getSql('role'));
            $this->redireccionar('acl/roles');
        }
        
        $this->_view->renderizar('nuevo_role', 'acl');
    }
    
    /**
     * Editar un rol
     * 
     * @param int $id_role id de rol
     * 
     * @return html
     */
    public function editar_role( $id_role )
    {
        $this->_acl->acceso('admin_access');
    
        $this->_view->assign('titulo', 'Editar Role');
        $this->_view->assign('datos', $this->_aclm->getRole($id_role));
        if ($this->getInt('guardar') == 1) {
            $this->_view->assign('datos', $_POST);
    
            if (!$this->getSql('role')) {
                $this->_view->assign('_error', 'Debe introducir el nombre del role');
                $this->_view->renderizar('nuevo_role', 'acl');
                exit;
            }

            $this->_aclm->updateRole($id_role, $this->getSql('role'));
            $this->redireccionar('acl/roles');
        }
    
        $this->_view->renderizar('nuevo_role', 'acl');
    }
    
    /**
     * Cargar vista de permisos
     * 
     * @return html
     */
    public function permisos()
    {
        $this->_acl->acceso('admin_access');
        
        $this->_view->assign('titulo', 'Administracion de permisos');
        $this->_view->assign('permisos', $this->_aclm->getPermisos());
        $this->_view->renderizar('permisos', 'acl');
    }
    
    /**
     * Eliminar un permiso existente
     *
     * @params $permId id de permiso
     *
     * @return null
     */
    function eliminar_permiso( $permId, $confirm = false )
    {
    	if ($confirm == false) {
    
    		$this->_view->assign("titulo", "Eliminar permiso");
    		$this->_view->assign("perm", $this->_aclm->getPermisoNombre($permId));
    		$this->_view->assign("permId", $permId);
    		$this->_view->renderizar('del_perm_conf');
    		return;
    	}
    	 
    	if ($confirm == 1) {
    		$this->_aclm->eliminarPermiso($permId);
    	}
    	 
    	$this->redireccionar('acl/permisos');
    }
    
    /**
     * Agregar nuevo permiso
     * 
     * @return html
     */
    public function nuevo_permiso()
    {
        $this->_acl->acceso('admin_access');
        
        $this->_view->assign('titulo', 'Nuevo Permiso');
        
        if ($this->getInt('guardar') == 1) {
            $this->_view->assign('datos', $_POST);
            
            if (!$this->getSql('permiso')) {
                $this->_view->assign(
                    '_error', 
                    'Debe introducir el nombre del permiso'
                );
                $this->_view->renderizar('nuevo_permiso', 'acl');
                exit;
            }
            
            if (!$this->getAlphaNum('key')) {
                $this->_view->assign('_error', 'Debe introducir el key del permiso');
                $this->_view->renderizar('nuevo_permiso', 'acl');
                exit;
            }
            
            $this->_aclm->insertarPermiso(
                $this->getSql('permiso'), 
                $this->getAlphaNum('key')
            );
            
            $this->redireccionar('acl/permisos');
        }
        
        $this->_view->renderizar('nuevo_permiso', 'acl');
    }

    /**
     * Editar permiso
     * 
     * @param int $id_permiso id de permiso
     * 
     * @return html
     */
    public function editar_permiso( $id_permiso )
    {
        $this->_acl->acceso('admin_access');
    
        $this->_view->assign('titulo', 'Editar Permiso');
    
        $this->_view->assign(
            'datos', 
            $this->_aclm->getPermisoNombreKey($id_permiso)
        );
        
        if ($this->getInt('guardar') == 1) {
            $this->_view->assign('datos', $_POST);
    
            if (!$this->getSql('permiso')) {
                $this->_view->assign(
                    '_error', 
                    'Debe introducir el nombre del permiso'
                );
                $this->_view->renderizar('nuevo_permiso', 'acl');
                exit;
            }
    
            if (!$this->getAlphaNum('key')) {
                $this->_view->assign('_error', 'Debe introducir el key del permiso');
                $this->_view->renderizar('nuevo_permiso', 'acl');
                exit;
            }
    
            $this->_aclm->updatePermiso(
                $id_permiso,
                $this->getSql('permiso'),
                $this->getAlphaNum('key')
            );
    
            $this->redireccionar('acl/permisos');
        }
    
        $this->_view->renderizar('nuevo_permiso', 'acl');
    }
    
}
