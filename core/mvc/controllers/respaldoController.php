<?php 
/**
 * Controlador de base para el modulo de respaldos
 *
 * PHP Version 5.4
 *
 * @category Framework.core.mvc
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/mvc/controllers/respaldoController.php
 *
 */

/**
 * Controlador de base para el modulo de respaldos
 *
 * @category Framework.core.mvc
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/mvc/controllers/respaldoController.php
 */

include_once 'masterTemplateController.php';

class RespaldoController extends masterTemplateController
{
    /**
     * Constructor predeterminado
     * 
     * @return null
     */
    public function __construct($modelo = false) 
    {
    	parent::__construct($modelo);
        $this->_acl->acceso('admin_access');
        $this->_view->assign('titulo', 'Mantenimiento de sistema');
    }
    
}