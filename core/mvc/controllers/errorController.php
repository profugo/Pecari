<?php
/**
 * Controlador de clase de gestión de Errores
 *
 * PHP Version 5.4
 *
 * @category Framework.core.mvc
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/mvc/controllers/errorController.php
 *
 */

/**
 * Controlador de clase de gestión de Errores
 *
 * @category Framework.core.mvc
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/mvc/controllers/errorController.php
 */

class ErrorController extends Controller
{
	private $_sys_errors;
	
    /**
     * Controlador predeterminado
     * 
     * return null
     */
    public function __construct()
    {
        parent::__construct();
        $this->_conf->load("error");
        $this->_sys_errors = $this->_conf->item("error", "sys_errors");
        if (is_array($this->_conf->item("error", "errors"))) {
        	$this->_sys_errors = array_merge(
        			$this->_sys_errors, 
        			$this->_conf->item("error", "errors")
        		);
        }
    }
    
    /**
     * Indice de error
     * 
     * @see Controller::index()
     * 
     * @return html
     */
    public function index($code = false, $param = false)
    {
    	$this->access($code, $param);
    }
    
    /**
     * Cargar mensaje de error de acceso
     * 
     * @param int $codigo id de error
     * 
     * @return html
     */
    public function access($code = false, $param = false)
    {
    	$this->_view->setCss('usuarios/login/dynCss', 'D');
    	 
        $this->_view->assign('titulo', 'Error');
        
        $errcode = $code == false ? "desconocido" : $code;
        $this->_view->assign("codigo", $errcode);

        $mensaje = str_replace("{PARAM}", $param, $this->_getError($code));

        $this->_view->assign('mensaje', $mensaje);
        $this->_view->renderizar('access');
    }
    
    /**
     * Obtener mensaje de error
     * 
     * @param int $codigo id de codigo de error
     * 
     * @return string cadena de error
     */
    private function _getError($codigo = false)
    {
        if ($codigo) {
            $codigo = $this->filtrarInt($codigo);
            if (is_int($codigo) ) {
                $codigo = "e" . $codigo;
            }
        } else {
            $codigo = 'default';
        }        

        return isset($this->_sys_errors[$codigo]) ? $this->_sys_errors[$codigo] : $this->_sys_errors["default"];
    }

}