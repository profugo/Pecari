<?php
/**
 * Controlador de clase de gestión de Errores
 *
 * PHP Version 5.4
 *
 * @category Framework.core.mvc
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/mvc/controllers/errorController.php
 *
 */

/**
 * Controlador de clase de gestión de Errores
 *
 * @category Framework.core.mvc
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/mvc/controllers/errorController.php
 */

class PecariInfoController extends Controller
{
	private $_sys_errors;
	
    /**
     * Controlador predeterminado
     * 
     * return null
     */
    public function __construct()
    {
        parent::__construct();
        $this->_acl->acceso('admin_access');
        $this->_view->assign("titulo", "Pecarí Info");
        $this->_view->setCss("info");
    }
    
    private function _getCoreApps()
    {
        return array(
            array(
                "name"    => "ACL (Access Control Lists)",
                "version" => "1.0",
                "url"     => BASE_URL . "acl",
                "descr"   => "Gestión de permisos del sistema",
                "perm"    => "admin_access"
            ),
            array(
                "name"    => "error",
                "version" => "1.0",
                "url"     => BASE_URL . "error",
                "descr"   => "Gestión de mensajes de error",
                "perm"    => ""
            )
        );
    }
    
    private function _getCoreModules()
    {
        return array(
            array(
                "name"    => "modmailer (SMTP Mailing)",
                "version" => "1.0",
                "url"     => BASE_URL . "modmailer",
                "descr"   => "Subsistema de mailing en base a plantillas HTML",
                "perm"    => "admin_access"
            ),
            array(
                "name"    => "respaldo (Backup & Restore)",
                "version" => "1.1",
                "url"     => BASE_URL . "respaldo",
                "descr"   => "Módulo de respaldo y restauración de base de datos",
                "perm"    => "admin_access"
            ),
            array(
                "name"    => "usuarios",
                "version" => "1.2",
                "url"     => BASE_URL . "usuarios",
                "descr"   => "Provee funcionalidad de gestión de usuarios y manejo de inicio de sesión en el sistema",
                "perm"    => "admin_access, admin_users (ninguno para login)"
            )
        );
    }
    
    private function _getCoreLibs()
    {
        return array(
            array(
                "name"    => "PHPMailer - PHP email class",
                "version" => "5.1",
                "url"     => "https://github.com/PHPMailer/PHPMailer",
                "descr"   => "PHP email transport class"
            ),
            array(
                "name"    => "FPDF",
                "version" => "1.7",
                "url"     => "http://www.fpdf.org/",
                "descr"   => "Permite generar documentos PDF directamente desde PHP"
            )
        );
    }
    
    public function index()
    {   
        $this->_conf->load(array("usuarios", "database", "session", "background"));
        
        $fw = "framework";
        
        $fwData = array(
            array("version", VERSION, $fw),
            array("default_layout", DEFAULT_LAYOUT, $fw),
            array("default_module", DEFAULT_MODULE, $fw),
            array("default_controller", DEFAULT_CONTROLLER, $fw),
            array("core_path", CORE_PATH, $fw),
            array("core_url", CORE_URL, $fw),
            array("root", ROOT, $fw),
            array("base_url", BASE_URL, $fw),
            array("hash_key", HASH_KEY, $fw)
        );
        
        $dbData = array(
            array("db_host", $this->_conf->item("database", "db_host"), "database"),
            array("db_name", $this->_conf->item("database", "db_name"), "database"),
            array("db_user", $this->_conf->item("database", "db_user"), "database"),
            array("db_char", $this->_conf->item("database", "db_char"), "database"),
            array("fw_prefix", FW_PREFIX, $fw)
        );
        
        $no_login = defined("NO_LOGIN") ? NO_LOGIN : 'NO_LOGIN';
        $sessData = array(
            array("session_id", $this->_conf->item("session", "session_id"), "session"),
            array("session_time", SESSION_TIME, $fw),
            array("no_login", $no_login, $fw),
            array("allow_register", $this->_conf->item("usuarios", "allow_register"), "usuarios"),
            array("error_limit", $this->_conf->item("usuarios", "error_limit"), "usuarios"),
            array("lock_minutes", $this->_conf->item("usuarios", "lock_minutes"), "usuarios"),
            array("err_msg", $this->_conf->item("usuarios", "err_msg"), "usuarios")
        );
        
        $mailData = array(
            array("mail_host", $this->_conf->item("usuarios", "mail_host"), "usuarios"),
            array("mail_from", $this->_conf->item("usuarios", "mail_from"), "usuarios"),
            array("mail_name", $this->_conf->item("usuarios", "mail_name"), "usuarios"),
            array("mail_user", $this->_conf->item("usuarios", "mail_user"), "usuarios")
        );
        
        $bkgData = array(
            array("default_background", print_r($this->_conf->item("background", "default_background"), true), "background"),
            array("style", $this->_conf->item("background", "style"), "background"),
        );
        
        $elementos = array(
            array("caption" => "Parámetros generales", "datos" => $fwData),
            array("caption" => "Parámetros de MySQL", "datos" => $dbData),
            array("caption" => "Parámetros de sesión", "datos" => $sessData),
            array("caption" => "Configuración SMTP (mailing)", "datos" => $mailData),
            array("caption" => "Fondo predeterminado", "datos" => $bkgData)
        );
        
        $modulos = array(
            array("caption" => "Aplicaciones del núcleo", "datos" => $this->_getCoreApps()),
            array("caption" => "Módulos del núcleo", "datos" => $this->_getCoreModules())
        );
        
        $librerias = array(
            array("caption" => "Librerías del núcleo", "datos" => $this->_getCoreLibs())
        );
        
        $this->_view->assign("elementos", $elementos);
        $this->_view->assign("modulos", $modulos);
        $this->_view->assign("librerias", $librerias);
        $this->_view->setCss('usuarios/login/dynCss/noresize', 'D');
        $this->_view->renderizar("info");
    }
    
    public function history()
    {
        $this->_view->assign("version", VERSION);
        $this->_view->setCss('usuarios/login/dynCss/noresize', 'D');
        $this->_view->renderizar("history");
    }
    
}