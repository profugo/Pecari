<?php
/**
 * Controlador de index predeterminado del framework
 *
 * PHP Version 5.4
 *
 * @category Framework.core.mvc
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/mvc/controllers/indexController.php
 *
 */

/**
 * Controlador de index predeterminado del framework
 *
 * @category Framework.core.mvc
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/mvc/controllers/indexController.php
 */

class IndexController extends Controller
{
    /**
     * Constructor predeterminado
     * 
     * @return null
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Controlador predeterminado del framework
     * 
     * @see Controller::index()
     * 
     * @return html
     */
    public function index()
    {
        $this->_view->assign('titulo', 'Portada');
        $this->_view->assign('hidemenu', '1');
        //$this->_view->assign('widget', $this->_view->widget('menu', 'getMenu'));
        $this->_view->renderizar('index', 'inicio');
    }
}