<?php
/**
 * Modelo para la clase de Access Control Lists
 *
 * PHP Version 5.4
 *
 * @category Framework.core.mvc
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/mvc/models/aclModel.php
 *
 */

/**
 * Modelo para la clase de Access Control Lists
 *
 * @category Framework.core.mvc
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /core/mvc/models/aclModel.php
 */

class AclModel extends Model
{
    
    /**
     * Constructor predeterminado
     * 
     * @return null
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Eliminar un rol
     * 
     * @param int $roleID id de role
     * 
     * @return null
     */
    public function eliminarRole( $roleID ) 
    {
    	$this->_db->query("DELETE FROM " . FW_PREFIX . "permisos_role WHERE role='{$roleID}'");
    	$this->_db->query("DELETE FROM " . FW_PREFIX . "roles WHERE id_role='{$roleID}'");
    } 
    
    /**
     * Obtener datos de rol
     * 
     * @param int $roleID id de rol
     * 
     * @return array(datos de tabla roles)
     */
    public function getRole($roleID)
    {
        $roleID = (int) $roleID;
        
        $role = $this->_db->query("SELECT * FROM " . FW_PREFIX . "roles WHERE id_role = {$roleID}");
        return $role->fetch();
    }
    
    /**
     * Obtener roles
     * 
     * @return array(*) de tabla roles
     */
    public function getRoles()
    {
        $roles = $this->_db->query("SELECT * FROM " . FW_PREFIX . "roles");
        
        return $roles->fetchAll(PDO::FETCH_ASSOC);
    }
    
    /**
     * Obtener los permisos del rol
     * 
     * @param int $roleID id de rol
     * 
     * @return array
     */
    public function getPermisosRole($roleID)
    {
        $data = array();
        
        $permisos = $this->_db->query(
            "SELECT * FROM " . FW_PREFIX . "permisos_role WHERE role = {$roleID}"
        );
                
        $permisos = $permisos->fetchAll(PDO::FETCH_ASSOC);
        
        for ($i = 0; $i < count($permisos); $i++) {
            $key = $this->getPermisoKey($permisos[$i]['permiso']);
            
            if ($key == '') {
                continue;
            }
            if ($permisos[$i]['valor'] == 1) {
                $v = true;
            } else {
                $v = false;
            }
            
            $data[$key] = array(
                'key' => $key,
                'valor' => $v,
                'nombre' => $this->getPermisoNombre($permisos[$i]['permiso']),
                'id' => $permisos[$i]['permiso']
            );
        }
        
        $todos = $this->getPermisosAll();
        $data = array_merge($todos, $data);
        
        return $data;
    }
    
    /**
     * Obtener nombre y clave de permiso
     * 
     * @param int $permisoID id de permiso
     * 
     * @return array(permiso, key) de tabla permisos
     */
    public function getPermisoNombreKey($permisoID)
    {
        $permisoID = (int) $permisoID;
    
        $key = $this->_db->query(
            "SELECT permiso,`key` FROM " . FW_PREFIX . "permisos WHERE id_permiso = $permisoID"
        );
    
        $key = $key->fetchAll(PDO::FETCH_ASSOC);
        return $key[ 0 ];
    }
    
    /**
     * Obtener clave de permiso
     * 
     * @param int $permisoID id de permiso
     * 
     * @return string clave de permiso
     */
    public function getPermisoKey($permisoID)
    {
        $permisoID = (int) $permisoID;
        
        $key = $this->_db->query(
            "SELECT `key` FROM " . FW_PREFIX . "permisos WHERE id_permiso = $permisoID"
        );
        
        $key = $key->fetch();
        return $key['key'];
    }
    
    /**
     * Eliminar un permiso
     * 
     * @param int $permisoID id de permiso
     * 
     * @return null
     */
    public function eliminarPermiso( $permisoID )
    {
    	$this->_db->query("DELETE FROM " . FW_PREFIX . "permisos_usuario WHERE permiso='{$permisoID}'");
    	$this->_db->query("DELETE FROM " . FW_PREFIX . "permisos_role WHERE permiso='{$permisoID}'");
    	$this->_db->query("DELETE FROM " . FW_PREFIX . "permisos WHERE id_permiso='{$permisoID}'");
    }
    
    /**
     * Obtener el nombre de un permiso
     * 
     * @param int $permisoID id de permiso
     * 
     * @return string nombre de permiso
     */
    public function getPermisoNombre($permisoID)
    {
        $permisoID = (int) $permisoID;
        
        $key = $this->_db->query(
            "SELECT permiso FROM " . FW_PREFIX . "permisos WHERE id_permiso = $permisoID"
        );
        
        $key = $key->fetch();
        return $key['permiso'];
    }
    
    /**
     * Obtener los permisos de la tabla permisos
     * 
     * @return array(key, valor, nombre, id) de permisos 
     */
    public function getPermisosAll()
    {
        $permisos = $this->_db->query(
            "SELECT * FROM " . FW_PREFIX . "permisos"
        );
                
        $permisos = $permisos->fetchAll(PDO::FETCH_ASSOC);
        
        for ($i = 0; $i < count($permisos); $i++) {
            $data[$permisos[$i]['key']] = array(
                'key' => $permisos[$i]['key'],
                'valor' => 'x',
                'nombre' => $permisos[$i]['permiso'],
                'id' => $permisos[$i]['id_permiso']
            );
        }
        
        return $data;
    }
    
    /**
     * Agregar un nuevo rol
     *  
     * @param string $role nombre de rol
     * 
     * @return null
     */
    public function insertarRole($role)
    {
        $this->_db->query("INSERT INTO " . FW_PREFIX . "roles VALUES(null, '{$role}')");
    }
    
    /**
     * Actualizar nombre de rol
     * 
     * @param int    $roleID id de rol
     * @param string $role   nuevo nombre de rol
     * 
     * @return null
     */
    public function updateRole($roleID, $role)
    {
        $this->_db->query("UPDATE " . FW_PREFIX . "roles set role='{$role}' WHERE id_role={$roleID}");
    }
    
    /**
     * Obtener todos los permisos
     * 
     * @return array(*) de tabla permisos
     */
    public function getPermisos()
    {
        $permisos = $this->_db->query("SELECT * FROM " . FW_PREFIX . "permisos");
        
        return $permisos->fetchAll(PDO::FETCH_ASSOC);
    }
    
    /**
     * Eliminar enlace permiso-rol
     * 
     * @param int $roleID    id de rol
     * @param int $permisoID id de permiso
     * 
     * @return null
     */
    public function eliminarPermisoRole($roleID, $permisoID)
    {
        $this->_db->query(
            "DELETE FROM " . FW_PREFIX . "permisos_role " . 
            "WHERE permiso = {$permisoID} " .
            "AND role = {$roleID}"
        );
    }

    /**
     * Editar tabla de enlace permiso-role
     * 
     * @param int $roleID    id de rol
     * @param int $permisoID id de permiso
     * @param int $valor     valor del enlace (0,1,2)
     * 
     * @return null
     */
    public function editarPermisoRole($roleID, $permisoID, $valor)
    {
        $this->_db->query(
            "replace into " . FW_PREFIX . "permisos_role set role = {$roleID}, " .
            "permiso = {$permisoID}, valor = '{$valor}'"
        );
    }

    /**
     * Insertar un nuevo permiso
     * 
     * @param string $permiso nombre de permiso
     * @param string $llave   clave de permiso
     * 
     * @return null
     */
    public function insertarPermiso($permiso, $llave)
    {
        $this->_db->query(
            "INSERT INTO " . FW_PREFIX . "permisos VALUES" .
            "(null, '{$permiso}', '{$llave}')"
        );
    }

    /**
     * Actualizar nombre/clave de permiso
     * 
     * @param int    $permisoID id de permiso
     * @param string $permiso   nombre de permiso
     * @param string $llave     clave de permiso
     * 
     * @return null
     */
    public function updatePermiso($permisoID, $permiso, $llave)
    {
        $this->_db->query(
            "UPDATE " . FW_PREFIX . "permisos SET " .
            "permiso='{$permiso}', " .
            "`key`='{$llave}' " .
            " WHERE id_permiso={$permisoID}"
        );
    }
}
