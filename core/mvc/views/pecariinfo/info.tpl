<h2 class="maintitle">Pecarí Info<sub>Configuración y módulos instalados en el sistema</sub></h2>
<div class='history'>
    <a href='{$_layoutParams.root}pecariInfo/history'>Historial de cambios</a>
</div>

<div class="panel card">
    <div class="card-body">
        Los parámetros que se defienen en <code>framework</code> son accesibles globalmente como constantes con el mismo nombre en mayúsculas, ej: <code>version</code> se accede con la constante <code>VERSION</code>.
        Los demás parámetros se cargan mediante el método <code>$this->_conf->load(&lt;archivo&gt;);</code> de la clase Controller y se accede a ellos usando <code>$this->_conf->item(&lt;archivo&gt;, &lt;parametro&gt;);</code>. Ver la documentación para métodos de carga estáticos desde fuera del controlador.
    </div>
</div>
<br>
{foreach from=$elementos item=elemento}
<table class="table table-bordered table-condensed table-striped table-sm">
    <caption>{$elemento.caption}</caption>
    <tr>
        <th>Param</th>
        <th>Valor</th>
        <th>Archivo</th>
    </tr>
{foreach from=$elemento.datos item=row}
    <tr>
        <td>{$row[0]}</td>
        <td>{$row[1]}</td>
        <td>{$row[2]}</td>
    </tr>
{/foreach}
</table>

<hr>
{/foreach}

<div class="panel card">
    <div class="card-body">
        Las aplicaciones y módulos listados están siempre accesibles y restringidos mediante filtros ACL, ver el manual para saber cómo usarlos.
    </div>
</div>
<br>

{foreach from=$modulos item=modulo}

<table class="table table-bordered table-condensed table-striped table-sm">
    <caption class="modulo">{$modulo.caption}</caption>
{foreach item=row from=$modulo.datos}
    <tr>
        <th colspan="2">{$row.name}</th>
    </tr>
    <tr>
        <td>Versión</td>
        <td>{$row.version}</td>
    </tr>
    <tr>
        <td>Permiso</td>
        <td>{$row.perm|default:'Sin restricciones'}</td>
    </tr>
    <tr>
        <td>URL</td>
        <td><a href="{$row.url}" target="_blank">{$row.url}</a></td>
    </tr>
    <tr>
        <td>Descripción</td>
        <td>{$row.descr}</td>
    </tr>
    <tr>
        <td colspan="2" style="background-color: #fff">&nbsp;</td>
    </tr>
{/foreach}
</table>

<hr>
{/foreach}


<div class="panel card">
    <div class="card-body">
        Las librerías se deben caragar con el método <code>$this->getLibrary(&lt;librería&gt;);</code> de la clase <code>Controller</code>, corre por cuenta de la aplicación la configuración de las librerías.
    </div>
</div>
<br>

{foreach from=$librerias item=libreria}

<table class="table table-bordered table-condensed table-striped table-sm">
    <caption class="libreria">{$libreria.caption}</caption>
{foreach item=row from=$libreria.datos}
    <tr>
        <th colspan="2">{$row.name}</th>
    </tr>
    <tr>
        <td>Versión</td>
        <td>{$row.version}</td>
    </tr>
    <tr>
        <td>Descripción</td>
        <td>{$row.descr}</td>
    </tr>
    <tr>
        <td>URL</td>
        <td><a href='{$row.url}' target="_blank">{$row.url}</a></td>
    </tr>
    <tr>
        <td colspan="2" style="background-color: #fff">&nbsp;</td>
    </tr>
{/foreach}
</table>

<hr>
{/foreach}