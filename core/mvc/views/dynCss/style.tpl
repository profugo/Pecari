@CHARSET "UTF-8";

html {
{if isset($tile)}
	background: url("{$tile}");
{/if}
{if isset($style)}
	{$style}
{/if}
}

body {
	background : none;
}

.jumbotron {
	display: none;
}

form.navbar-form {
	display: none;
}

#mainContainer {
	width: auto;
	display: inline-block;
	position: fixed;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	box-shadow: 4px 4px 8px #000;
}

h2 {
	text-align: center;
	padding-bottom: 16px;
	border-bottom: 1px solid #ccc;
	font-weight: normal;
}

hr {
	border-bottom: 1px solid #ccc;
}

table {
	margin: 8px;
}

td {
	padding: 8px;
}

.avatar-container{
	width: 100%;
	text-align: center;
}

.avatar {
	background-color: #acf;
	width: 64px;
	height: 64px;
	margin-left: auto;
	margin-right: auto;
	border-radius: 50%;
	box-shadow: -1px -1px 4px #000;
	background-repeat: no-repeat;
	background-size: contain;
	background-position: center;
}

.span11 {
	width: auto;
	margin: 0;
}

.menu_foot {
	border-top: 1px solid #bbb;
	border-bottom: 1px solid #bbb;
	padding: 8px 16px;
	margin-bottom: 16px;
}

img.errdlg {
 	height: auto; 
    width: auto; 
    max-width: 700px; 
    max-height: 188px;
}

.card-body {
    padding: 1.25rem 0;
}

.col-lg-11 {
    max-width: none;
}