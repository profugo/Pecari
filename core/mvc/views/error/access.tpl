<div class="errdlg" style=" width:100%;">
<img src="{$_layoutParams.ruta_fw}public/img/shame.jpg" style="width:100%; height:auto">
</div>

<p style="margin-top:32px">
Todo parece indicar que experimentamos un error <code>{$codigo}</code>, que viene a significar algo así como que <code>{$mensaje}</code>.
</p>
<p>
Esto no debería pasar en situaciones normales, si no estás haciendo nada raro y te sigue saliendo este mensaje de error por favor contactá al administrador y avisale de este problema para que pueda solucionarlo.
</p>
<p>&nbsp;</p>

<a href="{$_layoutParams.root}">Ir al Inicio</a> | 
<a href="javascript:history.back()">Volver a la p&aacute;gina anterior</a>