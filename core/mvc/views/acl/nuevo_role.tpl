<style type="text/css">
    table.table td { vertical-align: middle; }
    table.table td input { margin: 0; }
</style>

<h2 class="maintitle">Listas de control de acceso<sub>{$titulo|default:"Nuevo"}</sub></h2>

<form name="form1" method="post" action="">
    <input type="hidden" value="1" name="guardar">
    
    <table class="table table-bordered" style="width: 350px;">
        <tr>
            <td style="text-align: right;">Role: </td>
            <td><input type="text" name="role" value="{$datos.role|default:""}"></td>
        </tr>
    </table>
        
    <p><button type="submit" class="btn btn-primary"><li class="glyphicon glyphicon-ok icon-ok fa fa-check icon-white"> </li> Guardar</button></p>
</form>