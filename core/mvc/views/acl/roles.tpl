<h2 class="maintitle">Listas de control de acceso<sub>Administración de roles</sub></h2>

{if isset($roles) && count($roles)}
    <table class="table table-bordered table-condensed table-striped">
        <tr>
            <th>Role</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        
        {foreach item=rl from=$roles}
            <tr>
                <td>{$rl.role}</td>
                <td>
                    <a href="{$_layoutParams.root}acl/permisos_role/{$rl.id_role}">Permisos</a>
                </td>
                <td>
                    <a href="{$_layoutParams.root}acl/editar_role/{$rl.id_role}">Editar</a>
                </td>
                <td>
                    <a href="{$_layoutParams.root}acl/eliminar_role/{$rl.id_role}">Eliminar</a>
                </td>
            </tr>
        {/foreach}
    </table>
{/if}

<p><a class="btn btn-primary" href="{$_layoutParams.root}acl/nuevo_role"><i class="glyphicon glyphicon-plus-sign icon-plus-sign fa fa-plus icon-white"> </i> Agregar Role</a></p>