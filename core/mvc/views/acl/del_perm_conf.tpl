<h2 class="maintitle">Listas de control de acceso<sub>Eliminar permiso</sub></h2>

<p>
    ¿Está seguro que desea eliminar el permiso<strong>"{$perm}"</strong>? 
</p>

<p>
<a href="{$_layoutParams.root}acl/eliminar_permiso/{$permId}/1" class="btn btn-success">Aceptar</a> <a href="{$_layoutParams.root}usuarios" class="btn btn-success">Cancelar</a>
</p>