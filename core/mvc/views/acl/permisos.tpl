<h2 class="maintitle">Listas de control de acceso<sub>Administración de permisos</sub></h2>

{if isset($permisos) && count($permisos)}
<table class="table table-bordered table-condensed table-striped" style="width:500px;">
    <tr>
        <th>Permiso</th>
        <th>Llave</th>
        <th></th>
        <th></th>
    </tr>
    
    {foreach item=rl from=$permisos}
    
        <tr>
            <td>{$rl.permiso}</td>
            <td>{$rl.key}</td>
            <td>
                <a href="{$_layoutParams.root}acl/editar_permiso/{$rl.id_permiso}">Editar</a>
            </td>
            <td>
                <a href="{$_layoutParams.root}acl/eliminar_permiso/{$rl.id_permiso}">Eliminar</a>
            </td>
        </tr>
        
    {/foreach}
    
</table>
{/if}

<p><a href="{$_layoutParams.root}acl/nuevo_permiso" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign icon-plus-sign fa fa-plus icon-white"> </i> Agregar Permiso</a></p>