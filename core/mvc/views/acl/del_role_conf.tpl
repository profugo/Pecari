<h2 class="maintitle">Listas de control de acceso<sub>Eliminar role</sub></h2>

<p>
    ¿Está seguro que desea eliminar el role <strong>"{$role.role}"</strong>? 
</p>

<p>
<a href="{$_layoutParams.root}acl/eliminar_role/{$role.id_role}/1" class="btn btn-success">Aceptar</a> <a href="{$_layoutParams.root}usuarios" class="btn btn-success">Cancelar</a>
</p>