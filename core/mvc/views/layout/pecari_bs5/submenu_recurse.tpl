{foreach item=it from=$items}
{if $it.enlace == "dropdown"}
						 <li class="nav-item dropdown-submenu">
							<a data-toggle="dropdown" class="dropdown-item dropdown-toggle" href="#">
								{$it.titulo}
							</a>
							<ul class="dropdown-menu">
<!-- Start submenu //-->
{include file="submenu_recurse.tpl" items=$it.submenu}									
<!-- End submenu //-->
							</ul>
						</li>
{else}
						<li class="{$_item_style|default:'dropdown-item'}">
							<a  class="dropdown-item dropdown-standalone" href="{$it.enlace}">
								{if isset($it.imagen)}<i class="{$it.imagen}"> </i> {/if}{$it.titulo}
							</a>
						</li>
{/if}
{/foreach}
