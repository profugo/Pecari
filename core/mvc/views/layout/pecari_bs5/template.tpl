<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content = "width = device-width, initial-scale = 1.0, minimum-scale = 1.0, maximum-scale = 1.0, user-scalable = no" />
        <title>{strip_tags($titulo)|default:"Sin t&iacute;tulo"}</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="{$_layoutParams.ruta_css}bs5_base.css" type="text/css">
        
{if isset($_layoutParams.css) && count($_layoutParams.css)}
{foreach item=css from=$_layoutParams.css}
        <link href="{$css}" rel="stylesheet" type="text/css">
{/foreach}
{/if}
{if isset($_layoutParams.css_plugin) && count($_layoutParams.css_plugin)}
{foreach item=plg from=$_layoutParams.css_plugin}
        <link href="{$plg}" rel="stylesheet" type="text/css">
{/foreach}
{/if}
        
    </head>
    
    <body>
    
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="{$_layoutParams.root}">{$_layoutParams.configs.app_name}</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav01" aria-controls="nav01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="nav01">
		  <ul class="navbar-nav mr-auto">
{foreach item=it from=$_layoutParams.menu}
{if $it.enlace == "dropdown"}
				<li class="nav-item dropdown">
					<a data-toggle="dropdown" class="nav-link dropdown-toggle" href="#">
						{$it.titulo} <b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
{include file="submenu_recurse.tpl" items=$it.submenu}									
					</ul>
				</li>
{else}
                    <li{if isset($_layoutParams.item) && $_layoutParams.item == $it.id}class="active"{/if}>
                    	<a class="nav-link" href="{$it.enlace}">
{if isset($it.imagen)}                        	<i class="{$it.imagen}"> </i> {/if}{$it.titulo}
                        </a>
                	</li>
{/if}
{/foreach}
          </ul>
      	
{if Session::get('autenticado')}
				<div class="pull-right">
					<a class="btn btn-outline-primary" href="{$_layoutParams.root}usuarios/registro/perfil" title="Perfil de usuario" role="button">
						<span class="fa fa-user"></span>
					</a>
					<a class="btn btn-outline-primary" href="{$_layoutParams.root}usuarios/login/cerrar" title="Cerrar sesión" role="button">
						<span class="fa fa-power-off"></span>
					</a>
				</div>
{elseif $smarty.const.NO_LOGIN === "NO_LOGIN"}
			  <form class="form-inline my-2 my-lg-0 navbar-right" method="post" action="{$_layoutParams.root}usuarios/login">
				  <input type="hidden" value="1" name="enviar">
				<div class="form-group">
				  <input type="text" placeholder="Usuario" name="usuario" class="form-control">
				</div>
				<div class="form-group">
				  <input type="password" placeholder="Contraseña" name="pass" class="form-control">
				</div>
				<button type="submit" class="btn btn-outline-success">Ingresar</button>
{if Session::get('allow_register')}
<a href="{$_layoutParams.root}usuarios/registro" class="btn btn-info">Registro</a>
{/if}
			</form>
 {/if}			  

      </div>

    </nav>


{if isset($landing)}              
<div class="jumbotron padt32">
	<div class="container">
		<div style="float:left;{$_layoutParams.logo_style}" >
			<img src="{$_layoutParams.ruta_img}{$_layoutParams.logo_img}">
		</div>
		<h1>{$_layoutParams.configs.app_name}</h1>
		<p class="lead">{$_layoutParams.configs.app_slogan}</p>
	</div>
</div>
{else}

{/if}

        <div class="container card" id="mainContainer" style="background: #fff;">
            <div class="card-body">
{if count($widgets.sidebar) >0 }
                <div class="col-lg-3">
{if isset($widgets.sidebar)}
    {foreach from=$widgets.sidebar item=wd}
                    {$wd}
    {/foreach}
{/if}
                </div>
            
                <div class="col-lg-8">
{else}
                <div class="col-lg-11">
{/if}
                    <noscript><p>Para el correcto funcionamiento debe tener el soporte para javascript habilitado</p></noscript>
                    
{if isset($_error)}
                        <div id="_errl" class="alert alert-error">
                            <a class="close" data-dismiss="alert">x</a>
                            {$_error}
                        </div>
{/if}

{if isset($_mensaje)}
                        <div id="_msgl" class="alert alert-success">
                            <a class="close" data-dismiss="alert">x</a>
                            {$_mensaje}
                        </div>
{/if}
{include file=$_contenido}
                    </div>
                </div>
            </div>
        </div>
	 <div id="footer" class="footer" style="z-index:999">
      <div class="container">
        <p class="text-muted pull-right">Copyright &copy; 2013-{$smarty.server.REQUEST_TIME|date_format:'%Y'} <a href="http://darknoid.byethost7.com" target="_blank">Darknoid Software</a></p>
      </div>
    </div>
            
        <!-- javascript -->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.min.js"></script>
        
{if isset($_layoutParams.js_plugin) && count($_layoutParams.js_plugin)}
{foreach item=plg from=$_layoutParams.js_plugin}
        <script src="{$plg}" type="text/javascript"></script>
{/foreach}
{/if}
{if isset($_layoutParams.js) && count($_layoutParams.js)}
{foreach item=js from=$_layoutParams.js}
        <script src="{$js}" type="text/javascript"></script>
{/foreach}
{/if}

<script type="text/javascript">
    var _root_ = '{$_layoutParams.root}';
</script>
    </body>
</html>
