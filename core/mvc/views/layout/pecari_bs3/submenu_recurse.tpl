{foreach item=it from=$items}
{if $it.enlace == "dropdown"}
	 <li class="dropdown-submenu">
		<a href="#">{$it.titulo}</a>
		<ul class="dropdown-menu">
		{include file="submenu_recurse.tpl" items=$it.submenu}									
		</ul>
	</li>
{else}
	<li class="{$_item_style|default:''}"><a  href="{$it.enlace}">{if isset($it.imagen)}<i class="{$it.imagen}"> </i> {/if}{$it.titulo}</a></li>
{/if}
{/foreach}