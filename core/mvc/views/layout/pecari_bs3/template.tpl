<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content = "width = device-width, initial-scale = 1.0, minimum-scale = 1.0, maximum-scale = 1.0, user-scalable = no" />
        <title>{strip_tags($titulo)|default:"Sin t&iacute;tulo"}</title>
        <meta charset="utf-8">
        <link href="{$_layoutParams.ruta_css}bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="{$_layoutParams.ruta_css}footable-0.1.css" rel="stylesheet" type="text/css">
        <link href="{$_layoutParams.ruta_fw}public/css/submenu.css" rel="stylesheet" type="text/css">
        <link href="{$_layoutParams.ruta_fw}public/css/style.css" rel="stylesheet" type="text/css">
        
{if isset($_layoutParams.css) && count($_layoutParams.css)}
{foreach item=css from=$_layoutParams.css}
        <link href="{$css}" rel="stylesheet" type="text/css">
{/foreach}
{/if}
{if isset($_layoutParams.css_plugin) && count($_layoutParams.css_plugin)}
{foreach item=plg from=$_layoutParams.css_plugin}
        <link href="{$plg}" rel="stylesheet" type="text/css">
{/foreach}
{/if}
        
    </head>
    
    <body>
    
     <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{$_layoutParams.root}">{$_layoutParams.configs.app_name}</a>
        </div>
        <div class="collapse navbar-collapse">
          
          <ul class="nav navbar-nav">
          	{foreach item=it from=$_layoutParams.menu}
				{if $it.enlace == "dropdown"}
				<li class="dropdown">
				<a data-toggle="dropdown" class="dropdown-toggle" href="#">{$it.titulo} <b class="caret"></b></a>
					<ul class="dropdown-menu">
					{include file="submenu_recurse.tpl" items=$it.submenu}									
					</ul>
				</li>
				{else}
                    <li{if isset($_layoutParams.item) && $_layoutParams.item == $it.id}class="active"{/if}>
                    	<a  href="{$it.enlace}">{if isset($it.imagen)}
                        	<i class="{$it.imagen}"> </i> {/if}{$it.titulo}
                        </a>
                	</li>
            	{/if}
			{/foreach}
          </ul>
          
           <div class="navbar-collapse collapse">

{if Session::get('autenticado')}
				<div class="navbar-form pull-right">
					<a class="btn btn-default" href="{$_layoutParams.root}usuarios/registro/perfil" title="Perfil de usuario" role="button">
						<span class="glyphicon glyphicon-user"></span>
					</a>
					<a class="btn btn-default" href="{$_layoutParams.root}usuarios/login/cerrar" title="Cerrar sesión" role="button">
						<span class="glyphicon glyphicon-off"></span>
					</a>
				</div>
{elseif $smarty.const.NO_LOGIN === "NO_LOGIN"}
			  <form class="navbar-form navbar-right" method="post" action="{$_layoutParams.root}usuarios/login">
				  <input type="hidden" value="1" name="enviar">
				<div class="form-group">
				  <input type="text" placeholder="Usuario" name="usuario" class="form-control">
				</div>
				<div class="form-group">
				  <input type="password" placeholder="Contraseña" name="pass" class="form-control">
				</div>
				<button type="submit" class="btn btn-success">Ingresar</button>
{if Session::get('allow_register')}
<a href="{$_layoutParams.root}usuarios/registro" class="btn btn-info">Registro</a>
{/if}
			</form>
 {/if}			  
			</div>
        
        </div><!--/.nav-collapse -->
        
        
      </div>
    </div>
{if isset($landing)}              
<div class="jumbotron padt32">
	<div class="container">
		<div style="float:left;{$_layoutParams.logo_style}" >
			<img src="{$_layoutParams.ruta_img}{$_layoutParams.logo_img}">
		</div>
		<h1>{$_layoutParams.configs.app_name}</h1>
		<p class="lead">{$_layoutParams.configs.app_slogan}</p>
	</div>
</div>
{else}
<p>&nbsp;</p>
{/if}

        <div class="container well padded" id="mainContainer" style="background: #fff;">
           {if count($widgets.sidebar) >0 }
            <div class="span3">
                {if isset($widgets.sidebar)}
                    {foreach from=$widgets.sidebar item=wd}
                        {$wd}
                    {/foreach}
                {/if}
            </div>
            
            <div class="span8">
            {else}
            <div class="span11">
            {/if}
                <noscript><p>Para el correcto funcionamiento debe tener el soporte para javascript habilitado</p></noscript>
                    
                {if isset($_error)}
                    <div id="_errl" class="alert alert-error">
                        <a class="close" data-dismiss="alert">x</a>
                        {$_error}
                    </div>
                {/if}

                {if isset($_mensaje)}
                    <div id="_msgl" class="alert alert-success">
                        <a class="close" data-dismiss="alert">x</a>
                        {$_mensaje}
                    </div>
                {/if}
                {include file=$_contenido}
            </div>

        </div>
        </div>
	 <div id="footer" class="footer" style="z-index:999">
      <div class="container">
        <p class="text-muted">Copyright &copy; 2013-{$smarty.server.REQUEST_TIME|date_format:'%Y'} <a href="http://darknoid.byethost7.com" target="_blank">Darknoid Software</a></p>
      </div>
    </div>
            
        <!-- javascript -->
        <script type="text/javascript" src="{$_layoutParams.ruta_fw}public/js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="{$_layoutParams.ruta_fw}public/js/footable.min.js"></script>
        <script type="text/javascript" src="{$_layoutParams.ruta_js}bootstrap.min.js"></script>

{if isset($_layoutParams.js_plugin) && count($_layoutParams.js_plugin)}
{foreach item=plg from=$_layoutParams.js_plugin}
        <script src="{$plg}" type="text/javascript"></script>
{/foreach}
{/if}
{if isset($_layoutParams.js) && count($_layoutParams.js)}
{foreach item=js from=$_layoutParams.js}
        <script src="{$js}" type="text/javascript"></script>
{/foreach}
{/if}

<script type="text/javascript">
    var _root_ = '{$_layoutParams.root}';
</script>
    </body>
</html>
