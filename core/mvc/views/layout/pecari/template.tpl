<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content = "width = device-width, initial-scale = 1.0, minimum-scale = 1.0, maximum-scale = 1.0, user-scalable = no" />
        <title>{strip_tags($titulo)|default:"Sin t&iacute;tulo"}</title>
        <meta charset="utf-8">
        <link href="{$_layoutParams.ruta_css}bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="{$_layoutParams.ruta_css}bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
        <link href="{$_layoutParams.ruta_css}docs.css" rel="stylesheet" type="text/css">
        <link href="{$_layoutParams.ruta_css}docs_admini.css" rel="stylesheet" type="text/css">
        <link href="{$_layoutParams.ruta_css}footable-0.1.css" rel="stylesheet" type="text/css">
        <link href="{$_layoutParams.ruta_fw}public/css/style.css" rel="stylesheet" type="text/css">
        
{if isset($_layoutParams.css) && count($_layoutParams.css)}
{foreach item=css from=$_layoutParams.css}
        <link href="{$css}" rel="stylesheet" type="text/css">
{/foreach}
{/if}
{if isset($_layoutParams.css_plugin) && count($_layoutParams.css_plugin)}
{foreach item=plg from=$_layoutParams.css_plugin}
        <link href="{$plg}" rel="stylesheet" type="text/css">
{/foreach}
{/if}
        
    </head>
    
    <body>
    
        <div class="navbar navbar-fixed-top navbar-inverse">
            <div class="navbar-inner">
                <div class="container">
                    <a class="brand" href="{$_layoutParams.root}">{$_layoutParams.configs.app_name}</a>
                
                    {if isset($_layoutParams.menu)}

                            <ul class="nav">
{foreach item=it from=$_layoutParams.menu}
{if $it.enlace == "dropdown"}
									<li class="dropdown">
									<a data-toggle="dropdown" class="dropdown-toggle" href="#">{$it.titulo} <b class="caret"></b></a>
										<ul class="dropdown-menu">
{include file="submenu_recurse.tpl" items=$it.submenu}									
										</ul>
									</li>
{else}
                                    <li{if isset($_layoutParams.item) && $_layoutParams.item == $it.id} class="active"{/if}>
                                    	<a  href="{$it.enlace}">{if isset($it.imagen)}
                                    		<i class="{$it.imagen}"> </i> {/if}{$it.titulo}
                                    	</a>
                                    </li>
{/if}
{/foreach}
                            </ul>

                            {if Session::get('autenticado')}
                                <div class="navbar-form pull-right">
                                	<a href="{$_layoutParams.root}usuarios/registro/perfil" class="btn" title="Perfil de usuario"><i class="icon-user"> </i></a>
                                    <a href="{$_layoutParams.root}usuarios/login/cerrar" class="btn" title="Cerrar sesión"><i class="icon-off"> </i></a>
                                </div>
                            {else}
                                <form class="navbar-form pull-right" method="post" action="{$_layoutParams.root}usuarios/login">
                                    <input type="hidden" value="1" name="enviar">
                                    <input class="span2" name="usuario" type="text" placeholder="Usuario">
                                    <input class="span2" name="pass" type="password" placeholder="Password">
                                    <button type="submit" class="btn">Entrar</button>
                                    
{if Session::get('allow_register')}
<a href="{$_layoutParams.root}usuarios/registro" class="btn btn-info">Registro</a>
{/if}
                                    
                                </form>
                            {/if}

                    {/if}
                </div>
            </div>
        </div>
          
        <header class="jumbotron subhead" id="overview">
           <div class="container">
           <div style="float:left;{$_layoutParams.logo_style}" >
            <img src="{$_layoutParams.ruta_logo}">
            </div>
                <h1>{$_layoutParams.configs.app_name}</h1>
                <p class="lead">{$_layoutParams.configs.app_slogan}</p>
          </div>
         </header>
          
        <div class="container well" style="background: #fff;" id="mainContainer">
           {if count($widgets.sidebar) >0 }
            <div class="span3">
                {if isset($widgets.sidebar)}
                    {foreach from=$widgets.sidebar item=wd}
                        {$wd}
                    {/foreach}
                {/if}
            </div>
            
            <div class="span8">
            {else}
            <div class="span11">
            {/if}
                <noscript><p>Para el correcto funcionamiento debe tener el soporte para javascript habilitado</p></noscript>
                    
                {if isset($_error)}
                    <div id="_errl" class="alert alert-error">
                        <a class="close" data-dismiss="alert">x</a>
                        {$_error}
                    </div>
                {/if}

                {if isset($_mensaje)}
                    <div id="_msgl" class="alert alert-success">
                        <a class="close" data-dismiss="alert">x</a>
                        {$_mensaje}
                    </div>
                {/if}
                {include file=$_contenido}
            </div>

        </div>
        <p>&nbsp;</p>
        <!-- Footer -->
        <div class="navbar navbar-fixed-bottom">
            <div class="navbar-inner">
                <div class="container">
                    <div style="margin-top: 10px; text-align: center;">Copyright&copy; 2013-{$smarty.server.REQUEST_TIME|date_format:'%Y'} <a href="http://www.coodi.com.uy" target="_blank">www.coodi.com.uy</a></div>
                </div>
            </div>
        </div>
            
        <!-- javascript -->
        <script type="text/javascript" src="{$_layoutParams.ruta_fw}public/js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="{$_layoutParams.ruta_fw}public/js/footable.min.js"></script>
        <script type="text/javascript" src="{$_layoutParams.ruta_fw}public/js/bootstrap-3.0.0.min.js"></script>

{if isset($_layoutParams.js_plugin) && count($_layoutParams.js_plugin)}
{foreach item=plg from=$_layoutParams.js_plugin}
        <script src="{$plg}" type="text/javascript"></script>
{/foreach}
{/if}
{if isset($_layoutParams.js) && count($_layoutParams.js)}
{foreach item=js from=$_layoutParams.js}
        <script src="{$js}" type="text/javascript"></script>
{/foreach}
{/if}

<script type="text/javascript">
    var _root_ = '{$_layoutParams.root}';
</script>
    </body>
</html>
