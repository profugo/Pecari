<?php

$path = realpath( dirname( __FILE__ ) . DS . "..");
$configPath = $path . "/core/config/";
$filename = $configPath . "pecari_base_url.php";

if (!file_exists($filename)) {
    if (is_writeable($configPath) == false) {
        echo "ERROR: No se puede crear el archivo <code>{$filename}</code><br>Verifique permisos de directorio.";
        exit;
    }
    //$url = "<?php\n\$pecari_base_url = \"{$url}/\"\n";

    $uri = $_SERVER['REQUEST_URI'];
    if (strlen($uri) > 2)  {
        $uri = trim(str_replace("/", " ", $uri));
        $uri = explode(" ", $uri);
        $uri = $uri[0];
    }
    
    $init = strpos($path, $uri);
    $url = substr($path, $init);
    file_put_contents($filename, "<?php\n\$pecari_base_url = \"{$url}/\";\n");
} 
