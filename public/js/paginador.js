$(function () {
	$("#cmdPaginado").click(function() {
		$("#pagina").val($("#irAPagina").val());
		$("#buscador").submit();
	});
	
	$("#pageSelect").submit(function(e) {
		e.preventDefault();
		$("#pagina").val($("#irAPagina").val());
		$("#buscador").submit();
	});
});
 
function toPage(page)
{
	$("#pagina").val(page);
	$("#buscador").submit();
}