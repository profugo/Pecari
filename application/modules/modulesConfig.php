<?php
/**
 * Este archivo tiene la definición del array de modulos
 *
 * PHP Version 5.4
 *
 * @category Framework.modules
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /application/modules/modulesConfig.php
 *
 */

/**
 * Definición del menú principal
 *
 * @category Framework.modules
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /application/modules/modulesConfig.php
 */

class ModsConf
{
    private $_modulos = array( 'prueba' );
    
    /**
     * Constructor nulo
     * 
     * @return null
     */
    public function __construct()
    {
    }

    /**
     * Obtener los modulos configurados
     * 
     * @return array lista de modulos
     */
    public function getModulos()
    {
        return $this->_modulos;
    }
}
?>