<?php

class indexController extends pruebaController
{

    public function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        $this->_view->assign('titulo', 'Módulo de prueba');
        $this->_view->renderizar( 'index' );
    }
}

?>