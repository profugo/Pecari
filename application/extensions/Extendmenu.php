<?php
/**
 * Definición del menú principal
 *
 * PHP Version 5.4
 *
 * @category Framework.extensions
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /application/extensions/Extendmenu.php
 *
 */

/**
 * Definición del menú principal
 *
 * @category Framework.extensions
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /application/extensions/Extendmenu.php
 */

class ExtendMenu
{
    /**
     * Constructor nulo
     * 
     * @return null
     */
    public function __construct()
    {
    }

    /**
     * Objeto menú predeterminado
     * 
     * @param Acl $acl objeto tipo acl
     * 
     * @return array objeto tipo menu
     */
    public function getMenu( $acl = false )
    {	
        $menu = array(
                array(
                        'id'     => 'prueba',
                        'titulo' => 'Prueba',
                        'enlace' => BASE_URL . 'prueba',
                        'imagen' => 'icon-eye-open icon-white'
                )
        );
        
         
        if ( ! Session::get('autenticado') ) {
            $menu[] = array(
                    'id'     => 'registro',
                    'titulo' => 'Registro',
                    'enlace' => BASE_URL . 'usuarios/registro',
                    'imagen' => 'icon-book  icon-white'
            );
        }
         
        return $menu;
    }
}

?>