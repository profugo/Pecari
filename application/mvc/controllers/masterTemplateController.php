<?php

class masterTemplateController extends Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->_view->assign('titulo', 'Plantilla de aplicación');
	}

	public function index()
	{
	}
	
	public function validateAccess()
	{
		if( Session::get( 'autenticado' ) )
		{
				
			if( ( $this->_acl->permiso( "con_gestor" ) == false ) &&
					($this->_acl->permiso( "con_test" ) == false ) )
			{
				header("location:" . BASE_URL . "error/access/5050");
				exit;
			}
		} else {
			header("location:" . BASE_URL . "error/access/5050");
			exit;
		}
	}
	
	public function dateToLong( $fecha )
	{
		$meses = array(
				1 => 'Enero',
				'Febrero',
				'Marzo',
				'Abril',
				'Mayo',
				'Junio',
				'Julio',
				'Agosto',
				'Setiembre',
				'Octubre',
				'Noviembre',
				'Diciembre'
		);
	
		$ifecha = strtotime( $fecha );
	
		$dia = date( 'd', $ifecha );
		$mes = date( 'm', $ifecha );
		$ani = date( 'Y', $ifecha );
	
		$fecha = $dia . " de " . $meses[ (int)$mes ] . " de " . $ani;
	
		return $fecha;
	}
	
	public function isModule($module)
	{
		print_r($this->_modules);
	}
}