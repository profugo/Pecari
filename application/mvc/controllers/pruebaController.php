<?php
/**
 * Controlador de muestra de modulo de prueba
 *
 * PHP Version 5.4
 *
 * @category Framework.mvc.controllers
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /application/mvc/controllers/pruebaController.php
 *
 */

/**
 * Controlador de muestra de modulo de prueba
 *
 * @category Framework.mvc.controllers
 * @package  Sistema_Taller
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /application/mvc/controllers/pruebaController.php
 */

include_once "masterTemplateController.php";

class PruebaController extends masterTemplateController
{
	/**
	 * Prototipo de constructor
	 * 
	 * @return null
	 */
    public function __construct()
    {
        parent::__construct();
    }

}