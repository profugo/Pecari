<?php
/**
 * Index de proyecto, carga la configuración del framework, y ejecuta el bootstrap
 *
 * PHP Version 5.4
 *
 * @category Framework.base
 * @package  Framework_Pecari
 * @author   Washington Fernández <desarrollo@coodi.com.uy>
 * @license  http://sistemas.coodi.info/aluf.txt Acuerdo de Licencia de Usuario Final
 * @link     /index.php
 *
 */


ini_set('display_errors', 1);
define("DS", DIRECTORY_SEPARATOR);

if (function_exists("date_default_timezone_set") && function_exists("date_default_timezone_get")) {
    @date_default_timezone_set(@date_default_timezone_get());
}

if (file_exists("install/index.php")) {
    if (is_writable("install")) {
        include "install/index.php";
        $directory = str_replace("/", "!", realpath(dirname(__FILE__ ))) . "!";
        chdir("core/helpers/confdb/");
        include "index.php";
        exit;
    } else {
        echo "ERROR: No se puede renombrar el directorio <code>install</code><br>Verifique los permisos de escritura.";
        exit;
    }
}

try{
    if (is_file('config' . DS . 'core.php')) {
        include_once 'config' . DS . 'core.php';
        include_once $config["core_path"] .DS .'core' . DS . 'base' . DS . 'Configuration.php';
    } else {
        include_once 'application' . DS . 'config' . DS . 'core.php';
        include_once $config["core_path"] .DS .'core' . DS . 'base' . DS . 'Configuration.php';
        $nonDistributed = true;
    }

    Configuration::init();

    include_once ROOT . 'application' . DS. 'modules' . DS . 'modulesConfig.php';
    include_once APP_PATH . 'Autoload.php';

    Session::init();

    $modsConf           = new ModsConf();
    $registry           = Registry::getInstancia();
    $modules            = Configuration::loadStatic("modules");
    $modules            = array_merge($modules["modules"], $modsConf->getModulos());
    $registry->_request = new Request($modules);
    $registry->_db      = new Database();
    $registry->_acl     = new ACL();
    $registry->_config  = new Configuration($registry->_acl);
	
    if( isset($nonDistributed)) {
    	$registry->_modules = $modsConf;
    }
    
    Bootstrap::run($registry->_request);
}
catch( Exception $e ){
    echo $e->getMessage();
}

// echo "<pre>"; print_r(debug_backtrace()); exit;
